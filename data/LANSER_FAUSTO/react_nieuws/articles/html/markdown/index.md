::: {#page .hfeed .site}
[Skip to content](#content){.skip-link .screen-reader-text}

::: {#header-top .header-bar-wrap}
::: {#header-bar .header-bar .container .clearfix}
:::
:::

::: {#masthead .site-header .clearfix role="banner"}
::: {.header-main .container .clearfix}
::: {#logo .site-branding .clearfix}
:::
:::

::: {#main-navigation-wrap .primary-navigation-wrap}
::: main-navigation-menu-wrap
-   [[Thuis](http://reactnieuws.net)]{#menu-item-24475}
-   [[IN ENGLISH](https://reactnieuws.net/english/)]{#menu-item-35934}
-   [[Adverteren&steun](https://reactnieuws.net/adverteren/)]{#menu-item-13128}
-   [[Contact](https://reactnieuws.net/contact/)]{#menu-item-13123}
-   [[Webwinkel Rechtse
    Rakkers](https://reactnieuws.net/webwinkel-rechtse-rakkers/)]{#menu-item-51800}
-   [[REACT-TV](https://reactnieuws.net/react-tv/)]{#menu-item-51910}
:::
:::
:::

::: {#headimg .header-image}
[![](https://reactnieuws.net/wp-content/uploads/2019/03/cropped-hoofding-react-1.jpg)](https://reactnieuws.net/){rel="home"}
:::

::: {#content .site-content .container .clearfix}
::: {#primary .section .content-single .content-area}
::: {#main .site-main role="main"}
![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/iranoorlog_vs-1.png?fit=840%2C471&ssl=1){.attachment-tortuga-single-post
.size-tortuga-single-post .wp-post-image .jetpack-lazy-image width="840"
height="471" attachment-id="49125"
permalink="https://reactnieuws.net/2020/01/29/alexander-wolfheze-iran-is-duidelijk-deel-van-de-indo-europese-cultuurcirkel/iranoorlog_vs/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/iranoorlog_vs-1.png?fit=948%2C532&ssl=1"
orig-size="948,532" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="iranoorlog_vs" image-description="" image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/iranoorlog_vs-1.png?fit=300%2C168&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/iranoorlog_vs-1.png?fit=840%2C471&ssl=1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/iranoorlog_vs-1.png?fit=840%2C471&ssl=1&is-pending-load=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}

::: entry-header
# (OPINIE) Geen oorlog met Iran! {#opinie-geen-oorlog-met-iran .entry-title}
:::

::: {.entry-meta .clearfix}
[[10 januari
2020](https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/ "10:17"){rel="bookmark"}]{.meta-date}[
[[Fausto
Lanser](https://reactnieuws.net/author/faustolanser/ "View all posts by Fausto Lanser"){.url
.fn .n rel="author"}]{.author .vcard}]{.meta-author}[
[Opinie](https://reactnieuws.net/category/opinie/){rel="category tag"}]{.meta-category}[
[One
comment](https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/#comments)]{.meta-comments}
:::

::: {.entry-content .clearfix}
President Trump van de VS escaleerde recentelijk dramatisch de
spanningen in het Midden-Oosten door een Iraanse Generaal te bombarderen
die in Irak was voor onderhandelingen over spanningen in de regio met de
Saudi's. De generaal en enkele leiders van Sjiitische milities in Irak
werden gedood. Dit werd door de VS verantwoord door te stellen dat de
generaal in kwestie, Suleimani, verantwoordelijk was voor terrorisme in
de regio en aanslagen op Amerikaanse soldaten. Dit alles bleek onwaar,
Suleimani was juist een van de krachten die ISIS had verslagen, en zijn
Sjiitische milities en Al-Quds Brigades zijn nooit in direct conflict
geweest met de VS. Het zijn altijd Soenitische milities geweest die
tegen de VS haar bezetting van Irak en Syrië vochten, en aanslagen
pleegden tegen Amerikaanse soldaten.

De VS heeft zich echter nooit druk gemaakt om waarheid, of
internationaal recht of zelfs kwesties van ethiek bij het volgen van
haar buitenlandse beleid. Getuige de illegale invasie van Irak in 2003,
die werd gerechtvaardigd met de leugen dat er massavernietigingswapens
waren, de illegale couppogingen in Bolivia en Venezuela recentelijk, de
destabilisatie van Libië en Syrië, en de vele, vele andere onderhandse
moorden, coups en oorlogen van agressie in de geschiedenis van de VS. De
VS is de grootse schurkenstaat en narcostaat die er is, echter nemen zij
op het wereldtoneel iedereen de maat, van Iran tot Noord-Korea, wanneer
wij het objectief bekijken, deze landen en haar regimes beduidend minder
dood en verderf op hun geweten hebben. En zij laten hun buurlanden
relatief ongemoeid in vergelijking met de VS. Laten wij niet vergeten
dat de VS militaire basissen heeft van Polen tot het Verenigd
Koninkrijk, in Somalië, Japan, Korea, de Caraïben, de Filipijnen en
zelfs op de zuidpool.

![Duitsland zit volgepakt met Amerikaanse basissen en
militairen](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/800px-us_military_bases_in_germany.png?resize=222%2C300&ssl=1){.size-medium
.wp-image-49121 .jetpack-lazy-image attachment-id="49121"
permalink="https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/800px-us_military_bases_in_germany/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/800px-us_military_bases_in_germany.png?fit=800%2C1082&ssl=1"
orig-size="800,1082" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="800px-US_military_bases_in_Germany" image-description=""
image-caption="<p>Duitsland zit volgepakt met Amerikaanse basissen en militairen </p>
"
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/800px-us_military_bases_in_germany.png?fit=222%2C300&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/800px-us_military_bases_in_germany.png?fit=757%2C1024&ssl=1"
loading="lazy" width="222" height="300" recalc-dims="1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/800px-us_military_bases_in_germany.png?resize=222%2C300&is-pending-load=1#038;ssl=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}

Dit is waarom men in Nationalistische kringen ook wel spreekt van *The
American Empire* en *The World Hegemon*. In deze unipolaire geopolitieke
situatie is er niemand die het Imperium van de VS direct uit kan dagen
en dus is er geen enkele noodzaak voor de VS om zich iets aan te trekken
van (vaak door henzelf opgerichte) internationale instituten,
internationaal recht en verdragen, of zelfs enige mate van wederzijds
respect en handelen normaal te vinden. Alleen Rusland, China en een paar
Midden-Oostelijke staatjes verzetten zich enigszins, maar dit zijn -- om
het in klassieke termen te gieten -- buitengebieden die geen dreiging
zijn voor de *core provinces*. De grote economisch ontwikkelde zones van
de wereld staan ferm onder Amerikaanse controle. Soms met directe
militaire aanwezigheid als stok achter de deur, zoals de aanzienlijke
legermacht aanwezig in Duitsland (controle over Europa), Japan en Korea
(controle over het verre oosten), in Puerto Rico en Panama (controle
over Zuid-Amerika) of in Qatar, Irak en Jordanië (dit zijn nog recente
veroveringen maar vanuit deze posities beoogt de VS haar macht over het
Midden-Oosten uit te breiden). Dit is vergelijkbaar met in het verre
verleden dat Romeinse legioenen waren gestationeerd langs de Rijn en
Donau en in rebelse gebieden. Waar *soft power* niet toereikend is, in
de vorm van economische afhankelijkheid, sancties of 'hulp' van het IMF,
dan komen eerst Amerikaanse vliegtuigen en dan de laarzen van de
militairen. De manier waarop Imperia handelen is in de 5000 jaar van
verstedelijking niet veranderd, het is altijd dezelfde methodologie van
militaire *shock and awe*, het economisch afhankelijk maken van nieuwe
gebieden en dan het instellen van wingewesten (zoals Zuid-Amerika dat
lang was bijvoorbeeld) en het consolideren van de cultuur naar het model
van de veroveraar.

Wanneer de VS een morele of ethische of wettelijke motivatie aanvoert om
haar handelen te verantwoorden is dit altijd volstrekte onzin. Soleimani
en zijn Irakese bondgenoten werden niet omgelegd omdat hij een terrorist
was met dode Amerikanen op zijn geweten, zoals Trump beweert, maar omdat
hij een effectieve militaire leider was die Amerikaanse uitbreiding in
de regio in de weg stond. Hij bewerkstelligde de *Shia Crescent* van
bondgenootschappen tussen Assads Syrië, Hezbollah, Irakese Sjiieten en
Iran. Deze bundelingen van krachten is een bedreiging voor de positie
van de VS in de regio, maar met name ook voor Israël, Amerika's
belangrijkste provincie in de regio. En wanneer de Israëlische lobby in
de VS zegt spring, dan zegt een Amerikaanse politicus, Republikein of
Democraat, "hoe hoog?". Dus veegt men deze generaal van de kaart,
ondanks dat dit een escalatie betekend die de VS zich in haar huidige
positie niet kan veroorloven. Want hoewel de VS met gemak Iran plat zou
kunnen walsen, zou dit een te groot offer aan levens en geld betekenen
die men in het hartland niet bereid is te dragen. Buiten dat het een
groter conflict met Rusland en China aan zou wakkeren, die met lede ogen
aanzien hoe de VS het Midden-Oosten beetje bij beetje veroverd. Dat is
waarom Rusland Assad steunt, om de hegemonie tegen te werken die ook
steeds dichter naar hun voordeur kruipt, met als belangrijkste escalatie
*regime change* in Oekraïne in 2014. Rusland en China zien zich steeds
verder in het nauw gedreven door het Imperium, net als Iran, dus hebben
zij uit pure noodzaak een bondgenootschap gesloten. China neemt echter
ook veel van haar olie af uit Iran en als de VS controle heeft over
China's belangrijkste energiebron is dat een strategisch onwelgevallige
positie voor China, welke al in een handelsoorlog met de VS verkeert.

Dit alles is dus waarom Nationalisten, zowel in Europa als in de VS,
tegen oorlog met Iran zijn en tegen oorlog in het algemeen. De
Amerikaanse nationalisten zien dat hun enorme imperium vooral ten koste
gaat van hun eigen mensen. Amerikaanse soldaten sneuvelen en de
Amerikaanse middenklasse wordt leeggezogen puur ten behoeve van een heel
kleine elite op Wall Street en in Silicon Valley. De veroveringsoorlogen
komen niet ten goede van de gewone Amerikaan, net zoals dat de
veroveringsoorlogen van Rome, bevochten door het zwaard van de gewone
Romeinse man, vooral ten goede kwamen van de kleine elite van
Patriciërs. Er is geen meer heldhaftige dood dan sterven op het
slagveld, maar vechten in een oorlog van agressie ten behoeve van een
geldbeluste elite en ten koste van het eigen Volk kan nooit heroïsch
zijn. Heroïsme is vechten en sterven ter verdediging, en voor het
belang, van het eigen Volk. De Amerikaanse soldaat vecht voor
globalisme, liberalisme, het verspreiden van Gay Pride,
woekerkapitalisme en individualisme. Geen wonder dat er posttraumatische
stressstoornis heerst onder veteranen en dat er zoveel zelfmoorden zijn
onder militairen.

![Alt-right icoon Greg Johnson kritisch naar Trumps beleid in
Iran.](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/gregjohnson_iran.png?resize=400%2C370&ssl=1){.size-medium
.wp-image-49123 .jetpack-lazy-image attachment-id="49123"
permalink="https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/gregjohnson_iran/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/gregjohnson_iran.png?fit=680%2C611&ssl=1"
orig-size="680,611" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="gregjohnson_iran" image-description=""
image-caption="<p>Alt-right icoon Greg Johnson kritisch naar Trumps beleid in Iran.</p>
"
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/gregjohnson_iran.png?fit=300%2C270&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/gregjohnson_iran.png?fit=680%2C611&ssl=1"
loading="lazy" width="400" height="370" recalc-dims="1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/gregjohnson_iran.png?resize=400%2C370&is-pending-load=1#038;ssl=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}

Een Amerikaans Nationalist is dus tegen oorlog om dat het Imperium hem
niets brengt. Nationalisten zoals Nicholas Fuentes, Patrick Casey, Greg
Johnson en Jared Taylor verkondigen dit dan ook duidelijk. Toen Amerika
nog isolationistisch was, tot de Eerste Wereldoorlog en in zekere mate
nog tot de Tweede Wereldoorlog, kende zij een ongekende periode van
voorspoed en groei. Het was de *golden age* en de *gilded age*. De tijd
van grote industrialisaties en enorme technologische vooruitgang. Van
culturele opleving in romanticisme, de tijd van Poe, Melville en later
Fitzgerald en Hemmingway. Amerika is nu een cultureel, droge vlakte met
een uitgeputte en leeggetrokken middenklasse verslaafd aan drugs. Dat is
veelal te wijten aan het in stand houden van haar Imperium. Want dat is
hoe het de oorspronkelijke bevolking van Imperia vergaat. Romeinen uit
de stad zelf waren ondanks hun enorme Rijk, afhankelijk van
graanrantsoenen van de staat en hadden als ontsnapping uit hun
miserabele dagelijkse leven gladiatorische spelen, betaalt door politici
die populariteit poogden te vergaren om verkozen te worden. In dat
opzicht is ook niets veranderd. Politici zoals Trump en Bernie Sanders
slaan populistische retoriek uit om verkozen te worden, maar maken nooit
iets waar van hun beloftes, omdat zij opgeslokt worden door de mechaniek
van Imperia. En dus gaan de oorlogen verder en blijven de legionairs
marcheren voor oorlogen waar zij het doel niet meer van inzien.

In de bezette gebieden zoals Europa, zijn Nationalisten tegen oorlog
omdat wij willen dat de *hegemon* omvergeworpen wordt, opdat wij weer
soevereiniteit hebben en zelf kunnen bepalen hoe wij onze koers varen,
of dat nu cultureel, financieel of militair is. Want in al deze zaken
zijn wij onderworpen aan de VS. Van McDonalds tot Hollywood, onze
cultuur is geïnfecteerd met een Amerikaanse beeldvorming van de wereld.
Financieel is elk land ter wereld afhankelijk van de Petrodollar, de
centrale banken en Wall Street. En militair, militair zijn onze legers
niets meer dan hulptroepen. De hulptroepen van de VS die men vooral voor
bezettingstaken en politiedienst inzet, zoals het patrouilleren van de
Somalische kust of het indammen van regionale rebellen zoals in Mali.
Dat is wat het is om een Nederlands marinier of soldaat te zijn, een
hulptroep die vecht om de hegemonie van het Imperium in stand te houden.
Voor Nederland of het Nederlandse Volk vecht hij echter niet.
Patriotisme is een handige leugen die het regime gebruikt om jonge
mannen te motiveren in haar dienst te treden. Authentiek Nationalisme en
Patriotisme zijn het regime vreemd.

![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?resize=300%2C202&ssl=1){.alignleft
.size-medium .wp-image-49122 .jetpack-lazy-image attachment-id="49122"
permalink="https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/financing-1/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?fit=600%2C404&ssl=1"
orig-size="600,404" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="financing-1" image-description="" image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?fit=300%2C202&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?fit=600%2C404&ssl=1"
loading="lazy" width="300" height="202" recalc-dims="1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?resize=300%2C202&is-pending-load=1#038;ssl=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?resize=300%2C202&ssl=1){.alignleft
.size-medium .wp-image-49122 attachment-id="49122"
permalink="https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/financing-1/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?fit=600%2C404&ssl=1"
orig-size="600,404" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="financing-1" image-description="" image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?fit=300%2C202&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/financing-1.png?fit=600%2C404&ssl=1"
loading="lazy" width="300" height="202" recalc-dims="1"}

In deze geopolitieke macroanalyse zijn zaken zoals culturele verschillen
tussen Iran en het Westen minder belangrijk dan de daadwerkelijke
globale machtsverhoudingen. De bevolking opjutten tegen Iran door de
propaganda te richten op het islamitische karakter van het regime is een
'spook' zoals Max Stirner het zou noemen. Iets waar men PVV'ers en
anti-jihad types mee kan vangen om hen aan de kant van oorlog te
krijgen. Het is niets meer dan propaganda van het Imperium om enige
steun onder de bevolking voor haar oorlogen te krijgen. Zoals de
oplettende lezer wel gemerkt zal hebben kan de propaganda binnen een
seconde draaien, en is de boodschap weer dat multiculturalisme goed is
en islamofobie een probleem is. De propaganda die op het binnenland
gericht is en die op het buitenland, zijn dan ook vrij vaak
tegenstrijdig omdat de agenda voor veroverde gebieden consolidatie en
indoctrinatie in de ideologie van het regime is. De buitenlandse agenda
is chauvinisme aanwakkeren. Deze tegenstrijdige berichten die het regime
verkondigt valt echter niet veel mensen op, zo lijkt het.

Waarom zou een Nationalist dus ook oorlog met Iran, of welk land dan ook
in de huidige context steunen? Bloed vergiet men alleen bij de
verdediging van het eigen Land en Volk. Op zijn best vecht men als dit
het landsbelang dient. Maar imperiale oorlogen van verovering in dienst
van een liberale wereldorde die financiële woeker, homoseksualiteit,
transgenderisme, individualisme, massa-immigratie en decadentie in haar
kielzog voert, waarom zou een Nationalist dat steunen? Omdat er moslims
in Nederland zijn en Iraniërs ook moslim zijn? Alstublieft zeg. Dat zijn
om te beginnen al afzonderlijke kwesties en het is niet Iran die
islamisering en terrorisme in Europa financiert, dat is Saoedi-Arabië,
een 'bondgenoot' in de oorlog met Iran.

Het is alleen in het belang van het Amerikaanse Imperium, Israël en de
financiële elite om oorlog te voeren met Iran. Dus, geen oorlog met
Iran!

::: {.sharedaddy .sd-sharing-enabled}
::: {.robots-nocontent .sd-block .sd-social .sd-social-official .sd-sharing}
### Deel het met de wereld! {#deel-het-met-de-wereld .sd-title}

::: sd-content
-   ::: {.fb-share-button data-href="https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/" layout="button_count"}
    :::

-   [Tweet](https://twitter.com/share){.twitter-share-button
    url="https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/"
    data-text="(OPINIE) Geen oorlog met Iran!" via="reactnieuws"}

-   ::: linkedin_button
    :::

-   [WhatsApp](https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/?share=jetpack-whatsapp "Klik om te delen op WhatsApp"){.share-jetpack-whatsapp
    .sd-button rel="nofollow noopener noreferrer" shared=""
    target="_blank"}

-   [E-mail](https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/?share=email "Klik om dit te e-mailen naar een vriend"){.share-email
    .sd-button rel="nofollow noopener noreferrer" shared=""
    target="_blank"}

-   [Print](https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/#print "Klik om af te drukken"){.share-print
    .sd-button rel="nofollow noopener noreferrer" shared=""
    target="_blank"}

-   ::: reddit_button
    ::: iframe
    ::: wrap
    [reddit](#){.logo}

    ::: votebox
    [Upvote](#){.vote .up}[Downvote](#){.vote
    .down}[](#){.submission-details}
    :::
    :::
    :::
    :::

-   [Telegram](https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/?share=telegram "Klik om te delen op Telegram"){.share-telegram
    .sd-button rel="nofollow noopener noreferrer" shared=""
    target="_blank"}

-   ::: {.skype-share data-href="https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/" data-lang="en-US" data-style="small" source="jetpack"}
    :::

-   
:::
:::
:::

::: {#like-post-wrapper-15980765-49126-6207acdf7dc71 .sharedaddy .sd-block .sd-like .jetpack-likes-widget-wrapper .jetpack-likes-widget-unloaded data-src="https://widgets.wp.com/likes/#blog_id=15980765&post_id=49126&origin=reactnieuws.net&obj_id=15980765-49126-6207acdf7dc71" data-name="like-post-frame-15980765-49126-6207acdf7dc71" data-title="Vind ik leuk of Rebloggen"}
### Vind ik leuk: {#vind-ik-leuk .sd-title}

::: {.likes-widget-placeholder .post-likes-widget-placeholder style="height: 55px;"}
[Like]{.button} [Laden\...]{.loading}
:::

[]{.sd-text-color}[]{.sd-link-color}
:::
:::

::: {.entry-tags .clearfix}
[
[Iran](https://reactnieuws.net/tag/iran/){rel="tag"}[Nationalisme](https://reactnieuws.net/tag/nationalisme/){rel="tag"}[Trump](https://reactnieuws.net/tag/trump/){rel="tag"}
]{.meta-tags}
:::

## Berichtnavigatie {#berichtnavigatie .screen-reader-text}

::: nav-links
::: nav-previous
[[Previous Post:]{.screen-reader-text} Zijn de bosbranden in Australië
een gevolg van opwarming van de
aarde?](https://reactnieuws.net/2020/01/10/zijn-de-bosbranden-in-australie-een-gevolg-van-opwarming-van-de-aarde/){rel="prev"}
:::

::: nav-next
[[Next Post:]{.screen-reader-text} Oeioei, de computer
lekt](https://reactnieuws.net/2020/01/11/oeioei-de-computer-lekt/){rel="next"}
:::
:::

::: {#comments .comments-area}
::: comments-header
## One comment {#one-comment .comments-title}
:::

1.  ::: {#comment-18207}
    ::: {.comment-author .vcard}
    **stef vereecken** [schreef:]{.says}
    :::

    ::: comment-metadata
    [12 januari 2020 om
    10:14](https://reactnieuws.net/2020/01/10/geen-oorlog-met-iran/#comment-18207)
    :::

    ::: comment-content
    Op zo een dom en naïeve opinie hoeft geen reactie, man, man...

    ::: {#like-comment-wrapper-15980765-18207-6207acdf7eff7 .jetpack-comment-likes-widget-wrapper .jetpack-likes-widget-unloaded data-src="https://widgets.wp.com/likes/#blog_id=15980765&comment_id=18207&origin=reactnieuws.net&obj_id=15980765-18207-6207acdf7eff7" data-name="like-comment-frame-15980765-18207-6207acdf7eff7"}
    ::: {.likes-widget-placeholder .comment-likes-widget-placeholder .comment-likes}
    [Laden\...]{.loading}
    :::

    ::: {.comment-likes-widget .jetpack-likes-widget .comment-likes}
    []{.comment-like-feedback}[]{.sd-text-color}[]{.sd-link-color}
    :::
    :::

    ::: {#pd_rating_holder_2767031_comm_18207 .cs-rating .pd-rating}
    :::
    :::
    :::

Comments are closed.
:::
:::
:::

::: {#secondary .section .sidebar .widget-area .clearfix role="complementary"}
::: widget-wrap
::: widget-header
### Sociale media {#sociale-media .widget-title}
:::

-   [[Spotify]{.screen-reader-text}![](data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLXNwb3RpZnkiIGFyaWEtaGlkZGVuPSJ0cnVlIiByb2xlPSJwcmVzZW50YXRpb24iPiA8dXNlIGhyZWY9IiNpY29uLXNwb3RpZnkiIHhsaW5rOmhyZWY9IiNpY29uLXNwb3RpZnkiPjwvdXNlPiA8L3N2Zz4=){.icon
    .icon-spotify}](https://open.spotify.com/show/5u0EnzQNw5Xc0QRbb5hBnb){target="_self"}
-   [[Twitter]{.screen-reader-text}![](data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLXR3aXR0ZXIiIGFyaWEtaGlkZGVuPSJ0cnVlIiByb2xlPSJwcmVzZW50YXRpb24iPiA8dXNlIGhyZWY9IiNpY29uLXR3aXR0ZXIiIHhsaW5rOmhyZWY9IiNpY29uLXR3aXR0ZXIiPjwvdXNlPiA8L3N2Zz4=){.icon
    .icon-twitter}](https://twitter.com/reactnieuws/){target="_self"}
-   [[Facebook]{.screen-reader-text}![](data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLWZhY2Vib29rIiBhcmlhLWhpZGRlbj0idHJ1ZSIgcm9sZT0icHJlc2VudGF0aW9uIj4gPHVzZSBocmVmPSIjaWNvbi1mYWNlYm9vayIgeGxpbms6aHJlZj0iI2ljb24tZmFjZWJvb2siPjwvdXNlPiA8L3N2Zz4=){.icon
    .icon-facebook}](https://www.facebook.com/nieuwsvanreact){target="_self"}
-   [[YouTube]{.screen-reader-text}![](data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLXlvdXR1YmUiIGFyaWEtaGlkZGVuPSJ0cnVlIiByb2xlPSJwcmVzZW50YXRpb24iPiA8dXNlIGhyZWY9IiNpY29uLXlvdXR1YmUiIHhsaW5rOmhyZWY9IiNpY29uLXlvdXR1YmUiPjwvdXNlPiA8L3N2Zz4=){.icon
    .icon-youtube}](https://www.youtube.com/channel/UCk4Of0Pa6g99THUxiIFruyQ){target="_self"}
-   [[Telegram]{.screen-reader-text}![](data:image/svg+xml;base64,PHN2ZyBjbGFzcz0iaWNvbiBpY29uLXRlbGVncmFtIiBhcmlhLWhpZGRlbj0idHJ1ZSIgcm9sZT0icHJlc2VudGF0aW9uIj4gPHVzZSBocmVmPSIjaWNvbi10ZWxlZ3JhbSIgeGxpbms6aHJlZj0iI2ljb24tdGVsZWdyYW0iPjwvdXNlPiA8L3N2Zz4=){.icon
    .icon-telegram}](https://t.me/reactnieuws){target="_self"}
:::

::: widget-wrap
::: widget-header
### ReactGesprek {#reactgesprek .widget-title}
:::

::: wp-widget-group__inner-blocks
::: {.wp-container-6207acdf80492 .wp-block-group}
::: wp-block-group__inner-container
::: {.wp-container-6207acdf80203 .wp-block-columns}
::: {.wp-block-column .is-vertically-aligned-bottom}
:::
:::
:::
:::
:::
:::

::: widget-wrap
::: widget-header
### Volg reactnieuws via e-post {#volg-reactnieuws-via-e-post .widget-title}
:::

::: {#subscribe-text}
Voer je e-postadres in om deze webstek te volgen en om per e-post
meldingen over nieuwe berichten te ontvangen.
:::

E-mailadres

Volg
:::

::: widget-wrap
![](https://reactnieuws.net/wp-content/uploads/2021/09/202109-breek-de-censuur-add-react-a4-web-1.jpg?w=724){.wp-image-56852
loading="lazy" width="2480" height="3508"}
:::

::: widget-wrap
::: widget-header
### Steun onze adverteerders! {#steun-onze-adverteerders .widget-title}
:::

[![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-fdm-1.jpg?fit=211%2C300&ssl=1){.image
.wp-image-48430 .attachment-medium .size-medium .jetpack-lazy-image
width="211" height="300" loading="lazy"
style="max-width: 100%; height: auto;" attachment-id="48430"
permalink="https://reactnieuws.net/advertentie-fdm-2/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-fdm-1.jpg?fit=1353%2C1920&ssl=1"
orig-size="1353,1920" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="advertentie FDM" image-description="" image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-fdm-1.jpg?fit=211%2C300&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-fdm-1.jpg?fit=722%2C1024&ssl=1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-fdm-1.jpg?fit=211%2C300&ssl=1&is-pending-load=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}](https://www.facebook.com/filipdemanVB/)
:::

::: widget-wrap
[![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2021/03/advertentie-elementen-3.png?fit=300%2C275&ssl=1){.image
.wp-image-54640 .attachment-medium .size-medium .jetpack-lazy-image
width="300" height="275" loading="lazy"
style="max-width: 100%; height: auto;" attachment-id="54640"
permalink="https://reactnieuws.net/advertentie-elementen-3/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2021/03/advertentie-elementen-3.png?fit=472%2C432&ssl=1"
orig-size="472,432" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="advertentie elementen 3" image-description=""
image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2021/03/advertentie-elementen-3.png?fit=300%2C275&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2021/03/advertentie-elementen-3.png?fit=472%2C432&ssl=1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2021/03/advertentie-elementen-3.png?fit=300%2C275&ssl=1&is-pending-load=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}](http://elementen.news)
:::

::: widget-wrap
[![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/wie-zijn-religie-verlaat.gif?fit=300%2C250&ssl=1){.image
.wp-image-49212 .attachment-medium .size-medium .jetpack-lazy-image
width="300" height="250" loading="lazy"
style="max-width: 100%; height: auto;" attachment-id="49212"
permalink="https://reactnieuws.net/wie-zijn-religie-verlaat/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/wie-zijn-religie-verlaat.gif?fit=500%2C417&ssl=1"
orig-size="500,417" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="Wie-zijn-religie-verlaat" image-description=""
image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/wie-zijn-religie-verlaat.gif?fit=300%2C250&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/wie-zijn-religie-verlaat.gif?fit=500%2C417&ssl=1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/wie-zijn-religie-verlaat.gif?fit=300%2C250&ssl=1&is-pending-load=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}](https://polemos.be)
:::

::: widget-wrap
[![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/rechts-actueel-advertentie-pallieterke-2.jpg?fit=211%2C300&ssl=1){.image
.wp-image-49213 .attachment-medium .size-medium .jetpack-lazy-image
width="211" height="300" loading="lazy"
style="max-width: 100%; height: auto;" attachment-id="49213"
permalink="https://reactnieuws.net/rechts-actueel-advertentie-pallieterke-2/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/rechts-actueel-advertentie-pallieterke-2.jpg?fit=496%2C704&ssl=1"
orig-size="496,704" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="rechts-actueel-advertentie-pallieterke 2"
image-description="" image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/rechts-actueel-advertentie-pallieterke-2.jpg?fit=211%2C300&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/rechts-actueel-advertentie-pallieterke-2.jpg?fit=496%2C704&ssl=1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2020/01/rechts-actueel-advertentie-pallieterke-2.jpg?fit=211%2C300&ssl=1&is-pending-load=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}](https://pallieterke.net)
:::

::: widget-wrap
[![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/06/nationalistisch-verzet-voorpost-2019-advertentie-re.jpg?fit=2480%2C3508&ssl=1){.image
.wp-image-46449 .alignnone .attachment-full .size-full
.jetpack-lazy-image width="2480" height="3508" loading="lazy"
style="max-width: 100%; height: auto;" attachment-id="46449"
permalink="https://reactnieuws.net/nationalistisch-verzet-voorpost-2019-advertentie-re/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/06/nationalistisch-verzet-voorpost-2019-advertentie-re.jpg?fit=2480%2C3508&ssl=1"
orig-size="2480,3508" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"1\"}"
image-title="Nationalistisch Verzet Voorpost 2019 (advertentie Re"
image-description="" image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/06/nationalistisch-verzet-voorpost-2019-advertentie-re.jpg?fit=212%2C300&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/06/nationalistisch-verzet-voorpost-2019-advertentie-re.jpg?fit=724%2C1024&ssl=1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/06/nationalistisch-verzet-voorpost-2019-advertentie-re.jpg?fit=2480%2C3508&ssl=1&is-pending-load=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}](http://www.voorpost.org/)
:::

::: widget-wrap
[![](https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-rechtse-rakkers.png?fit=1748%2C2480&ssl=1){.image
.wp-image-47979 .alignnone .attachment-full .size-full
.jetpack-lazy-image width="1748" height="2480" loading="lazy"
style="max-width: 100%; height: auto;" attachment-id="47979"
permalink="https://reactnieuws.net/advertentie-rechtse-rakkers-2/"
orig-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-rechtse-rakkers.png?fit=1748%2C2480&ssl=1"
orig-size="1748,2480" comments-opened="1"
image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}"
image-title="Advertentie Rechtse Rakkers" image-description=""
image-caption=""
medium-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-rechtse-rakkers.png?fit=211%2C300&ssl=1"
large-file="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-rechtse-rakkers.png?fit=722%2C1024&ssl=1"
lazy-src="https://i0.wp.com/reactnieuws.net/wp-content/uploads/2019/11/advertentie-rechtse-rakkers.png?fit=1748%2C2480&ssl=1&is-pending-load=1"
srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"}](https://rechtserakkers.com/curiosa)
:::

::: {.widget_text .widget-wrap}
::: widget-header
### 5 euro voor de vrije pers {#euro-voor-de-vrije-pers .widget-title}
:::

::: {.textwidget .custom-html-widget}
**Steun ons, stort je bijdrage!**

::: {.jetpack-simple-payments-41966 .jetpack-simple-payments-wrapper}
::: jetpack-simple-payments-product
::: jetpack-simple-payments-details
::: jetpack-simple-payments-title
Vijf euro voor de vrije pers!
:::

::: jetpack-simple-payments-description
Lang genoeg heeft de leugenpers de mensen misleid met fake news. Dankzij
de sociale media breekt de vrije pers door -- Maar dat kan alleen
dankzij jouw steun! Links onder deze tekst ziet u een vakje met daarin
'5', als u op de PayPallknop rechts ernaast drukt, geeft u ons 5 euro.
(Het mogen er ook meer zijn, u kunt het bedrag zelf veranderen...) De
betaling verloopt automatisch via een Paypall-rekening. Storten via
bankrekening kan ook; zie onze gegevens hieronder! DANK U!
:::

::: jetpack-simple-payments-price
€5,00
:::

::: {#jetpack-simple-payments-41966_6207acdf82a161.42840529-message-container .jetpack-simple-payments-purchase-message}
:::

::: jetpack-simple-payments-purchase-box
::: jetpack-simple-payments-items
:::

::: {#jetpack-simple-payments-41966_6207acdf82a161.42840529_button .jetpack-simple-payments-button}
:::
:::
:::
:::
:::
:::
:::
:::
:::

::: {#footer-widgets-wrap .footer-widgets-wrap}
::: {#footer-widgets .footer-widgets .container}
::: {#footer-widgets-columns .footer-widgets-columns .clearfix role="complementary"}
::: footer-widget-column
Zoek

::: wp-block-search__inside-wrapper
Zoek
:::
:::

::: footer-widget-column
::: widget-header
### Instagram {#instagram .widget-title}
:::

::: {.wpcom-instagram-images .wpcom-instagram-columns-2}
[](https://www.instagram.com/p/CRqQe98DJ2M/){target="_self"}

::: {.sq-bg-image style="background-image: url(https://scontent-lax3-2.cdninstagram.com/v/t51.29350-15/220818654_216014946972218_7098671679013109040_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=IWF6lI6d0SoAX9FKzyt&_nc_ht=scontent-lax3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT8azAUsD1jxWXqh2n5gF5uhT8DP7BTzabCRcgu33RxiZA&oe=620CAFBB)"}
[\'Oekraïne doet laatste poging gasleiding te
saboteren.\']{.screen-reader-text}
:::

[](https://www.instagram.com/p/CLOha6mBGhQ/){target="_self"}

::: {.sq-bg-image style="background-image: url(https://scontent-lax3-2.cdninstagram.com/v/t51.29350-15/149677854_118826880148436_7576409857692454806_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=ZPP1VZsVHG8AX8QFIFl&_nc_ht=scontent-lax3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT-rHiH55GTRcpvyujNxHX93hAMtZeNZ9-y3esRvtxPqTg&oe=620C7280)"}
[\'Het spuitje van Rutte\']{.screen-reader-text}
:::

[](https://www.instagram.com/p/CKrX2LQB23H/){target="_self"}

::: {.sq-bg-image style="background-image: url(https://scontent-lax3-2.cdninstagram.com/v/t51.29350-15/143767217_142287977627469_9085957302129603712_n.jpg?_nc_cat=101&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=z9BR5sJ_2fEAX-DdWAV&_nc_ht=scontent-lax3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT_mW5cj05Lyix_SYZxtHuS3ffOmloBB_onLMqlWF_jrpg&oe=620D55ED)"}
[De vrije liberale democratie: warm en verbonden! ]{.screen-reader-text}
:::

[](https://www.instagram.com/p/B2ZqEAengMF/){target="_self"}

::: {.sq-bg-image style="background-image: url(https://scontent-lax3-1.cdninstagram.com/v/t51.2885-15/67962730_367312050890078_3609970200804677722_n.jpg?_nc_cat=110&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=-lcbm4Ze5dcAX_bKZ6d&_nc_ht=scontent-lax3-1.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT_FKfXI3HHLtC9MHhDq8I-NLixtn1GCcSm_EguPV6kqhg&oe=620C9A91)"}
[IJzerwake van 2019 te Ieper]{.screen-reader-text}
:::

[](https://www.instagram.com/p/B2ZqANzHtyJ/){target="_self"}

::: {.sq-bg-image style="background-image: url(https://scontent-lax3-1.cdninstagram.com/v/t51.2885-15/69832760_490830335050717_1740528506977591519_n.jpg?_nc_cat=109&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=HqhRR4fMpIoAX-g6kIg&_nc_ht=scontent-lax3-1.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT8nbFvP33Uk5CTdmqhDkzDrzxV59-2e1m0DH2IBSLwtKQ&oe=620D0595)"}
[IJzerwake van 2019 te Ieper]{.screen-reader-text}
:::

[](https://www.instagram.com/p/B2Zp9sPHfFO/){target="_self"}

::: {.sq-bg-image style="background-image: url(https://scontent-lax3-1.cdninstagram.com/v/t51.2885-15/70725344_119787332413057_4236619426292907310_n.jpg?_nc_cat=105&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=K0bC_MI-RusAX8pyTB5&_nc_ht=scontent-lax3-1.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT9mqnNSzLh0MaiVuWHTqs2eRhftjQ40pEvNAO0DscvRww&oe=620CA789)"}
[IJzerwake van 2019 te Ieper]{.screen-reader-text}
:::
:::
:::

::: footer-widget-column
::: widget-header
### Categorieën {#categorieën .widget-title}
:::

-   [Flitsbericht](https://reactnieuws.net/category/flitsbericht/)
-   [Nieuws](https://reactnieuws.net/category/nieuws/)
-   [Opinie](https://reactnieuws.net/category/opinie/)
-   [Overig](https://reactnieuws.net/category/overig/)
-   [Verslag](https://reactnieuws.net/category/verslag/)
-   [Video en Audio](https://reactnieuws.net/category/video-en-audio/)
:::

::: footer-widget-column
::: widget-header
### Sleutelwoorden {#sleutelwoorden .widget-title}
:::

::: tagcloud
[Antwerpen](https://reactnieuws.net/tag/antwerpen/){.tag-cloud-link
.tag-link-177627 .tag-link-position-1
style="font-size: 8.9180327868852pt;" aria-label="Antwerpen (83 items)"}
[brussel](https://reactnieuws.net/tag/brussel-2/){.tag-cloud-link
.tag-link-36271503 .tag-link-position-2
style="font-size: 10.983606557377pt;" aria-label="brussel (101 items)"}
[duitsland](https://reactnieuws.net/tag/duitsland-2/){.tag-cloud-link
.tag-link-36247065 .tag-link-position-3
style="font-size: 8.9180327868852pt;" aria-label="duitsland (83 items)"}
[eu](https://reactnieuws.net/tag/eu/){.tag-cloud-link .tag-link-2702
.tag-link-position-4 style="font-size: 12.819672131148pt;"
aria-label="eu (123 items)"} [Europese
Unie](https://reactnieuws.net/tag/europese-unie/){.tag-cloud-link
.tag-link-171039 .tag-link-position-5 style="font-size: 8pt;"
aria-label="Europese Unie (74 items)"}
[Frankrijk](https://reactnieuws.net/tag/frankrijk/){.tag-cloud-link
.tag-link-180858 .tag-link-position-6
style="font-size: 16.032786885246pt;"
aria-label="Frankrijk (170 items)"} [Front
National](https://reactnieuws.net/tag/front-national/){.tag-cloud-link
.tag-link-796712 .tag-link-position-7
style="font-size: 10.295081967213pt;"
aria-label="Front National (95 items)"}
[immigratie](https://reactnieuws.net/tag/immigratie/){.tag-cloud-link
.tag-link-1623275 .tag-link-position-8
style="font-size: 8.4590163934426pt;"
aria-label="immigratie (79 items)"}
[islam](https://reactnieuws.net/tag/islam-2/){.tag-cloud-link
.tag-link-831549 .tag-link-position-9
style="font-size: 18.55737704918pt;" aria-label="islam (220 items)"}
[islamisering](https://reactnieuws.net/tag/islamisering/){.tag-cloud-link
.tag-link-516120 .tag-link-position-10
style="font-size: 10.295081967213pt;"
aria-label="islamisering (94 items)"} [marine le
pen](https://reactnieuws.net/tag/marine-le-pen/){.tag-cloud-link
.tag-link-810641 .tag-link-position-11 style="font-size: 8pt;"
aria-label="marine le pen (75 items)"}
[n-va](https://reactnieuws.net/tag/n-va-2/){.tag-cloud-link
.tag-link-56316174 .tag-link-position-12
style="font-size: 17.868852459016pt;" aria-label="n-va (202 items)"}
[NSV!](https://reactnieuws.net/tag/nsv/){.tag-cloud-link
.tag-link-760268 .tag-link-position-13
style="font-size: 8.2295081967213pt;" aria-label="NSV! (76 items)"}
[pvv](https://reactnieuws.net/tag/pvv/){.tag-cloud-link
.tag-link-1192434 .tag-link-position-14
style="font-size: 10.295081967213pt;" aria-label="pvv (95 items)"}
[racisme](https://reactnieuws.net/tag/racisme/){.tag-cloud-link
.tag-link-26297 .tag-link-position-15
style="font-size: 8.6885245901639pt;" aria-label="racisme (81 items)"}
[Syrië](https://reactnieuws.net/tag/syrie/){.tag-cloud-link
.tag-link-273535 .tag-link-position-16
style="font-size: 9.8360655737705pt;" aria-label="Syrië (91 items)"}
[Vlaams
Belang](https://reactnieuws.net/tag/vlaams-belang/){.tag-cloud-link
.tag-link-379180 .tag-link-position-17 style="font-size: 22pt;"
aria-label="Vlaams Belang (308 items)"}
[Voorpost](https://reactnieuws.net/tag/voorpost/){.tag-cloud-link
.tag-link-3057502 .tag-link-position-18
style="font-size: 14.426229508197pt;" aria-label="Voorpost (142 items)"}
[zuid-afrika](https://reactnieuws.net/tag/zuid-afrika/){.tag-cloud-link
.tag-link-53127 .tag-link-position-19
style="font-size: 13.27868852459pt;"
aria-label="zuid-afrika (127 items)"} [Zwarte
Piet](https://reactnieuws.net/tag/zwarte-piet/){.tag-cloud-link
.tag-link-231609 .tag-link-position-20
style="font-size: 8.2295081967213pt;"
aria-label="Zwarte Piet (76 items)"}
:::
:::
:::
:::
:::

::: {#footer .footer-wrap}
::: site-info
:::
:::
:::

::: {#nxsFavNotice style="display: none;"}
::: {#nxsFavNoticeCnt}
Posting\....
:::
:::

::: {#jp-carousel-loading-overlay}
::: {#jp-carousel-loading-wrapper}
[ ]{#jp-carousel-library-loading}
:::
:::

::: {.jp-carousel-overlay style="display: none;"}
::: jp-carousel-container
::: {.jp-carousel-wrap .swiper-container .jp-carousel-swiper-container .jp-carousel-transitions itemscope="" itemtype="https://schema.org/ImageGallery"}
::: {.jp-carousel .swiper-wrapper}
:::

::: {.jp-swiper-button-prev .swiper-button-prev}
![](data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUiIGhlaWdodD0iMjQiIHZpZXdib3g9IjAgMCAyNSAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgICAgICAgICAgICAgICAgICAgICAgPG1hc2sgaWQ9Im1hc2tQcmV2IiBtYXNrLXR5cGU9ImFscGhhIiBtYXNrdW5pdHM9InVzZXJTcGFjZU9uVXNlIiB4PSI4IiB5PSI2IiB3aWR0aD0iOSIgaGVpZ2h0PSIxMiI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMTYuMjA3MiAxNi41OUwxMS42NDk2IDEyTDE2LjIwNzIgNy40MUwxNC44MDQxIDZMOC44MzM1IDEyTDE0LjgwNDEgMThMMTYuMjA3MiAxNi41OVoiIGZpbGw9IndoaXRlIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvbWFzaz4KICAgICAgICAgICAgICAgICAgICAgICAgPGcgbWFzaz0idXJsKCNtYXNrUHJldikiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHJlY3QgeD0iMC41NzkxMDIiIHdpZHRoPSIyMy44ODIzIiBoZWlnaHQ9IjI0IiBmaWxsPSIjRkZGRkZGIj48L3JlY3Q+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgICAgICA8L3N2Zz4=)
:::

::: {.jp-swiper-button-next .swiper-button-next}
![](data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUiIGhlaWdodD0iMjQiIHZpZXdib3g9IjAgMCAyNSAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgICAgICAgICAgICAgICAgICAgICAgPG1hc2sgaWQ9Im1hc2tOZXh0IiBtYXNrLXR5cGU9ImFscGhhIiBtYXNrdW5pdHM9InVzZXJTcGFjZU9uVXNlIiB4PSI4IiB5PSI2IiB3aWR0aD0iOCIgaGVpZ2h0PSIxMiI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNOC41OTgxNCAxNi41OUwxMy4xNTU3IDEyTDguNTk4MTQgNy40MUwxMC4wMDEyIDZMMTUuOTcxOCAxMkwxMC4wMDEyIDE4TDguNTk4MTQgMTYuNTlaIiBmaWxsPSJ3aGl0ZSI+PC9wYXRoPgogICAgICAgICAgICAgICAgICAgICAgICA8L21hc2s+CiAgICAgICAgICAgICAgICAgICAgICAgIDxnIG1hc2s9InVybCgjbWFza05leHQpIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxyZWN0IHg9IjAuMzQzNzUiIHdpZHRoPSIyMy44ODIyIiBoZWlnaHQ9IjI0IiBmaWxsPSIjRkZGRkZGIj48L3JlY3Q+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgICAgICA8L3N2Zz4=)
:::
:::

::: jp-carousel-close-hint
![](data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUiIGhlaWdodD0iMjQiIHZpZXdib3g9IjAgMCAyNSAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgICAgICAgICAgICAgICAgICA8bWFzayBpZD0ibWFza0Nsb3NlIiBtYXNrLXR5cGU9ImFscGhhIiBtYXNrdW5pdHM9InVzZXJTcGFjZU9uVXNlIiB4PSI1IiB5PSI1IiB3aWR0aD0iMTUiIGhlaWdodD0iMTQiPgogICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMTkuMzE2NiA2LjQxTDE3LjkxMzUgNUwxMi4zNTA5IDEwLjU5TDYuNzg4MzQgNUw1LjM4NTI1IDYuNDFMMTAuOTQ3OCAxMkw1LjM4NTI1IDE3LjU5TDYuNzg4MzQgMTlMMTIuMzUwOSAxMy40MUwxNy45MTM1IDE5TDE5LjMxNjYgMTcuNTlMMTMuNzU0IDEyTDE5LjMxNjYgNi40MVoiIGZpbGw9IndoaXRlIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgPC9tYXNrPgogICAgICAgICAgICAgICAgICAgIDxnIG1hc2s9InVybCgjbWFza0Nsb3NlKSI+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZWN0IHg9IjAuNDA5NjY4IiB3aWR0aD0iMjMuODgyMyIgaGVpZ2h0PSIyNCIgZmlsbD0iI0ZGRkZGRiI+PC9yZWN0PgogICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgIDwvc3ZnPg==)
:::

::: jp-carousel-info
::: jp-carousel-info-footer
::: jp-carousel-pagination-container
::: {.jp-swiper-pagination .swiper-pagination}
:::

::: jp-carousel-pagination
:::
:::

::: jp-carousel-photo-title-container
##  {#section .jp-carousel-photo-caption}
:::

::: jp-carousel-photo-icons-container
[[
![](data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUiIGhlaWdodD0iMjQiIHZpZXdib3g9IjAgMCAyNSAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hc2sgaWQ9Im1hc2tJbmZvIiBtYXNrLXR5cGU9ImFscGhhIiBtYXNrdW5pdHM9InVzZXJTcGFjZU9uVXNlIiB4PSIyIiB5PSIyIiB3aWR0aD0iMjEiIGhlaWdodD0iMjAiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xMi43NTM3IDJDNy4yNjA3NiAyIDIuODAyNzMgNi40OCAyLjgwMjczIDEyQzIuODAyNzMgMTcuNTIgNy4yNjA3NiAyMiAxMi43NTM3IDIyQzE4LjI0NjYgMjIgMjIuNzA0NiAxNy41MiAyMi43MDQ2IDEyQzIyLjcwNDYgNi40OCAxOC4yNDY2IDIgMTIuNzUzNyAyWk0xMS43NTg2IDdWOUgxMy43NDg4VjdIMTEuNzU4NlpNMTEuNzU4NiAxMVYxN0gxMy43NDg4VjExSDExLjc1ODZaTTQuNzkyOTIgMTJDNC43OTI5MiAxNi40MSA4LjM2NTMxIDIwIDEyLjc1MzcgMjBDMTcuMTQyIDIwIDIwLjcxNDQgMTYuNDEgMjAuNzE0NCAxMkMyMC43MTQ0IDcuNTkgMTcuMTQyIDQgMTIuNzUzNyA0QzguMzY1MzEgNCA0Ljc5MjkyIDcuNTkgNC43OTI5MiAxMloiIGZpbGw9IndoaXRlIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbWFzaz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGcgbWFzaz0idXJsKCNtYXNrSW5mbykiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHJlY3QgeD0iMC44MTI1IiB3aWR0aD0iMjMuODgyMyIgaGVpZ2h0PSIyNCIgZmlsbD0iI0ZGRkZGRiI+PC9yZWN0PgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zdmc+)
]{.jp-carousel-icon}](#){.jp-carousel-icon-btn .jp-carousel-icon-info
aria-label="Toggle zichtbaarheid metagegevens van foto's"} [[
![](data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUiIGhlaWdodD0iMjQiIHZpZXdib3g9IjAgMCAyNSAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hc2sgaWQ9Im1hc2tDb21tZW50cyIgbWFzay10eXBlPSJhbHBoYSIgbWFza3VuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeD0iMiIgeT0iMiIgd2lkdGg9IjIxIiBoZWlnaHQ9IjIwIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNNC4zMjcxIDJIMjAuMjQ4NkMyMS4zNDMyIDIgMjIuMjM4OCAyLjkgMjIuMjM4OCA0VjE2QzIyLjIzODggMTcuMSAyMS4zNDMyIDE4IDIwLjI0ODYgMThINi4zMTcyOUwyLjMzNjkxIDIyVjRDMi4zMzY5MSAyLjkgMy4yMzI1IDIgNC4zMjcxIDJaTTYuMzE3MjkgMTZIMjAuMjQ4NlY0SDQuMzI3MVYxOEw2LjMxNzI5IDE2WiIgZmlsbD0id2hpdGUiPjwvcGF0aD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9tYXNrPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZyBtYXNrPSJ1cmwoI21hc2tDb21tZW50cykiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHJlY3QgeD0iMC4zNDY2OCIgd2lkdGg9IjIzLjg4MjMiIGhlaWdodD0iMjQiIGZpbGw9IiNGRkZGRkYiPjwvcmVjdD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3ZnPg==)
[]{.jp-carousel-has-comments-indicator
aria-label="Deze afbeelding heeft reacties."}
]{.jp-carousel-icon}](#){.jp-carousel-icon-btn
.jp-carousel-icon-comments
aria-label="Toggle zichtbaarheid foto reacties"}
:::
:::

::: jp-carousel-info-extra
::: jp-carousel-info-content-wrapper
::: jp-carousel-photo-title-container
##  {#section-1 .jp-carousel-photo-title}
:::

::: jp-carousel-comments-wrapper
::: {#jp-carousel-comments-loading}
Reacties laden\....
:::

::: jp-carousel-comments
:::

::: {#jp-carousel-comment-form-container}
[ ]{#jp-carousel-comment-form-spinner}

::: {#jp-carousel-comment-post-results}
:::

Laat een reactie achter\...

::: {#jp-carousel-comment-form-submit-and-info-wrapper}
::: {#jp-carousel-comment-form-commenting-as}
E-mailadres (Vereist)

Naam (Vereist)

Site
:::
:::
:::
:::

::: jp-carousel-image-meta
::: jp-carousel-title-and-caption
::: jp-carousel-photo-info
###  {#section-2 .jp-carousel-caption itemprop="caption description"}
:::

::: jp-carousel-photo-description
:::
:::

[![](data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUiIGhlaWdodD0iMjQiIHZpZXdib3g9IjAgMCAyNSAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hc2sgaWQ9Im1hc2swIiBtYXNrLXR5cGU9ImFscGhhIiBtYXNrdW5pdHM9InVzZXJTcGFjZU9uVXNlIiB4PSIzIiB5PSIzIiB3aWR0aD0iMTkiIGhlaWdodD0iMTgiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik01Ljg0NjE1IDVWMTlIMTkuNzc3NVYxMkgyMS43Njc3VjE5QzIxLjc2NzcgMjAuMSAyMC44NzIxIDIxIDE5Ljc3NzUgMjFINS44NDYxNUM0Ljc0MTU5IDIxIDMuODU1OTYgMjAuMSAzLjg1NTk2IDE5VjVDMy44NTU5NiAzLjkgNC43NDE1OSAzIDUuODQ2MTUgM0gxMi44MTE4VjVINS44NDYxNVpNMTQuODAyIDVWM0gyMS43Njc3VjEwSDE5Ljc3NzVWNi40MUw5Ljk5NTY5IDE2LjI0TDguNTkyNjEgMTQuODNMMTguMzc0NCA1SDE0LjgwMloiIGZpbGw9IndoaXRlIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbWFzaz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGcgbWFzaz0idXJsKCNtYXNrMCkiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHJlY3QgeD0iMC44NzA2MDUiIHdpZHRoPSIyMy44ODIzIiBoZWlnaHQ9IjI0IiBmaWxsPSIjRkZGRkZGIj48L3JlY3Q+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3N2Zz4=)
[]{.jp-carousel-download-text}]{.jp-carousel-image-download
target="_blank" style="display: none;"}

::: {.jp-carousel-image-map style="display: none;"}
:::
:::
:::
:::
:::
:::
:::

::: {#fb-root}
:::

::: {#sharing_email style="display: none;"}
Stuur naar e-mailadres Je naam Je e-mailadres
![loading](https://reactnieuws.net/wp-content/plugins/jetpack/modules/sharedaddy/images/loading.gif){.loading
style="float: right; display: none" width="16" height="16"}
[Annuleren](#cancel){.sharing_cancel rel="nofollow" role="button"}

::: {.errors .errors-1 style="display: none;"}
Bericht niet verstuurd - controleer je e-mailadres!
:::

::: {.errors .errors-2 style="display: none;"}
E-mail-controle mislukt, probeer het opnieuw
:::

::: {.errors .errors-3 style="display: none;"}
Helaas, je blog kan geen berichten per e-mail delen.
:::
:::

::: iframe
:::

::: {#likes-other-gravatars}
::: likes-text
%d bloggers liken dit:
:::
:::

![](data:image/svg+xml;base64,PHN2ZyBzdHlsZT0icG9zaXRpb246IGFic29sdXRlOyB3aWR0aDogMDsgaGVpZ2h0OiAwOyBvdmVyZmxvdzogaGlkZGVuOyIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj4KPGRlZnM+CjxzeW1ib2wgaWQ9Imljb24tNTAwcHgiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik02Ljk0MDI2LDE1LjE0MTJjLjAwNDM3LjAxMjEzLjEwOC4yOTg2Mi4xNjguNDQwNjRhNi41NTAwOCw2LjU1MDA4LDAsMSwwLDYuMDMxOTEtOS4wOTU1Nyw2LjY4NjU0LDYuNjg2NTQsMCwwLDAtMi41ODM1Ny41MTQ2N0E4LjUzOTE0LDguNTM5MTQsMCwwLDAsOC4yMTI2OCw4LjYxMzQ0TDguMjA5LDguNjE3MjVWMy4yMjk0OGw5LjA1MDQtLjAwMDA4Yy4zMjkzNC0uMDAzNi4zMjkzNC0uNDYzNTMuMzI5MzQtLjYxNDY2czAtLjYxMDkxLS4zMzAzNS0uNjE0NjdMNy40NzI0OCwyYS40My40MywwLDAsMC0uNDMxMzEuNDI2OTJ2Ny41ODM1NWMwLC4yNDQ2Ni4zMDQ3Ni40MjEzMS41ODc5My40ODE5LjU1My4xMTgxMi42ODA3NC0uMDU4NjQuODE2MTctLjI0NTdsLjAxOC0uMDI0ODFBMTAuNTI2NzMsMTAuNTI2NzMsMCwwLDEsOS4zMjI1OCw5LjI1OGE1LjM1MjY4LDUuMzUyNjgsMCwxLDEsNy41ODk4NSw3LjU0OTc2LDUuNDE3LDUuNDE3LDAsMCwxLTMuODA4NjcsMS41NjM2NSw1LjE3NDgzLDUuMTc0ODMsMCwwLDEtMi42OTgyMi0uNzQ0NzhsLjAwMzQyLTQuNjExMTFhMi43OTM3MiwyLjc5MzcyLDAsMCwxLC43MTM3Mi0xLjc4NzkyLDIuNjE2MTEsMi42MTYxMSwwLDAsMSwxLjk4MjgyLS44OTQ3NywyLjc1NjgzLDIuNzU2ODMsMCwwLDEsMS45NTUyNS43OTQ3NywyLjY2ODY3LDIuNjY4NjcsMCwwLDEsLjc5NjU2LDEuOTA5LDIuNzI0LDIuNzI0LDAsMCwxLTIuNzU4NDksMi43NDgsNC45NDY1MSw0Ljk0NjUxLDAsMCwxLS44NjI1NC0uMTM3MTljLS4zMTIzNC0uMDkzLS40NDUxOS4zNDA1OC0uNDg4OTIuNDgzNDktLjE2ODExLjU0OTY2LjA4NDUzLjY1ODYyLjEzNjg3LjY3NDg5YTMuNzU3NTEsMy43NTc1MSwwLDAsMCwxLjI1MjM0LjE4Mzc1LDMuOTQ2MzQsMy45NDYzNCwwLDEsMC0yLjgyNDQ0LTYuNzQyLDMuNjc0NzgsMy42NzQ3OCwwLDAsMC0xLjEzMDI4LDIuNTg0bC0uMDAwNDEuMDIzMjNjLS4wMDM1LjExNjY3LS4wMDU3OSwyLjg4MS0uMDA2NDQsMy43ODgxMWwtLjAwNDA3LS4wMDQ1MWE2LjE4NTIxLDYuMTg1MjEsMCwwLDEtMS4wODUxLTEuODYwOTJjLS4xMDU0NC0uMjc4NTYtLjM0MzU4LS4yMjkyNS0uNjY4NTctLjEyOTE3LS4xNDE5Mi4wNDM3Mi0uNTczODYuMTc2NzctLjQ3ODMzLjQ4OVptNC42NTE2NS0xLjA4MzM4YS41MTM0Ni41MTM0NiwwLDAsMCwuMTk1MTMuMzE4MThsLjAyMjc2LjAyMmEuNTI5NDUuNTI5NDUsMCwwLDAsLjM1MTcuMTg0MTYuMjQyNDIuMjQyNDIsMCwwLDAsLjE2NTc3LS4wNjExYy4wNTQ3My0uMDUwODIuNjczODItLjY3ODEyLjczMjg3LS43MzhsLjY5MDQxLjY4ODE5YS4yODk3OC4yODk3OCwwLDAsMCwuMjE0MzcuMTEwMzIuNTMyMzkuNTMyMzksMCwwLDAsLjM1NzA4LS4xOTQ4NmMuMjk3OTItLjMwNDE5LjE0ODg1LS40NjgyMS4wNzY3Ni0uNTQ3NTFsLS42OTk1NC0uNjk5NzUuNzI5NTItLjczNDY5Yy4xNi0uMTczMTEuMDE4NzQtLjM1NzA4LS4xMjIxOC0uNDk4LS4yMDQ2MS0uMjA0NjEtLjQwMi0uMjU3NDItLjUyODU1LS4xNDA4M2wtLjcyNTQuNzI2NjUtLjczMzU0LS43MzM3NWEuMjAxMjguMjAxMjgsMCwwLDAtLjE0MTc5LS4wNTY5NS41NDEzNS41NDEzNSwwLDAsMC0uMzQzNzkuMTk2NDhjLS4yMjU2MS4yMjU1NS0uMjc0LjM4MTQ5LS4xNTY1Ni41MDU5bC43MzM3NC43MzE1LS43Mjk0Mi43MzA3MkEuMjY1ODkuMjY1ODksMCwwLDAsMTEuNTkxOTEsMTQuMDU3ODJabTEuNTk4NjYtOS45MTVBOC44NjA4MSw4Ljg2MDgxLDAsMCwwLDkuODU0LDQuNzc2YS4yNjE2OS4yNjE2OSwwLDAsMC0uMTY5MzguMjI3NTkuOTI5NzguOTI5NzgsMCwwLDAsLjA4NjE5LjQyMDk0Yy4wNTY4Mi4xNDUyNC4yMDc3OS41MzEuNTAwMDYuNDE5NTVhOC40MDk2OSw4LjQwOTY5LDAsMCwxLDIuOTE5NjgtLjU1NDg0LDcuODc4NzUsNy44Nzg3NSwwLDAsMSwzLjA4Ni42MjI4Niw4LjYxODE3LDguNjE4MTcsMCwwLDEsMi4zMDU2MiwxLjQ5MzE1LjI3ODEuMjc4MSwwLDAsMCwuMTgzMTguMDc1ODZjLjE1NTI5LDAsLjMwNDI1LS4xNTI1My40MzE2Ny0uMjk1NTEuMjEyNjgtLjIzODYxLjM1ODczLS40MzY5LjE0OTItLjYzNTM4YTguNTA0MjUsOC41MDQyNSwwLDAsMC0yLjYyMzEyLTEuNjk0QTkuMDE3Nyw5LjAxNzcsMCwwLDAsMTMuMTkwNTgsNC4xNDI4M1pNMTkuNTA5NDUsMTguNjIzNmgwYS45MzE3MS45MzE3MSwwLDAsMC0uMzY2NDItLjI1NDA2LjI2NTg5LjI2NTg5LDAsMCwwLS4yNzYxMy4wNjYxM2wtLjA2OTQzLjA2OTI5QTcuOTA2MDYsNy45MDYwNiwwLDAsMSw3LjYwNjM5LDE4LjUwNWE3LjU3Mjg0LDcuNTcyODQsMCwwLDEtMS42OTYtMi41MTUzNyw4LjU4NzE1LDguNTg3MTUsMCwwLDEtLjUxNDctMS43Nzc1NGwtLjAwODcxLS4wNDg2NGMtLjA0OTM5LS4yNTg3My0uMjg3NTUtLjI3Njg0LS42Mjk4MS0uMjI0NDgtLjE0MjM0LjAyMTc4LS41NzU1LjA4OC0uNTM0MjYuMzk5NjlsLjAwMS4wMDcxMmE5LjA4ODA3LDkuMDg4MDcsMCwwLDAsMTUuNDA2LDQuOTkwOTRjLjAwMTkzLS4wMDE5Mi4wNDc1My0uMDQ3MTguMDcyNS0uMDc0MzZDMTkuNzk0MjUsMTkuMTYyMzQsMTkuODc0MjIsMTguOTg3MjgsMTkuNTA5NDUsMTguNjIzNloiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tYW1hem9uIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTMuNTgyLDguMTgyQzExLjkzNCw4LjM2Nyw5Ljc4LDguNDksOC4yMzgsOS4xNjZjLTEuNzgxLDAuNzY5LTMuMDMsMi4zMzctMy4wMyw0LjY0NCBjMCwyLjk1MywxLjg2LDQuNDI5LDQuMjUzLDQuNDI5YzIuMDIsMCwzLjEyNS0wLjQ3Nyw0LjY4NS0yLjA2NWMwLjUxNiwwLjc0NywwLjY4NSwxLjEwOSwxLjYyOSwxLjg5NCBjMC4yMTIsMC4xMTQsMC40ODMsMC4xMDMsMC42NzItMC4wNjZsMC4wMDYsMC4wMDZjMC41NjctMC41MDUsMS41OTktMS40MDEsMi4xOC0xLjg4OGMwLjIzMS0wLjE4OCwwLjE5LTAuNDk2LDAuMDA5LTAuNzU0IGMtMC41Mi0wLjcxOC0xLjA3Mi0xLjMwMy0xLjA3Mi0yLjYzNFY4LjMwNWMwLTEuODc2LDAuMTMzLTMuNTk5LTEuMjQ5LTQuODkxQzE1LjIzLDIuMzY5LDEzLjQyMiwyLDEyLjA0LDIgQzkuMzM2LDIsNi4zMTgsMy4wMSw1LjY4Niw2LjM1MUM1LjYxOCw2LjcwNiw1Ljg3Nyw2Ljg5Myw2LjEwOSw2Ljk0NWwyLjc1NCwwLjI5OEM5LjEyMSw3LjIzLDkuMzA4LDYuOTc3LDkuMzU3LDYuNzIgYzAuMjM2LTEuMTUxLDEuMi0xLjcwNiwyLjI4NC0xLjcwNmMwLjU4NCwwLDEuMjQ5LDAuMjE1LDEuNTk1LDAuNzM4YzAuMzk4LDAuNTg0LDAuMzQ2LDEuMzg0LDAuMzQ2LDIuMDYxVjguMTgyeiBNMTMuMDQ5LDE0LjA4OCBjLTAuNDUxLDAuOC0xLjE2OSwxLjI5MS0xLjk2NywxLjI5MWMtMS4wOSwwLTEuNzI4LTAuODMtMS43MjgtMi4wNjFjMC0yLjQyLDIuMTcxLTIuODYsNC4yMjctMi44NnYwLjYxNSBDMTMuNTgyLDEyLjE4MSwxMy42MDgsMTMuMTA0LDEzLjA0OSwxNC4wODh6IE0yMC42ODMsMTkuMzM5QzE4LjMyOSwyMS4wNzYsMTQuOTE3LDIyLDExLjk3OSwyMmMtNC4xMTgsMC03LjgyNi0xLjUyMi0xMC42MzItNC4wNTcgYy0wLjIyLTAuMTk5LTAuMDI0LTAuNDcxLDAuMjQxLTAuMzE3YzMuMDI3LDEuNzYyLDYuNzcxLDIuODIzLDEwLjYzOSwyLjgyM2MyLjYwOCwwLDUuNDc2LTAuNTQxLDguMTE1LTEuNjYgQzIwLjczOSwxOC42MiwyMS4wNzIsMTkuMDUxLDIwLjY4MywxOS4zMzl6IE0yMS4zMzYsMjEuMDQzYy0wLjE5NCwwLjE2My0wLjM3OSwwLjA3Ni0wLjI5My0wLjEzOSBjMC4yODQtMC43MSwwLjkyLTIuMjk4LDAuNjE5LTIuNjg0Yy0wLjMwMS0wLjM4Ni0xLjk5LTAuMTgzLTIuNzQ5LTAuMDkyYy0wLjIzLDAuMDI3LTAuMjY2LTAuMTczLTAuMDU5LTAuMzE5IGMxLjM0OC0wLjk0NiwzLjU1NS0wLjY3MywzLjgxMS0wLjM1NkMyMi45MjUsMTcuNzczLDIyLjU5OSwxOS45ODYsMjEuMzM2LDIxLjA0M3oiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tYXBwbGUiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0yMC4wNywxNy41ODZhMTAuODc0LDEwLjg3NCwwLDAsMS0xLjA3NSwxLjkzMyw5LjgyMiw5LjgyMiwwLDAsMS0xLjM4NSwxLjY3NCwyLjY4NywyLjY4NywwLDAsMS0xLjc4Ljc4NCw0LjQ2Miw0LjQ2MiwwLDAsMS0xLjY0NC0uMzkzLDQuNzE4LDQuNzE4LDAsMCwwLTEuNzctLjM5MSw0Ljg3OCw0Ljg3OCwwLDAsMC0xLjgyLjM5MUE0LjksNC45LDAsMCwxLDkuMDIxLDIyYTIuNTMsMi41MywwLDAsMS0xLjgyLS44QTEwLjMxNCwxMC4zMTQsMCwwLDEsNS43NTIsMTkuNDYsMTEuOTg3LDExLjk4NywwLDAsMSw0LjIyLDE2LjQxN2ExMS4xNDMsMTEuMTQzLDAsMCwxLS42NDMtMy42MjcsNi42MjMsNi42MjMsMCwwLDEsLjg3LTMuNDY1QTUuMSw1LjEsMCwwLDEsNi4yNjgsNy40ODNhNC45LDQuOSwwLDAsMSwyLjQ2My0uNjk1LDUuOCw1LjgsMCwwLDEsMS45LjQ0Myw2LjEyMyw2LjEyMywwLDAsMCwxLjUxMS40NDQsOS4wNCw5LjA0LDAsMCwwLDEuNjc1LS41MjMsNS41MzcsNS41MzcsMCwwLDEsMi4yNzctLjQsNC44MzUsNC44MzUsMCwwLDEsMy43ODgsMS45OTQsNC4yMTMsNC4yMTMsMCwwLDAtMi4yMzUsMy44MjcsNC4yMjIsNC4yMjIsMCwwLDAsMS4zODYsMy4xODEsNC41NTYsNC41NTYsMCwwLDAsMS4zODUuOTA5cS0uMTY3LjQ4My0uMzUzLjkyN1pNMTYuMjExLDIuNGE0LjI2Nyw0LjI2NywwLDAsMS0xLjA5NCwyLjgsMy43MjYsMy43MjYsMCwwLDEtMy4xLDEuNTI4QTMuMTE0LDMuMTE0LDAsMCwxLDEyLDYuMzQ3YTQuMzg0LDQuMzg0LDAsMCwxLDEuMTYtMi44MjgsNC40NjcsNC40NjcsMCwwLDEsMS40MTQtMS4wNjFBNC4yMTUsNC4yMTUsMCwwLDEsMTYuMTksMmEzLjYzMywzLjYzMywwLDAsMSwuMDIxLjRaIj48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLWJhbmRjYW1wIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTUuMjcgMTcuMjg5IDMgMTcuMjg5IDguNzMgNi43MTEgMjEgNi43MTEgMTUuMjcgMTcuMjg5Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLWJlaGFuY2UiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik03Ljc5OSw1LjY5OGMwLjU4OSwwLDEuMTIsMC4wNTEsMS42MDYsMC4xNTZjMC40ODIsMC4xMDIsMC44OTQsMC4yNzMsMS4yNDEsMC41MDdjMC4zNDQsMC4yMzUsMC42MTIsMC41NDYsMC44MDQsMC45MzggYzAuMTg4LDAuMzg3LDAuMjgxLDAuODcxLDAuMjgxLDEuNDQzYzAsMC42MTktMC4xNDEsMS4xMzctMC40MjEsMS41NTFjLTAuMjg0LDAuNDEzLTAuNywwLjc1MS0xLjI1NSwxLjAxNCBjMC43NTYsMC4yMTgsMS4zMTcsMC42MDEsMS42ODksMS4xNDZjMC4zNzQsMC41NDksMC41NTcsMS4yMDUsMC41NTcsMS45NzVjMCwwLjYyMy0wLjEyLDEuMTYxLTAuMzU5LDEuNjEyIGMtMC4yNDEsMC40NTctMC41NjksMC44MjgtMC45NzMsMS4xMTRjLTAuNDA4LDAuMjg4LTAuODc2LDAuNS0xLjM5OSwwLjYzN0M5LjA1MiwxNy45MzEsOC41MTQsMTgsNy45NjMsMThIMlY1LjY5OEg3Ljc5OSBNNy40NDksMTAuNjY4YzAuNDgxLDAsMC44NzgtMC4xMTQsMS4xOTItMC4zNDVjMC4zMTEtMC4yMjgsMC40NjMtMC42MDMsMC40NjMtMS4xMTljMC0wLjI4Ni0wLjA1MS0wLjUyMy0wLjE1Mi0wLjcwNyBDOC44NDgsOC4zMTUsOC43MTEsOC4xNzEsOC41MzYsOC4wN0M4LjM2Miw3Ljk2Niw4LjE2Niw3Ljg5NCw3Ljk0LDcuODU0Yy0wLjIyNC0wLjA0NC0wLjQ1Ny0wLjA2LTAuNjk3LTAuMDZINC43MDl2Mi44NzRINy40NDl6IE03LjYsMTUuOTA1YzAuMjY3LDAsMC41MjEtMC4wMjQsMC43NTktMC4wNzdjMC4yNDMtMC4wNTMsMC40NTctMC4xMzcsMC42MzctMC4yNjFjMC4xODItMC4xMiwwLjMzMi0wLjI4MywwLjQ0MS0wLjQ5MSBDOS41NDcsMTQuODcsOS42LDE0LjYwMiw5LjYsMTQuMjc4YzAtMC42MzMtMC4xOC0xLjA4NC0wLjUzMy0xLjM1N2MtMC4zNTYtMC4yNy0wLjgzLTAuNDA0LTEuNDEzLTAuNDA0SDQuNzA5djMuMzg4TDcuNiwxNS45MDV6IE0xNi4xNjIsMTUuODY0YzAuMzY3LDAuMzU4LDAuODk3LDAuNTM4LDEuNTgzLDAuNTM4YzAuNDkzLDAsMC45Mi0wLjEyNSwxLjI3Ny0wLjM3NGMwLjM1NC0wLjI0OCwwLjU3MS0wLjUxNCwwLjY1NC0wLjc5aDIuMTU1IGMtMC4zNDcsMS4wNzItMC44NzIsMS44MzgtMS41ODksMi4yOTlDMTkuNTM0LDE4LDE4LjY3LDE4LjIzLDE3LjY2MiwxOC4yM2MtMC43MDEsMC0xLjMzMi0wLjExMy0xLjg5OS0wLjMzNyBjLTAuNTY3LTAuMjI3LTEuMDQxLTAuNTQ0LTEuNDM5LTAuOTU4Yy0wLjM4OS0wLjQxNS0wLjY4OS0wLjkwNy0wLjkwNC0xLjQ4NGMtMC4yMTMtMC41NzQtMC4zMi0xLjIxLTAuMzItMS44OTkgYzAtMC42NjYsMC4xMS0xLjI4OCwwLjMyOS0xLjg2M2MwLjIyMi0wLjU3NywwLjUyOS0xLjA3NSwwLjkzMy0xLjQ5MmMwLjQwNi0wLjQyLDAuODg1LTAuNzUxLDEuNDQ0LTAuOTk0IGMwLjU1OC0wLjI0MSwxLjE3NS0wLjM2MywxLjg1Ny0wLjM2M2MwLjc1NCwwLDEuNDE0LDAuMTQ1LDEuOTgsMC40NGMwLjU2MywwLjI5MSwxLjAyNiwwLjY4NiwxLjM4OSwxLjE4MSBjMC4zNjMsMC40OTMsMC42MjIsMS4wNTcsMC43ODMsMS42OWMwLjE2LDAuNjMyLDAuMjE3LDEuMjkyLDAuMTcxLDEuOTgzaC02LjQyOEMxNS41NTcsMTQuODQsMTUuNzk1LDE1LjUwNiwxNi4xNjIsMTUuODY0IE0xOC45NzMsMTEuMTg0Yy0wLjI5MS0wLjMyMS0wLjc4My0wLjQ5Ni0xLjM4NC0wLjQ5NmMtMC4zOSwwLTAuNzE0LDAuMDY2LTAuOTczLDAuMmMtMC4yNTQsMC4xMzItMC40NjEsMC4yOTctMC42MjEsMC40OTEgYy0wLjE1NywwLjE5Ny0wLjI2NSwwLjQwNS0wLjMyOCwwLjYyOGMtMC4wNjMsMC4yMTctMC4xMDEsMC40MTMtMC4xMTEsMC41ODdoMy45OEMxOS40NzgsMTEuOTY5LDE5LjI2NSwxMS41MDksMTguOTczLDExLjE4NHogTTE1LjA1Nyw3LjczOGg0Ljk4NVY2LjUyNGgtNC45ODVMMTUuMDU3LDcuNzM4eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1ibG9nZ2VyIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTQuNzIyLDE0LjAxOWMwLDAuMzYxLTAuMjkzLDAuNjU0LTAuNjU0LDAuNjU0SDkuOTc3Yy0wLjM2MSwwLTAuNjU0LTAuMjkzLTAuNjU0LTAuNjU0czAuMjkzLTAuNjU0LDAuNjU0LTAuNjU0aDQuMDkxQzE0LjQyOSwxMy4zNjUsMTQuNzIyLDEzLjY1OCwxNC43MjIsMTQuMDE5eiBNOS45ODEsMTAuNjk4aDIuMDM4YzAuMzgyLDAsMC42OTItMC4zMSwwLjY5Mi0wLjY5MmMwLTAuMzgyLTAuMzEtMC42OTItMC42OTItMC42OTJIOS45ODFjLTAuMzgyLDAtMC42OTIsMC4zMS0wLjY5MiwwLjY5MkM5LjI4OSwxMC4zODgsOS41OTksMTAuNjk4LDkuOTgxLDEwLjY5OHogTTIxLDV2MTRjMCwxLjEwNS0wLjg5NSwyLTIsMkg1Yy0xLjEwNSwwLTItMC44OTUtMi0yVjVjMC0xLjEwNSwwLjg5NS0yLDItMmgxNEMyMC4xMDUsMywyMSwzLjg5NSwyMSw1eiBNMTcuNTQ0LDExLjM5YzAtMC4zOTgtMC4zMjItMC43Mi0wLjcyLTAuNzJoLTAuNjA3bC0wLjAxMywwLjAwMWMtMC4zOCwwLTAuNjkyLTAuMjk1LTAuNzE4LTAuNjY4bC0wLjAwMS0wLjAwOGMwLTEuOTg4LTEuNjExLTMuNTk5LTMuNTk5LTMuNTk5aC0xLjgxNmMtMS45ODgsMC0zLjU5OSwxLjYxMS0zLjU5OSwzLjU5OXYzLjk0N2MwLDEuOTg3LDEuNjExLDMuNTk5LDMuNTk5LDMuNTk5aDMuODc0YzEuOTg4LDAsMy41OTktMS42MTEsMy41OTktMy41OTlMMTcuNTQ0LDExLjM5eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1jaGFpbiIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTE5LjY0NywxNi43MDZhMS4xMzQsMS4xMzQsMCwwLDAtLjM0My0uODMzbC0yLjU0OS0yLjU0OWExLjEzNCwxLjEzNCwwLDAsMC0uODMzLS4zNDMsMS4xNjgsMS4xNjgsMCwwLDAtLjg4My4zOTJsLjIzMy4yMjZxLjIuMTg5LjI2NC4yNjRhMi45MjIsMi45MjIsMCwwLDEsLjE4NC4yMzMuOTg2Ljk4NiwwLDAsMSwuMTU5LjMxMiwxLjI0MiwxLjI0MiwwLDAsMSwuMDQzLjMzNywxLjE3MiwxLjE3MiwwLDAsMS0xLjE3NiwxLjE3NiwxLjIzNywxLjIzNywwLDAsMS0uMzM3LS4wNDMsMSwxLDAsMCwxLS4zMTItLjE1OSwyLjc2LDIuNzYsMCwwLDEtLjIzMy0uMTg0cS0uMDczLS4wNjgtLjI2NC0uMjY0bC0uMjI2LS4yMzNhMS4xOSwxLjE5LDAsMCwwLS40Ljg5NSwxLjEzNCwxLjEzNCwwLDAsMCwuMzQzLjgzM0wxNS44MzcsMTkuM2ExLjEzLDEuMTMsMCwwLDAsLjgzMy4zMzEsMS4xOCwxLjE4LDAsMCwwLC44MzMtLjMxOGwxLjgtMS43ODlhMS4xMiwxLjEyLDAsMCwwLC4zNDMtLjgyMVptLTguNjE1LTguNjRhMS4xMzQsMS4xMzQsMCwwLDAtLjM0My0uODMzTDguMTYzLDQuN2ExLjEzNCwxLjEzNCwwLDAsMC0uODMzLS4zNDMsMS4xODQsMS4xODQsMCwwLDAtLjgzMy4zMzFMNC43LDYuNDczYTEuMTIsMS4xMiwwLDAsMC0uMzQzLjgyMSwxLjEzNCwxLjEzNCwwLDAsMCwuMzQzLjgzM2wyLjU0OSwyLjU0OWExLjEzLDEuMTMsMCwwLDAsLjgzMy4zMzEsMS4xODQsMS4xODQsMCwwLDAsLjg4My0uMzhMOC43MjgsMTAuNHEtLjItLjE4OS0uMjY0LS4yNjRBMi45MjIsMi45MjIsMCwwLDEsOC4yOCw5LjlhLjk4Ni45ODYsMCwwLDEtLjE1OS0uMzEyLDEuMjQyLDEuMjQyLDAsMCwxLS4wNDMtLjMzN0ExLjE3MiwxLjE3MiwwLDAsMSw5LjI1NCw4LjA3OWExLjIzNywxLjIzNywwLDAsMSwuMzM3LjA0MywxLDEsMCwwLDEsLjMxMi4xNTksMi43NjEsMi43NjEsMCwwLDEsLjIzMy4xODRxLjA3My4wNjguMjY0LjI2NGwuMjI2LjIzM2ExLjE5LDEuMTksMCwwLDAsLjQtLjg5NVpNMjIsMTYuNzA2YTMuMzQzLDMuMzQzLDAsMCwxLTEuMDQyLDIuNDg4bC0xLjgsMS43ODlhMy41MzYsMy41MzYsMCwwLDEtNC45ODgtLjAyNWwtMi41MjUtMi41MzdhMy4zODQsMy4zODQsMCwwLDEtMS4wMTctMi40ODgsMy40NDgsMy40NDgsMCwwLDEsMS4wNzgtMi41NjFsLTEuMDc4LTEuMDc4YTMuNDM0LDMuNDM0LDAsMCwxLTIuNTQ5LDEuMDc4LDMuNCwzLjQsMCwwLDEtMi41LTEuMDI5TDMuMDI5LDkuNzk0QTMuNCwzLjQsMCwwLDEsMiw3LjI5NCwzLjM0MywzLjM0MywwLDAsMSwzLjA0Miw0LjgwNmwxLjgtMS43ODlBMy4zODQsMy4zODQsMCwwLDEsNy4zMzEsMmEzLjM1NywzLjM1NywwLDAsMSwyLjUsMS4wNDJsMi41MjUsMi41MzdhMy4zODQsMy4zODQsMCwwLDEsMS4wMTcsMi40ODgsMy40NDgsMy40NDgsMCwwLDEtMS4wNzgsMi41NjFsMS4wNzgsMS4wNzhhMy41NTEsMy41NTEsMCwwLDEsNS4wNDktLjA0OWwyLjU0OSwyLjU0OUEzLjQsMy40LDAsMCwxLDIyLDE2LjcwNloiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tY29kZXBlbiIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTIyLjAxNiw4Ljg0Yy0wLjAwMi0wLjAxMy0wLjAwNS0wLjAyNS0wLjAwNy0wLjAzN2MtMC4wMDUtMC4wMjUtMC4wMDgtMC4wNDgtMC4wMTUtMC4wNzIgYy0wLjAwMy0wLjAxNS0wLjAxLTAuMDI4LTAuMDEzLTAuMDQyYy0wLjAwOC0wLjAyLTAuMDE1LTAuMDQtMC4wMjMtMC4wNjJjLTAuMDA3LTAuMDE1LTAuMDEzLTAuMDI4LTAuMDItMC4wNDIgYy0wLjAwOC0wLjAyLTAuMDE4LTAuMDM3LTAuMDMtMC4wNTdjLTAuMDA3LTAuMDEzLTAuMDE3LTAuMDI3LTAuMDI1LTAuMDM4Yy0wLjAxMi0wLjAxOC0wLjAyMy0wLjAzNS0wLjAzNS0wLjA1MiBjLTAuMDEtMC4wMTMtMC4wMi0wLjAyNS0wLjAzLTAuMDM3Yy0wLjAxNS0wLjAxNy0wLjAyOC0wLjAzMi0wLjA0My0wLjA0NWMtMC4wMS0wLjAxMi0wLjAyMi0wLjAyMy0wLjAzNS0wLjAzNSBjLTAuMDE1LTAuMDE1LTAuMDMyLTAuMDI4LTAuMDQ4LTAuMDRjLTAuMDEyLTAuMDEtMC4wMjUtMC4wMi0wLjAzNy0wLjAzYy0wLjAwNS0wLjAwMy0wLjAxLTAuMDA4LTAuMDE1LTAuMDEybC05LjE2MS02LjA5NiBjLTAuMjg5LTAuMTkyLTAuNjY2LTAuMTkyLTAuOTU1LDBMMi4zNTksOC4yMzdDMi4zNTQsOC4yNCwyLjM0OSw4LjI0NSwyLjM0NCw4LjI0OUwyLjMwNiw4LjI3NyBjLTAuMDE3LDAuMDEzLTAuMDMzLDAuMDI3LTAuMDQ4LDAuMDRDMi4yNDYsOC4zMzEsMi4yMzQsOC4zNDIsMi4yMjIsOC4zNTJjLTAuMDE1LDAuMDE1LTAuMDI4LDAuMDMtMC4wNDIsMC4wNDcgYy0wLjAxMiwwLjAxMy0wLjAyMiwwLjAyMy0wLjAzLDAuMDM3QzIuMTM5LDguNDUzLDIuMTI1LDguNDcxLDIuMTE1LDguNDg4QzIuMTA3LDguNTAxLDIuMDk5LDguNTE0LDIuMDksOC41MjYgQzIuMDc5LDguNTQ4LDIuMDY5LDguNTY1LDIuMDYsOC41ODVDMi4wNTQsOC42LDIuMDQ3LDguNjEzLDIuMDQsOC42MjZDMi4wMzIsOC42NDgsMi4wMjUsOC42NywyLjAxOSw4LjY5IGMtMC4wMDUsMC4wMTMtMC4wMSwwLjAyNy0wLjAxMywwLjA0MkMxLjk5OSw4Ljc1NSwxLjk5NSw4Ljc3OCwxLjk5LDguODAzQzEuOTg5LDguODE3LDEuOTg1LDguODI4LDEuOTg0LDguODQgQzEuOTc4LDguODc5LDEuOTc1LDguOTE1LDEuOTc1LDguOTU0djYuMDkzYzAsMC4wMzcsMC4wMDMsMC4wNzUsMC4wMDgsMC4xMTJjMC4wMDIsMC4wMTIsMC4wMDUsMC4wMjUsMC4wMDcsMC4wMzggYzAuMDA1LDAuMDIzLDAuMDA4LDAuMDQ3LDAuMDE1LDAuMDcyYzAuMDAzLDAuMDE1LDAuMDA4LDAuMDI4LDAuMDEzLDAuMDRjMC4wMDcsMC4wMjIsMC4wMTMsMC4wNDIsMC4wMjIsMC4wNjMgYzAuMDA3LDAuMDE1LDAuMDEzLDAuMDI4LDAuMDIsMC4wNGMwLjAwOCwwLjAyLDAuMDE4LDAuMDM4LDAuMDMsMC4wNThjMC4wMDcsMC4wMTMsMC4wMTUsMC4wMjcsMC4wMjUsMC4wMzggYzAuMDEyLDAuMDE4LDAuMDIzLDAuMDM1LDAuMDM1LDAuMDUyYzAuMDEsMC4wMTMsMC4wMiwwLjAyNSwwLjAzLDAuMDM3YzAuMDEzLDAuMDE1LDAuMDI4LDAuMDMyLDAuMDQyLDAuMDQ1IGMwLjAxMiwwLjAxMiwwLjAyMywwLjAyMywwLjAzNSwwLjAzNWMwLjAxNSwwLjAxMywwLjAzMiwwLjAyOCwwLjA0OCwwLjA0bDAuMDM4LDAuMDNjMC4wMDUsMC4wMDMsMC4wMSwwLjAwNywwLjAxMywwLjAxIGw5LjE2Myw2LjA5NUMxMS42NjgsMjEuOTUzLDExLjgzMywyMiwxMiwyMmMwLjE2NywwLDAuMzMyLTAuMDQ3LDAuNDc4LTAuMTQ0bDkuMTYzLTYuMDk1bDAuMDE1LTAuMDEgYzAuMDEzLTAuMDEsMC4wMjctMC4wMiwwLjAzNy0wLjAzYzAuMDE4LTAuMDEzLDAuMDM1LTAuMDI4LDAuMDQ4LTAuMDRjMC4wMTMtMC4wMTIsMC4wMjUtMC4wMjMsMC4wMzUtMC4wMzUgYzAuMDE3LTAuMDE1LDAuMDMtMC4wMzIsMC4wNDMtMC4wNDVjMC4wMS0wLjAxMywwLjAyLTAuMDI1LDAuMDMtMC4wMzdjMC4wMTMtMC4wMTgsMC4wMjUtMC4wMzUsMC4wMzUtMC4wNTIgYzAuMDA4LTAuMDEzLDAuMDE4LTAuMDI3LDAuMDI1LTAuMDM4YzAuMDEyLTAuMDIsMC4wMjItMC4wMzgsMC4wMy0wLjA1OGMwLjAwNy0wLjAxMywwLjAxMy0wLjAyNywwLjAyLTAuMDQgYzAuMDA4LTAuMDIyLDAuMDE1LTAuMDQyLDAuMDIzLTAuMDYzYzAuMDAzLTAuMDEzLDAuMDEtMC4wMjcsMC4wMTMtMC4wNGMwLjAwNy0wLjAyNSwwLjAxLTAuMDQ4LDAuMDE1LTAuMDcyIGMwLjAwMi0wLjAxMywwLjAwNS0wLjAyNywwLjAwNy0wLjAzN2MwLjAwMy0wLjA0MiwwLjAwNy0wLjA3OSwwLjAwNy0wLjExN1Y4Ljk1NEMyMi4wMjUsOC45MTUsMjIuMDIyLDguODc5LDIyLjAxNiw4Ljg0eiBNMTIuODYyLDQuNDY0bDYuNzUxLDQuNDlsLTMuMDE2LDIuMDEzbC0zLjczNS0yLjQ5MlY0LjQ2NHogTTExLjEzOCw0LjQ2NHY0LjAwOWwtMy43MzUsMi40OTRMNC4zODksOC45NTRMMTEuMTM4LDQuNDY0eiBNMy42OTksMTAuNTYyTDUuODUzLDEybC0yLjE1NSwxLjQzOFYxMC41NjJ6IE0xMS4xMzgsMTkuNTM2bC02Ljc0OS00LjQ5MWwzLjAxNS0yLjAxMWwzLjczNSwyLjQ5MlYxOS41MzZ6IE0xMiwxNC4wMzVMOC45NTMsMTIgTDEyLDkuOTY2TDE1LjA0NywxMkwxMiwxNC4wMzV6IE0xMi44NjIsMTkuNTM2di00LjAwOWwzLjczNS0yLjQ5MmwzLjAxNiwyLjAxMUwxMi44NjIsMTkuNTM2eiBNMjAuMzAzLDEzLjQzOEwxOC4xNDcsMTIgbDIuMTU2LTEuNDM4TDIwLjMwMywxMy40Mzh6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLWRldmlhbnRhcnQiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0gMTguMTkgNS42MzYgMTguMTkgMiAxOC4xODggMiAxNC41NTMgMiAxNC4xOSAyLjM2NiAxMi40NzQgNS42MzYgMTEuOTM1IDYgNS44MSA2IDUuODEgMTAuOTk0IDkuMTc3IDEwLjk5NCA5LjQ3NyAxMS4zNTcgNS44MSAxOC4zNjMgNS44MSAyMiA1LjgxMSAyMiA5LjQ0NyAyMiA5LjgxIDIxLjYzNCAxMS41MjYgMTguMzY0IDEyLjA2NSAxOCAxOC4xOSAxOCAxOC4xOSAxMy4wMDYgMTQuODIzIDEzLjAwNiAxNC41MjMgMTIuNjQxIDE4LjE5IDUuNjM2eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1kaWdnIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNNC41LDUuNGgyLjJWMTZIMVY4LjVoMy41VjUuNEw0LjUsNS40eiBNNC41LDE0LjJ2LTRIMy4ydjRINC41eiBNNy42LDguNVYxNmgyLjJWOC41QzkuOCw4LjUsNy42LDguNSw3LjYsOC41eiBNNy42LDUuNCB2Mi4yaDIuMlY1LjRDOS44LDUuNCw3LjYsNS40LDcuNiw1LjR6IE0xMC43LDguNWg1Ljd2MTAuMWgtNS43di0xLjhoMy41VjE2aC0zLjVDMTAuNywxNiwxMC43LDguNSwxMC43LDguNXogTTE0LjIsMTQuMnYtNGgtMS4zdjQgSDE0LjJ6IE0xNy4zLDguNUgyM3YxMC4xaC01Ljd2LTEuOGgzLjVWMTZoLTMuNUMxNy4zLDE2LDE3LjMsOC41LDE3LjMsOC41eiBNMjAuOCwxNC4ydi00aC0xLjN2NEgyMC44eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1kaXNjb3JkIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTAuMjI3IDkuOTU3Yy0uNTU5IDAtMSAuNDgtMSAxLjA2MyAwIC41ODUuNDUzIDEuMDY2IDEgMS4wNjYuNTU4IDAgMS0uNDggMS0xLjA2Ni4wMDctLjU4Mi0uNDQyLTEuMDYzLTEtMS4wNjN6bTMuNTc0IDBjLS41NTkgMC0uOTk2LjQ4LS45OTYgMS4wNjMgMCAuNTg1LjQ0OSAxLjA2Ni45OTYgMS4wNjYuNTU4IDAgMS0uNDggMS0xLjA2NiAwLS41ODItLjQ0Mi0xLjA2My0xLTEuMDYzem0wIDAgTTE4LjU2MyAxLjkxOEg1LjQzOGMtMS4xMSAwLTIuMDA4Ljg3OS0yLjAwOCAxLjk3M3YxMi45NTdjMCAxLjA5My44OTggMS45NzIgMi4wMDcgMS45NzJoMTEuMTFsLS41Mi0xLjc3MyAxLjI1NCAxLjE0IDEuMTg0IDEuMDc1IDIuMTA1IDEuODJWMy44OTFjMC0xLjA5NC0uODk4LTEuOTczLTIuMDA4LTEuOTczek0xNC43OCAxNC40MzRzLS4zNTEtLjQxNC0uNjQ0LS43NzhjMS4yODEtLjM1NSAxLjc3My0xLjE0IDEuNzczLTEuMTRhNS43NDUgNS43NDUgMCAwIDEtMS4xMjkuNTY2Yy0uNDg4LjItLjk2LjMzNi0xLjQxOC40MWE3LjA3IDcuMDcgMCAwIDEtMi41MzktLjAwOCA4LjEzMyA4LjEzMyAwIDAgMS0xLjQ0MS0uNDE0IDYuMjE5IDYuMjE5IDAgMCAxLS43MTUtLjMyNGMtLjAyNy0uMDItLjA1OS0uMDI3LS4wODYtLjA0N2EuMTEzLjExMyAwIDAgMS0uMDM5LS4wMzFjLS4xNzYtLjA5NC0uMjczLS4xNi0uMjczLS4xNnMuNDY4Ljc2NSAxLjcxIDEuMTI5Yy0uMjkzLjM2My0uNjU2Ljc5Ny0uNjU2Ljc5Ny0yLjE2NC0uMDY3LTIuOTg0LTEuNDU3LTIuOTg0LTEuNDU3IDAtMy4wODYgMS40MS01LjU4NiAxLjQxLTUuNTg2IDEuNDEtMS4wMzYgMi43NS0xLjAwOCAyLjc1LTEuMDA4bC4wOTguMTEzYy0xLjc2Mi41LTIuNTc1IDEuMjU4LTIuNTc1IDEuMjU4cy4yMTUtLjExNy41NzktLjI3N2MxLjA0Ni0uNDU0IDEuODc4LS41NzkgMi4yMjItLjYwNi4wNTktLjAwOC4xMS0uMDIuMTY4LS4wMmE4LjcyOCA4LjcyOCAwIDAgMSAxLjk3Ny0uMDE5Yy45MzMuMTA2IDEuOTMuMzc1IDIuOTQ5LjkyMiAwIDAtLjc3My0uNzE5LTIuNDM4LTEuMjE5bC4xMzctLjE1MnMxLjM0LS4wMjggMi43NSAxLjAwOGMwIDAgMS40MTQgMi41IDEuNDE0IDUuNTg2IDAgMC0uODM2IDEuMzktMyAxLjQ1N3ptMCAwIj48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLWRyaWJiYmxlIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTIsMjJDNi40ODYsMjIsMiwxNy41MTQsMiwxMlM2LjQ4NiwyLDEyLDJjNS41MTQsMCwxMCw0LjQ4NiwxMCwxMFMxNy41MTQsMjIsMTIsMjJ6IE0yMC40MzQsMTMuMzY5IGMtMC4yOTItMC4wOTItMi42NDQtMC43OTQtNS4zMi0wLjM2NWMxLjExNywzLjA3LDEuNTcyLDUuNTcsMS42NTksNi4wOUMxOC42ODksMTcuNzk4LDIwLjA1MywxNS43NDUsMjAuNDM0LDEzLjM2OXogTTE1LjMzNiwxOS44NzZjLTAuMTI3LTAuNzQ5LTAuNjIzLTMuMzYxLTEuODIyLTYuNDc3Yy0wLjAxOSwwLjAwNi0wLjAzOCwwLjAxMy0wLjA1NiwwLjAxOWMtNC44MTgsMS42NzktNi41NDcsNS4wMi02LjcwMSw1LjMzNCBjMS40NDgsMS4xMjksMy4yNjgsMS44MDMsNS4yNDMsMS44MDNDMTMuMTgzLDIwLjU1NSwxNC4zMTEsMjAuMzEzLDE1LjMzNiwxOS44NzZ6IE01LjY1NCwxNy43MjQgYzAuMTkzLTAuMzMxLDIuNTM4LTQuMjEzLDYuOTQzLTUuNjM3YzAuMTExLTAuMDM2LDAuMjI0LTAuMDcsMC4zMzctMC4xMDJjLTAuMjE0LTAuNDg1LTAuNDQ4LTAuOTcxLTAuNjkyLTEuNDUgYy00LjI2NiwxLjI3Ny04LjQwNSwxLjIyMy04Ljc3OCwxLjIxNmMtMC4wMDMsMC4wODctMC4wMDQsMC4xNzQtMC4wMDQsMC4yNjFDMy40NTgsMTQuMjA3LDQuMjksMTYuMjEsNS42NTQsMTcuNzI0eiBNMy42MzksMTAuMjY0IGMwLjM4MiwwLjAwNSwzLjkwMSwwLjAyLDcuODk3LTEuMDQxYy0xLjQxNS0yLjUxNi0yLjk0Mi00LjYzMS0zLjE2Ny00Ljk0QzUuOTc5LDUuNDEsNC4xOTMsNy42MTMsMy42MzksMTAuMjY0eiBNOS45OTgsMy43MDkgYzAuMjM2LDAuMzE2LDEuNzg3LDIuNDI5LDMuMTg3LDVjMy4wMzctMS4xMzgsNC4zMjMtMi44NjcsNC40NzctMy4wODVDMTYuMTU0LDQuMjg2LDE0LjE3LDMuNDcxLDEyLDMuNDcxIEMxMS4zMTEsMy40NzEsMTAuNjQxLDMuNTU0LDkuOTk4LDMuNzA5eiBNMTguNjEyLDYuNjEyQzE4LjQzMiw2Ljg1NSwxNyw4LjY5LDEzLjg0Miw5Ljk3OWMwLjE5OSwwLjQwNywwLjM4OSwwLjgyMSwwLjU2NywxLjIzNyBjMC4wNjMsMC4xNDgsMC4xMjQsMC4yOTUsMC4xODQsMC40NDFjMi44NDItMC4zNTcsNS42NjYsMC4yMTUsNS45NDgsMC4yNzVDMjAuNTIyLDkuOTE2LDE5LjgwMSw4LjA2NSwxOC42MTIsNi42MTJ6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLWRyb3Bib3giIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xMiw2LjEzNEw2LjA2OSw5Ljc5N0wyLDYuNTRsNS44ODMtMy44NDNMMTIsNi4xMzR6IE0yLDEzLjA1NGw1Ljg4MywzLjg0M0wxMiwxMy40NTlMNi4wNjksOS43OTdMMiwxMy4wNTR6IE0xMiwxMy40NTkgbDQuMTE2LDMuNDM5TDIyLDEzLjA1NGwtNC4wNjktMy4yNTdMMTIsMTMuNDU5eiBNMjIsNi41NGwtNS44ODQtMy44NDNMMTIsNi4xMzRsNS45MzEsMy42NjNMMjIsNi41NHogTTEyLjAxMSwxNC4ybC00LjEyOSwzLjQyNiBsLTEuNzY3LTEuMTUzdjEuMjkxbDUuODk2LDMuNTM5bDUuODk3LTMuNTM5di0xLjI5MWwtMS43NjksMS4xNTNMMTIuMDExLDE0LjJ6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLWV0c3kiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik05LjE2MDMzLDQuMDM4YzAtLjI3MTc0LjAyNzE3LS40MzQ3OC40ODkxMy0uNDM0NzhoNi4yMjI4M2MxLjA4NywwLDEuNjg0NzguOTIzOTEsMi4xMTk1NywyLjY2M2wuMzUzMjYsMS4zODU4N2gxLjA1OTc4QzE5LjU5NTExLDMuNzEyLDE5Ljc1ODE1LDIsMTkuNzU4MTUsMnMtMi42NjMuMjk4OTEtNC4yMzkxMy4yOTg5MWgtNy45NjJMMy4yOTA3NiwyLjE2M3YxLjE0MTNMNC43MzEsMy41NzYwOWMxLjAwNTQzLjE5MDIyLDEuMjUuNDA3NjEsMS4zMzE1MiwxLjMzMTUyLDAsMCwuMDgxNTIsMi43MTczOS4wODE1Miw3LjIwMTA5cy0uMDgxNTIsNy4xNzM5MS0uMDgxNTIsNy4xNzM5MWMwLC44MTUyMi0uMzI2MDksMS4xMTQxMy0xLjMzMTUyLDEuMzA0MzVsLTEuNDQwMjIuMjcxNzRWMjJsNC4yNjYzLS4xMzU4N2g3LjExOTU3YzEuNjAzMjYsMCw1LjMyNjA5LjEzNTg3LDUuMzI2MDkuMTM1ODcuMDgxNTItLjk3ODI2LjYyNS01LjQwNzYxLjcwNjUyLTUuODk2NzRIMTkuNzAzOEwxOC42NDQsMTguNTIxNzRjLS44NDIzOSwxLjkwMjE3LTIuMDY1MjIsMi4wMzgtMy40MjM5MSwyLjAzOEgxMS4xNzEyYy0xLjM1ODcsMC0yLjAxMDg3LS41NDM0OC0yLjAxMDg3LTEuNzEyVjEyLjY1MjE3czMuMDE2MywwLDMuOTk0NTcuMDgxNTJjLjc2MDg3LjA1NDM1LDEuMjIyODMuMjcxNzQsMS40NjczOSwxLjMzMTUybC4zMjYwOSwxLjQxM2gxLjE2ODQ4bC0uMDgxNTItMy41NTk3OC4xNjMtMy41ODdIMTUuMDI5ODlsLS4zODA0MywxLjU3NjA5Yy0uMjQ0NTcsMS4wMzI2MS0uNDA3NjEsMS4yMjI4My0xLjQ2NzM5LDEuMzMxNTItMS4zODU4Ny4xMzU4Ny00LjAyMTc0LjEwODctNC4wMjE3NC4xMDg3WiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1ldmVudGJyaXRlIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7IiBkPSJNMTguMDQxLDMuOTMxTDUuOTU5LDNDNC4zMjUsMywzLDQuMzI1LDMsNS45NTl2MTIuMDgzQzMsMTkuNjc1LDQuMzI1LDIxLDUuOTU5LDIxbDEyLjA4My0wLjkzMUMxOS42OTksMTkuOTgzLDIxLDE4Ljc0NCwyMSwxNy4xMVY2Ljg5QzIxLDUuMjU2LDE5Ljc0MSw0LjAyNywxOC4wNDEsMy45MzF6TTE2LjkzMyw4LjE3Yy0wLjA4MiwwLjIxNS0wLjE5MiwwLjQzMi0wLjM3OCwwLjU1MWMtMC4xODgsMC4xMjItMC40ODksMC4xMzItMC43OTksMC4xMzJjLTEuNTIxLDAtMy4wNjItMC4wNDgtNC42MDctMC4wNDhjLTAuMTUyLDAuNzA4LTAuMzA0LDEuNDE2LTAuNDUxLDIuMTI4YzAuOTMyLTAuMDA0LDEuODczLDAuMDA1LDIuODEsMC4wMDVjMC43MjYsMCwxLjQ2Mi0wLjA2OSwxLjU4NiwwLjUyNWMwLjA0LDAuMTg5LTAuMDAxLDAuNDI2LTAuMDUyLDAuNjE1Yy0wLjEwNSwwLjM4LTAuMjU4LDAuNjc2LTAuNjI1LDAuNzgzYy0wLjE4NSwwLjA1NC0wLjQwOCwwLjA1OC0wLjY0NiwwLjA1OGMtMS4xNDUsMC0yLjM0NSwwLjAxNy0zLjQ5MywwLjAyYy0wLjE2OSwwLjc3Mi0wLjMyOCwxLjU1My0wLjQ4OSwyLjMzM2MxLjU3LTAuMDA1LDMuMDY3LTAuMDQxLDQuNjMzLTAuMDU4YzAuNjI3LTAuMDA3LDEuMDg1LDAuMTk0LDEuMDA5LDAuODVjLTAuMDMxLDAuMjYyLTAuMDk4LDAuNDk3LTAuMjExLDAuNzI1Yy0wLjEwMiwwLjIwOC0wLjI0OCwwLjM3Ni0wLjQ4OCwwLjQ1MmMtMC4yMzcsMC4wNzUtMC41NDEsMC4wNjQtMC44NjIsMC4wNzhjLTAuMzA0LDAuMDE0LTAuNjE0LDAuMDA4LTAuOTI0LDAuMDE2Yy0wLjMwOSwwLjAwOS0wLjYxOSwwLjAyMi0wLjkxOSwwLjAyMmMtMS4yNTMsMC0yLjQyOSwwLjA4LTMuNjgzLDAuMDczYy0wLjYwMy0wLjAwNC0xLjAxNC0wLjI0OS0xLjEyNC0wLjc1N2MtMC4wNTktMC4yNzMtMC4wMTgtMC41OCwwLjAzNi0wLjg0MWMwLjU0MS0yLjU5MiwxLjA4My01LjE3NiwxLjYyOS03Ljc2M2MwLjA1Ni0wLjI2NSwwLjExNC0wLjUxMSwwLjIyNS0wLjcxNEM5LjI3OSw3LjA1MSw5LjUzNCw2LjgzNCw5LjksNi43MzVjMC4zNjgtMC4wOTksMC44ODMtMC4wNDcsMS4zNDQtMC4wNDdjMC4zMDUsMCwwLjYxMiwwLjAwOCwwLjkxNCwwLjAxNmMwLjkyNSwwLjAyNiwxLjgxNywwLjAzLDIuNzQ3LDAuMDUzYzAuMzA0LDAuMDA3LDAuNjE1LDAuMDE2LDAuOTE1LDAuMDE2YzAuNjIxLDAsMS4xNywwLjA3MywxLjI0NSwwLjYxNEMxNy4xMDQsNy42NzUsMTcuMDE0LDcuOTU0LDE2LjkzMyw4LjE3eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1mYWNlYm9vayIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTEyLDJDNi41LDIsMiw2LjUsMiwxMmMwLDUsMy43LDkuMSw4LjQsOS45di03SDcuOVYxMmgyLjVWOS44YzAtMi41LDEuNS0zLjksMy44LTMuOWMxLjEsMCwyLjIsMC4yLDIuMiwwLjJ2Mi41aC0xLjMgYy0xLjIsMC0xLjYsMC44LTEuNiwxLjZWMTJoMi44bC0wLjQsMi45aC0yLjN2N0MxOC4zLDIxLjEsMjIsMTcsMjIsMTJDMjIsNi41LDE3LjUsMiwxMiwyeiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1mZWVkIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMiw4LjY2N1YxMmM1LjUxNSwwLDEwLDQuNDg1LDEwLDEwaDMuMzMzQzE1LjMzMywxNC42MzcsOS4zNjMsOC42NjcsMiw4LjY2N3ogTTIsMnYzLjMzMyBjOS4xOSwwLDE2LjY2Nyw3LjQ3NywxNi42NjcsMTYuNjY3SDIyQzIyLDEwLjk1NSwxMy4wNDUsMiwyLDJ6IE00LjUsMTdDMy4xMTgsMTcsMiwxOC4xMiwyLDE5LjVTMy4xMTgsMjIsNC41LDIyUzcsMjAuODgsNywxOS41IFM1Ljg4MiwxNyw0LjUsMTd6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLWZsaWNrciIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTYuNSw3Yy0yLjc1LDAtNSwyLjI1LTUsNXMyLjI1LDUsNSw1czUtMi4yNSw1LTVTOS4yNSw3LDYuNSw3eiBNMTcuNSw3Yy0yLjc1LDAtNSwyLjI1LTUsNXMyLjI1LDUsNSw1czUtMi4yNSw1LTUgUzIwLjI1LDcsMTcuNSw3eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1mb3Vyc3F1YXJlIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTcuNTczLDJjMCwwLTkuMTk3LDAtMTAuNjY4LDBTNSwzLjEwNyw1LDMuODA1czAsMTYuOTQ4LDAsMTYuOTQ4YzAsMC43ODUsMC40MjIsMS4wNzcsMC42NiwxLjE3MiBjMC4yMzgsMC4wOTcsMC44OTIsMC4xNzcsMS4yODUtMC4yNzVjMCwwLDUuMDM1LTUuODQzLDUuMTIyLTUuOTNjMC4xMzItMC4xMzIsMC4xMzItMC4xMzIsMC4yNjItMC4xMzJoMy4yNiBjMS4zNjgsMCwxLjU4OC0wLjk3NywxLjczMi0xLjU1MmMwLjA3OC0wLjMxOCwwLjY5Mi0zLjQyOCwxLjIyNS02LjEyMmwwLjY3NS0zLjM2OEMxOS41NiwyLjg5MywxOS4xNCwyLDE3LjU3MywyeiBNMTYuNDk1LDcuMjIgYy0wLjA1MywwLjI1Mi0wLjM3MiwwLjUxOC0wLjY2NSwwLjUxOGMtMC4yOTMsMC00LjE1NywwLTQuMTU3LDBjLTAuNDY3LDAtMC44MDIsMC4zMTgtMC44MDIsMC43ODd2MC41MDggYzAsMC40NjcsMC4zMzcsMC43OTgsMC44MDUsMC43OThjMCwwLDMuMTk3LDAsMy41MjgsMHMwLjY1NSwwLjM2MiwwLjU4MywwLjcxNWMtMC4wNzIsMC4zNTMtMC40MDcsMi4xMDItMC40NDgsMi4yOTUgYy0wLjA0LDAuMTkzLTAuMjYyLDAuNTIzLTAuNjU1LDAuNTIzYy0wLjMzLDAtMi44OCwwLTIuODgsMGMtMC41MjMsMC0wLjY4MywwLjA2OC0xLjAzMywwLjUwMyBjLTAuMzUsMC40MzctMy41MDUsNC4yMjMtMy41MDUsNC4yMjNjLTAuMDMyLDAuMDM1LTAuMDYzLDAuMDI3LTAuMDYzLTAuMDE1VjQuODUyYzAtMC4yOTgsMC4yNi0wLjY0OCwwLjY0OC0wLjY0OCBjMCwwLDguMjI4LDAsOC41NjIsMGMwLjMxNSwwLDAuNjEsMC4yOTcsMC41MjgsMC42ODNMMTYuNDk1LDcuMjJ6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLWdob3N0IiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTAuMjAzLDIwLjk5N0gzLjAwNXYtMy41OTloNy4xOThWMjAuOTk3eiBNMjAuOTk1LDE3LjM5OGgtNy4xOTN2My41OTloNy4xOTNWMTcuMzk4eiBNMjAuOTk4LDEwLjJIM3YzLjU5OWgxNy45OThWMTAuMnpNMTMuODAzLDMuMDAzSDMuMDA1djMuNTk5aDEwLjc5OFYzLjAwM3ogTTIxLDMuMDAzaC0zLjU5OXYzLjU5OUgyMVYzLjAwM3oiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tZ29vZHJlYWRzIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTcuMywxNy41Yy0wLjIsMC44LTAuNSwxLjQtMSwxLjljLTAuNCwwLjUtMSwwLjktMS43LDEuMkMxMy45LDIwLjksMTMuMSwyMSwxMiwyMWMtMC42LDAtMS4zLTAuMS0xLjktMC4yIGMtMC42LTAuMS0xLjEtMC40LTEuNi0wLjdjLTAuNS0wLjMtMC45LTAuNy0xLjItMS4yYy0wLjMtMC41LTAuNS0xLjEtMC41LTEuN2gxLjVjMC4xLDAuNSwwLjIsMC45LDAuNSwxLjIgYzAuMiwwLjMsMC41LDAuNiwwLjksMC44YzAuMywwLjIsMC43LDAuMywxLjEsMC40YzAuNCwwLjEsMC44LDAuMSwxLjIsMC4xYzEuNCwwLDIuNS0wLjQsMy4xLTEuMmMwLjYtMC44LDEtMiwxLTMuNXYtMS43aDAgYy0wLjQsMC44LTAuOSwxLjQtMS42LDEuOWMtMC43LDAuNS0xLjUsMC43LTIuNCwwLjdjLTEsMC0xLjktMC4yLTIuNi0wLjVDOC43LDE1LDguMSwxNC41LDcuNywxNGMtMC41LTAuNi0wLjgtMS4zLTEtMi4xIGMtMC4yLTAuOC0wLjMtMS42LTAuMy0yLjVjMC0wLjksMC4xLTEuNywwLjQtMi41YzAuMy0wLjgsMC42LTEuNSwxLjEtMmMwLjUtMC42LDEuMS0xLDEuOC0xLjRDMTAuMywzLjIsMTEuMSwzLDEyLDMgYzAuNSwwLDAuOSwwLjEsMS4zLDAuMmMwLjQsMC4xLDAuOCwwLjMsMS4xLDAuNWMwLjMsMC4yLDAuNiwwLjUsMC45LDAuOGMwLjMsMC4zLDAuNSwwLjYsMC42LDFoMFYzLjRoMS41VjE1IEMxNy42LDE1LjksMTcuNSwxNi43LDE3LjMsMTcuNXogTTEzLjgsMTQuMWMwLjUtMC4zLDAuOS0wLjcsMS4zLTEuMWMwLjMtMC41LDAuNi0xLDAuOC0xLjZjMC4yLTAuNiwwLjMtMS4yLDAuMy0xLjkgYzAtMC42LTAuMS0xLjItMC4yLTEuOWMtMC4xLTAuNi0wLjQtMS4yLTAuNy0xLjdjLTAuMy0wLjUtMC43LTAuOS0xLjMtMS4yYy0wLjUtMC4zLTEuMS0wLjUtMS45LTAuNXMtMS40LDAuMi0xLjksMC41IGMtMC41LDAuMy0xLDAuNy0xLjMsMS4yQzguNSw2LjQsOC4zLDcsOC4xLDcuNkM4LDguMiw3LjksOC45LDcuOSw5LjVjMCwwLjYsMC4xLDEuMywwLjIsMS45QzguMywxMiw4LjYsMTIuNSw4LjksMTMgYzAuMywwLjUsMC44LDAuOCwxLjMsMS4xYzAuNSwwLjMsMS4xLDAuNCwxLjksMC40QzEyLjcsMTQuNSwxMy4zLDE0LjQsMTMuOCwxNC4xeiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1nb29nbGUiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xMi4wMiwxMC4xOHYzLjcydjAuMDFoNS41MWMtMC4yNiwxLjU3LTEuNjcsNC4yMi01LjUsNC4yMmMtMy4zMSwwLTYuMDEtMi43NS02LjAxLTYuMTJzMi43LTYuMTIsNi4wMS02LjEyIGMxLjg3LDAsMy4xMywwLjgsMy44NSwxLjQ4bDIuODQtMi43NkMxNi45OSwyLjk5LDE0LjczLDIsMTIuMDMsMmMtNS41MiwwLTEwLDQuNDgtMTAsMTBzNC40OCwxMCwxMCwxMGM1Ljc3LDAsOS42LTQuMDYsOS42LTkuNzcgYzAtMC44My0wLjExLTEuNDItMC4yNS0yLjA1SDEyLjAyeiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1naXRodWIiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xMiwyQzYuNDc3LDIsMiw2LjQ3NywyLDEyYzAsNC40MTksMi44NjUsOC4xNjYsNi44MzksOS40ODljMC41LDAuMDksMC42ODItMC4yMTgsMC42ODItMC40ODQgYzAtMC4yMzYtMC4wMDktMC44NjYtMC4wMTQtMS42OTljLTIuNzgyLDAuNjAyLTMuMzY5LTEuMzQtMy4zNjktMS4zNGMtMC40NTUtMS4xNTctMS4xMS0xLjQ2NS0xLjExLTEuNDY1IGMtMC45MDktMC42MiwwLjA2OS0wLjYwOCwwLjA2OS0wLjYwOGMxLjAwNCwwLjA3MSwxLjUzMiwxLjAzLDEuNTMyLDEuMDNjMC44OTEsMS41MjksMi4zNDEsMS4wODksMi45MSwwLjgzMwpjMC4wOTEtMC42NDcsMC4zNDktMS4wODYsMC42MzUtMS4zMzdjLTIuMjItMC4yNTEtNC41NTUtMS4xMTEtNC41NTUtNC45NDNjMC0xLjA5MSwwLjM5LTEuOTg0LDEuMDMtMi42ODIgQzYuNTQ2LDguNTQsNi4yMDIsNy41MjQsNi43NDYsNi4xNDhjMCwwLDAuODQtMC4yNjksMi43NSwxLjAyNUMxMC4yOTUsNi45NSwxMS4xNSw2Ljg0LDEyLDYuODM2IGMwLjg1LDAuMDA0LDEuNzA1LDAuMTE0LDIuNTA0LDAuMzM2YzEuOTA5LTEuMjk0LDIuNzQ4LTEuMDI1LDIuNzQ4LTEuMDI1YzAuNTQ2LDEuMzc2LDAuMjAyLDIuMzk0LDAuMSwyLjY0NiBjMC42NCwwLjY5OSwxLjAyNiwxLjU5MSwxLjAyNiwyLjY4MmMwLDMuODQxLTIuMzM3LDQuNjg3LTQuNTY1LDQuOTM1YzAuMzU5LDAuMzA3LDAuNjc5LDAuOTE3LDAuNjc5LDEuODUyIGMwLDEuMzM1LTAuMDEyLDIuNDE1LTAuMDEyLDIuNzQxYzAsMC4yNjksMC4xOCwwLjU3OSwwLjY4OCwwLjQ4MUMxOS4xMzgsMjAuMTYxLDIyLDE2LjQxNiwyMiwxMkMyMiw2LjQ3NywxNy41MjMsMiwxMiwyeiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1pbnN0YWdyYW0iIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xMiw0LjYyMmMyLjQwMywwLDIuNjg4LDAuMDA5LDMuNjM3LDAuMDUyYzAuODc3LDAuMDQsMS4zNTQsMC4xODcsMS42NzEsMC4zMWMwLjQyLDAuMTYzLDAuNzIsMC4zNTgsMS4wMzUsMC42NzMgYzAuMzE1LDAuMzE1LDAuNTEsMC42MTUsMC42NzMsMS4wMzVjMC4xMjMsMC4zMTcsMC4yNywwLjc5NCwwLjMxLDEuNjcxYzAuMDQzLDAuOTQ5LDAuMDUyLDEuMjM0LDAuMDUyLDMuNjM3IHMtMC4wMDksMi42ODgtMC4wNTIsMy42MzdjLTAuMDQsMC44NzctMC4xODcsMS4zNTQtMC4zMSwxLjY3MWMtMC4xNjMsMC40Mi0wLjM1OCwwLjcyLTAuNjczLDEuMDM1IGMtMC4zMTUsMC4zMTUtMC42MTUsMC41MS0xLjAzNSwwLjY3M2MtMC4zMTcsMC4xMjMtMC43OTQsMC4yNy0xLjY3MSwwLjMxYy0wLjk0OSwwLjA0My0xLjIzMywwLjA1Mi0zLjYzNywwLjA1MiBzLTIuNjg4LTAuMDA5LTMuNjM3LTAuMDUyYy0wLjg3Ny0wLjA0LTEuMzU0LTAuMTg3LTEuNjcxLTAuMzFjLTAuNDItMC4xNjMtMC43Mi0wLjM1OC0xLjAzNS0wLjY3MyBjLTAuMzE1LTAuMzE1LTAuNTEtMC42MTUtMC42NzMtMS4wMzVjLTAuMTIzLTAuMzE3LTAuMjctMC43OTQtMC4zMS0xLjY3MUM0LjYzMSwxNC42ODgsNC42MjIsMTQuNDAzLDQuNjIyLDEyIHMwLjAwOS0yLjY4OCwwLjA1Mi0zLjYzN2MwLjA0LTAuODc3LDAuMTg3LTEuMzU0LDAuMzEtMS42NzFjMC4xNjMtMC40MiwwLjM1OC0wLjcyLDAuNjczLTEuMDM1IGMwLjMxNS0wLjMxNSwwLjYxNS0wLjUxLDEuMDM1LTAuNjczYzAuMzE3LTAuMTIzLDAuNzk0LTAuMjcsMS42NzEtMC4zMUM5LjMxMiw0LjYzMSw5LjU5Nyw0LjYyMiwxMiw0LjYyMiBNMTIsMyBDOS41NTYsMyw5LjI0OSwzLjAxLDguMjg5LDMuMDU0QzcuMzMxLDMuMDk4LDYuNjc3LDMuMjUsNi4xMDUsMy40NzJDNS41MTMsMy43MDIsNS4wMTEsNC4wMSw0LjUxMSw0LjUxMSBjLTAuNSwwLjUtMC44MDgsMS4wMDItMS4wMzgsMS41OTRDMy4yNSw2LjY3NywzLjA5OCw3LjMzMSwzLjA1NCw4LjI4OUMzLjAxLDkuMjQ5LDMsOS41NTYsMywxMmMwLDIuNDQ0LDAuMDEsMi43NTEsMC4wNTQsMy43MTEgYzAuMDQ0LDAuOTU4LDAuMTk2LDEuNjEyLDAuNDE4LDIuMTg1YzAuMjMsMC41OTIsMC41MzgsMS4wOTQsMS4wMzgsMS41OTRjMC41LDAuNSwxLjAwMiwwLjgwOCwxLjU5NCwxLjAzOCBjMC41NzIsMC4yMjIsMS4yMjcsMC4zNzUsMi4xODUsMC40MThDOS4yNDksMjAuOTksOS41NTYsMjEsMTIsMjFzMi43NTEtMC4wMSwzLjcxMS0wLjA1NGMwLjk1OC0wLjA0NCwxLjYxMi0wLjE5NiwyLjE4NS0wLjQxOCBjMC41OTItMC4yMywxLjA5NC0wLjUzOCwxLjU5NC0xLjAzOGMwLjUtMC41LDAuODA4LTEuMDAyLDEuMDM4LTEuNTk0YzAuMjIyLTAuNTcyLDAuMzc1LTEuMjI3LDAuNDE4LTIuMTg1IEMyMC45OSwxNC43NTEsMjEsMTQuNDQ0LDIxLDEycy0wLjAxLTIuNzUxLTAuMDU0LTMuNzExYy0wLjA0NC0wLjk1OC0wLjE5Ni0xLjYxMi0wLjQxOC0yLjE4NWMtMC4yMy0wLjU5Mi0wLjUzOC0xLjA5NC0xLjAzOC0xLjU5NCBjLTAuNS0wLjUtMS4wMDItMC44MDgtMS41OTQtMS4wMzhjLTAuNTcyLTAuMjIyLTEuMjI3LTAuMzc1LTIuMTg1LTAuNDE4QzE0Ljc1MSwzLjAxLDE0LjQ0NCwzLDEyLDNMMTIsM3ogTTEyLDcuMzc4IGMtMi41NTIsMC00LjYyMiwyLjA2OS00LjYyMiw0LjYyMlM5LjQ0OCwxNi42MjIsMTIsMTYuNjIyczQuNjIyLTIuMDY5LDQuNjIyLTQuNjIyUzE0LjU1Miw3LjM3OCwxMiw3LjM3OHogTTEyLDE1IGMtMS42NTcsMC0zLTEuMzQzLTMtM3MxLjM0My0zLDMtM3MzLDEuMzQzLDMsM1MxMy42NTcsMTUsMTIsMTV6IE0xNi44MDQsNi4xMTZjLTAuNTk2LDAtMS4wOCwwLjQ4NC0xLjA4LDEuMDggczAuNDg0LDEuMDgsMS4wOCwxLjA4YzAuNTk2LDAsMS4wOC0wLjQ4NCwxLjA4LTEuMDhTMTcuNDAxLDYuMTE2LDE2LjgwNCw2LjExNnoiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tbGlua2VkaW4iIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xOS43LDNINC4zQzMuNTgyLDMsMywzLjU4MiwzLDQuM3YxNS40QzMsMjAuNDE4LDMuNTgyLDIxLDQuMywyMWgxNS40YzAuNzE4LDAsMS4zLTAuNTgyLDEuMy0xLjNWNC4zIEMyMSwzLjU4MiwyMC40MTgsMywxOS43LDN6IE04LjMzOSwxOC4zMzhINS42Njd2LTguNTloMi42NzJWMTguMzM4eiBNNy4wMDQsOC41NzRjLTAuODU3LDAtMS41NDktMC42OTQtMS41NDktMS41NDggYzAtMC44NTUsMC42OTEtMS41NDgsMS41NDktMS41NDhjMC44NTQsMCwxLjU0NywwLjY5NCwxLjU0NywxLjU0OEM4LjU1MSw3Ljg4MSw3Ljg1OCw4LjU3NCw3LjAwNCw4LjU3NHogTTE4LjMzOSwxOC4zMzhoLTIuNjY5IHYtNC4xNzdjMC0wLjk5Ni0wLjAxNy0yLjI3OC0xLjM4Ny0yLjI3OGMtMS4zODksMC0xLjYwMSwxLjA4Ni0xLjYwMSwyLjIwNnY0LjI0OWgtMi42Njd2LTguNTloMi41NTl2MS4xNzRoMC4wMzcgYzAuMzU2LTAuNjc1LDEuMjI3LTEuMzg3LDIuNTI2LTEuMzg3YzIuNzAzLDAsMy4yMDMsMS43NzksMy4yMDMsNC4wOTJWMTguMzM4eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1tYWlsIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMjAsNEg0QzIuODk1LDQsMiw0Ljg5NSwyLDZ2MTJjMCwxLjEwNSwwLjg5NSwyLDIsMmgxNmMxLjEwNSwwLDItMC44OTUsMi0yVjZDMjIsNC44OTUsMjEuMTA1LDQsMjAsNHogTTIwLDguMjM2bC04LDQuODgyIEw0LDguMjM2VjZoMTZWOC4yMzZ6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLW1lZXR1cCIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTE5LjI0Nzc1LDE0LjcyMmEzLjU3MDMyLDMuNTcwMzIsMCwwLDEtMi45NDQ1NywzLjUyMDczLDMuNjE4ODYsMy42MTg4NiwwLDAsMS0uNjQ2NTIuMDU2MzRjLS4wNzMxNC0uMDAwOC0uMTAxODcuMDI4NDYtLjEyNTA3LjA5NTQ3QTIuMzg4ODEsMi4zODg4MSwwLDAsMSwxMy40OTQ1MywyMC4wOTRhMi4zMzA5MiwyLjMzMDkyLDAsMCwxLTEuODI3LS41MDcxNi4xMzYzNS4xMzYzNSwwLDAsMC0uMTk4NzgtLjAwNDA4LDMuMTkxLDMuMTkxLDAsMCwxLTIuMTA0LjYwMjQ4LDMuMjYzMDksMy4yNjMwOSwwLDAsMS0zLjAwMzI0LTIuNzE5OTMsMi4xOTA3NiwyLjE5MDc2LDAsMCwxLS4wMzUxMi0uMzA4NjVjLS4wMDE1Ni0uMDg1NzktLjAzNDEzLS4xMTg5LS4xMTYwOC0uMTM0OTNhMi44NjQyMSwyLjg2NDIxLDAsMCwxLTEuMjMxODktLjU2MTExLDIuOTQ1LDIuOTQ1LDAsMCwxLTEuMTY2LTIuMDU3NDksMi45NzQ4NCwyLjk3NDg0LDAsMCwxLC44NzUyNC0yLjUwNzc0LjExMi4xMTIsMCwwLDAsLjAyMDkxLS4xNjEwNywyLjcyMTMsMi43MjEzLDAsMCwxLS4zNjY0OC0xLjQ4QTIuODEyNTYsMi44MTI1NiwwLDAsMSw2LjU3NjczLDcuNTg4MzhhLjM1NzY0LjM1NzY0LDAsMCwwLC4yODg2OS0uMjI4MTksNC4yMjA4LDQuMjIwOCwwLDAsMSw2LjAyODkyLTEuOTAxMTEuMjUxNjEuMjUxNjEsMCwwLDAsLjIyMDIzLjAyNDMsMy42NTYwOCwzLjY1NjA4LDAsMCwxLDMuNzYwMzEuOTA2NzhBMy41NzI0NCwzLjU3MjQ0LDAsMCwxLDE3Ljk1OTE4LDguNjI2YTIuOTczMzksMi45NzMzOSwwLDAsMSwuMDE4MjkuNTczNTYuMTA2MzcuMTA2MzcsMCwwLDAsLjA4NTMuMTI3OTIsMS45NzY2OSwxLjk3NjY5LDAsMCwxLDEuMjc5MzksMS4zMzczMywyLjAwMjY2LDIuMDAyNjYsMCwwLDEtLjU3MTEyLDIuMTI2NTJjLS4wNTI4NC4wNTE2Ni0uMDQxNjguMDgzMjgtLjAxMTczLjEzNDg5QTMuNTExODksMy41MTE4OSwwLDAsMSwxOS4yNDc3NSwxNC43MjJabS02LjM1OTU5LS4yNzgzNmExLjY5ODQsMS42OTg0LDAsMCwwLDEuMTQ1NTYsMS42MTExMywzLjgyMDM5LDMuODIwMzksMCwwLDAsMS4wMzYuMTc5MzUsMS40Njg4OCwxLjQ2ODg4LDAsMCwwLC43MzUwOS0uMTIyNTUuNDQwODIuNDQwODIsMCwwLDAsLjI2MDU3LS40NDI3NC40NTMxMi40NTMxMiwwLDAsMC0uMjkyMTEtLjQzMzc1Ljk3MTkxLjk3MTkxLDAsMCwwLS4yMDY3OC0uMDYzYy0uMjEzMjYtLjAzODA2LS40Mjc1NC0uMDcwMS0uNjM5NzMtLjExMjE1YS41NDc4Ny41NDc4NywwLDAsMS0uNTAxNzItLjYwOTI2LDIuNzU4NjQsMi43NTg2NCwwLDAsMSwuMTc3My0uOTAxYy4xNzYzLS41MzUuNDE0LTEuMDQ1LjY0MTgzLTEuNTU5MTNBMTIuNjg2LDEyLjY4NiwwLDAsMCwxNS44NSwxMC40Nzg2M2ExLjU4NDYxLDEuNTg0NjEsMCwwLDAsLjA0ODYxLS44NzIwOCwxLjA0NTMxLDEuMDQ1MzEsMCwwLDAtLjg1NDMyLS44Mzk4MSwxLjYwNjU4LDEuNjA2NTgsMCwwLDAtMS4yMzY1NC4xNjU5NC4yNzU5My4yNzU5MywwLDAsMS0uMzYyODYtLjAzNDEzYy0uMDg1LS4wNzQ3LS4xNjU5NC0uMTUzNzktLjI0OTE4LS4yMzA1NWEuOTg2ODIuOTg2ODIsMCwwLDAtMS4zMzU3Ny0uMDQ5MzMsNi4xNDY4LDYuMTQ2OCwwLDAsMS0uNDk4OS40MTYxNS40Nzc2Mi40Nzc2MiwwLDAsMS0uNTE1MzUuMDM1NjZjLS4xNzQ0OC0uMDkzMDctLjM1NTEyLS4xNzUtLjUzNTMxLS4yNTY2NWExLjc0OTQ5LDEuNzQ5NDksMCwwLDAtLjU2NDc2LS4yMDE2LDEuNjk5NDMsMS42OTk0MywwLDAsMC0xLjYxNjU0LjkxNzg3LDguMDU4MTUsOC4wNTgxNSwwLDAsMC0uMzI5NTIuODAxMjZjLS40NTQ3MSwxLjI1NTctLjgyNTA3LDIuNTM4MjUtMS4yMDgzOCwzLjgxNjM5YTEuMjQxNTEsMS4yNDE1MSwwLDAsMCwuNTE1MzIsMS40NDM4OSwxLjQyNjU5LDEuNDI2NTksMCwwLDAsMS4yMjAwOC4xNzE2NiwxLjA5NzI4LDEuMDk3MjgsMCwwLDAsLjY2OTk0LS42OTc2NGMuNDQxNDUtMS4wNDExMS44MzktMi4wOTk4OSwxLjI1OTgxLTMuMTQ5MjYuMTE1ODEtLjI4ODc2LjIyNzkyLS41Nzg3NC4zNTA3OC0uODY0MzhhLjQ0NTQ4LjQ0NTQ4LDAsMCwxLC42OTE4OS0uMTk1MzkuNTA1MjEuNTA1MjEsMCwwLDEsLjE1MDQ0LjQzODM2LDEuNzU2MjUsMS43NTYyNSwwLDAsMS0uMTQ3MzEuNTA0NTNjLS4yNzM3OS42OTIxOS0uNTUyNjUsMS4zODIzNi0uODI3NjYsMi4wNzRhMi4wODM2LDIuMDgzNiwwLDAsMC0uMTQwMzguNDI4NzYuNTA3MTkuNTA3MTksMCwwLDAsLjI3MDgyLjU3NzIyLjg3MjM2Ljg3MjM2LDAsMCwwLC42NjE0NS4wMjczOS45OTEzNy45OTEzNywwLDAsMCwuNTM0MDYtLjUzMnEuNjE1NzEtMS4yMDkxNCwxLjIyOC0yLjQyMDMxLjI4NDIzLS41NTg2My41NzU4NS0xLjExMzNhLjg3MTg5Ljg3MTg5LDAsMCwxLC4yOTA1NS0uMzUyNTMuMzQ5ODcuMzQ5ODcsMCwwLDEsLjM3NjM0LS4wMTI2NS4zMDI5MS4zMDI5MSwwLDAsMSwuMTI0MzQuMzE0NTkuNTY3MTYuNTY3MTYsMCwwLDEtLjA0NjU1LjE5MTVjLS4wNTMxOC4xMjczOS0uMTAyODYuMjU2NjktLjE2MTgzLjM4MTU2LS4zNDExOC43MTc3NS0uNjg3NTQsMS40MzI3My0xLjAyNTY4LDIuMTUyQTIuMDAyMTMsMi4wMDIxMywwLDAsMCwxMi44ODgxNiwxNC40NDM2NlptNC43ODU2OCw1LjI4OTcyYS44ODU3My44ODU3MywwLDAsMC0xLjc3MTM5LjAwNDY1Ljg4NTcuODg1NywwLDAsMCwxLjc3MTM5LS4wMDQ2NVptLTE0LjgzODM4LTcuMjk2YS44NDMyOS44NDMyOSwwLDEsMCwuMDA4MjctMS42ODY1NS44NDMzLjg0MzMsMCwwLDAtLjAwODI3LDEuNjg2NTVabTEwLjM2Ni05LjQzNjczYS44MzUwNi44MzUwNiwwLDEsMC0uMDA5MSwxLjY3LjgzNTA1LjgzNTA1LDAsMCwwLC4wMDkxLTEuNjdabTYuODUwMTQsNS4yMmEuNzE2NTEuNzE2NTEsMCwwLDAtMS40MzMuMDA5My43MTY1Ni43MTY1NiwwLDAsMCwxLjQzMy0uMDA5M1pNNS4zNzUyOCw2LjE3OTA4QS42MzgyMy42MzgyMywwLDEsMCw2LjAxNSw1LjU0NDgzLjYyMjkyLjYyMjkyLDAsMCwwLDUuMzc1MjgsNi4xNzkwOFptNi42ODIxNCwxNC44MDg0M2EuNTQ5NDkuNTQ5NDksMCwxLDAtLjU1MDUyLjU0MUEuNTQ1NTYuNTQ1NTYsMCwwLDAsMTIuMDU3NDIsMjAuOTg3NTJabTguNTMyMzUtOC40OTY4OWEuNTQ3NzcuNTQ3NzcsMCwwLDAtLjU0MDI3LjU0MDIzLjUzMzI3LjUzMzI3LDAsMCwwLC41MzIuNTIyOTMuNTE1NDguNTE1NDgsMCwwLDAsLjUzMjcyLS41MjM3QS41MzE4Ny41MzE4NywwLDAsMCwyMC41ODk3NywxMi40OTA2M1pNNy44Mjg0NiwyLjQ3MTVhLjQ0OTI3LjQ0OTI3LDAsMSwwLC40NDQ4NC40NDc2NkEuNDM4MjEuNDM4MjEsMCwwLDAsNy44Mjg0NiwyLjQ3MTVabTEzLjc3NSw3LjYwNDkyYS40MTE4Ni40MTE4NiwwLDAsMC0uNDAwNjUuMzk2MjMuNDAxNzguNDAxNzgsMCwwLDAsLjQwMTY4LjQwMTY4QS4zODk5NC4zODk5NCwwLDAsMCwyMiwxMC40ODE3Mi4zOTk0Ni4zOTk0NiwwLDAsMCwyMS42MDM0OSwxMC4wNzY0MlpNNS43OTE5MywxNy45NjIwN2EuNDA0NjkuNDA0NjksMCwwLDAtLjM5Ny0uMzk2NDYuMzk5LjM5OSwwLDAsMC0uMzk2LjQwNS4zOTIzNC4zOTIzNCwwLDAsMCwuMzk5MzkuMzg5QS4zOTg1Ny4zOTg1NywwLDAsMCw1Ljc5MTkzLDE3Ljk2MjA3WiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1tZWRpdW0iIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik01LjcyNyA4LjAyN2EuNjIzLjYyMyAwIDAgMC0uMjA0LS41MjdMNC4wMiA1LjY4N3YtLjI3M0g4LjY5bDMuNjE0IDcuOTI2IDMuMTc1LTcuOTI2aDQuNDU3di4yNzRsLTEuMjg1IDEuMjM0YS4zNjcuMzY3IDAgMCAwLS4xNDQuMzZ2OS4wNjZhLjM3NC4zNzQgMCAwIDAgLjE0NC4zNjNsMS4yNTggMS4yMzR2LjI3aC02LjMyNHYtLjI3bDEuMy0xLjI2NWMuMTMtLjEzLjEzLS4xNjQuMTMtLjM2VjguOTg4bC0zLjYyMSA5LjE5NmgtLjQ4OUw2LjY5MSA4Ljk4OHY2LjE2NGMtLjAzNS4yNTguMDUxLjUyLjIzNS43MDdsMS42OTEgMi4wNTV2LjI3aC00Ljh2LS4yN2wxLjY5LTIuMDU1YS44MTQuODE0IDAgMCAwIC4yMi0uNzA3em0wIDAiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tcGF0cmVvbiIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTEzLjk3NSA1YTUuMDUgNS4wNSAwIDAwLTUuMDQxIDUuMDQ2YzAgMi43NzQgMi4yNjEgNS4wMyA1LjA0IDUuMDNBNS4wMzQgNS4wMzQgMCAwMDE5IDEwLjA0N0MxOSA3LjI2NCAxNi43NDYgNSAxMy45NzUgNXpNNSAxOC40NGgyLjQ2MVY1SDV2MTMuNDR6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXBpbnRlcmVzdCIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTEyLjI4OSwyQzYuNjE3LDIsMy42MDYsNS42NDgsMy42MDYsOS42MjJjMCwxLjg0NiwxLjAyNSw0LjE0NiwyLjY2Niw0Ljg3OGMwLjI1LDAuMTExLDAuMzgxLDAuMDYzLDAuNDM5LTAuMTY5IGMwLjA0NC0wLjE3NSwwLjI2Ny0xLjAyOSwwLjM2NS0xLjQyOGMwLjAzMi0wLjEyOCwwLjAxNy0wLjIzNy0wLjA5MS0wLjM2MkM2LjQ0NSwxMS45MTEsNi4wMSwxMC43NSw2LjAxLDkuNjY4IGMwLTIuNzc3LDIuMTk0LTUuNDY0LDUuOTMzLTUuNDY0YzMuMjMsMCw1LjQ5LDIuMTA4LDUuNDksNS4xMjJjMCwzLjQwNy0xLjc5NCw1Ljc2OC00LjEzLDUuNzY4Yy0xLjI5MSwwLTIuMjU3LTEuMDIxLTEuOTQ4LTIuMjc3IGMwLjM3Mi0xLjQ5NSwxLjA4OS0zLjExMiwxLjA4OS00LjE5MWMwLTAuOTY3LTAuNTQyLTEuNzc1LTEuNjYzLTEuNzc1Yy0xLjMxOSwwLTIuMzc5LDEuMzA5LTIuMzc5LDMuMDU5IGMwLDEuMTE1LDAuMzk0LDEuODY5LDAuMzk0LDEuODY5cy0xLjMwMiw1LjI3OS0xLjU0LDYuMjYxYy0wLjQwNSwxLjY2NiwwLjA1Myw0LjM2OCwwLjA5NCw0LjYwNCBjMC4wMjEsMC4xMjYsMC4xNjcsMC4xNjksMC4yNSwwLjA2M2MwLjEyOS0wLjE2NSwxLjY5OS0yLjQxOSwyLjE0Mi00LjA1MWMwLjE1OC0wLjU5LDAuODE3LTIuOTk1LDAuODE3LTIuOTk1IGMwLjQzLDAuNzg0LDEuNjgxLDEuNDQ2LDMuMDEzLDEuNDQ2YzMuOTYzLDAsNi44MjItMy40OTQsNi44MjItNy44MzNDMjAuMzk0LDUuMTEyLDE2Ljg0OSwyLDEyLjI4OSwyIj48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXBvY2tldCIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTIxLjkyNyw0LjE5NEMyMS42NjcsMy40OCwyMC45ODIsMywyMC4yMjIsM2gtMC4wMWgtMS43MjFIMy44MzlDMy4wOTIsMywyLjQxMSwzLjQ3LDIuMTQ1LDQuMTcgQzIuMDY2LDQuMzc4LDIuMDI2LDQuNTk0LDIuMDI2LDQuODE0djYuMDM1bDAuMDY5LDEuMmMwLjI5LDIuNzMsMS43MDcsNS4xMTUsMy44OTksNi43NzhjMC4wMzksMC4wMywwLjA3OSwwLjA1OSwwLjExOSwwLjA4OSBsMC4wMjUsMC4wMThjMS4xNzUsMC44NTksMi40OTEsMS40NDEsMy45MSwxLjcyN2MwLjY1NSwwLjEzMiwxLjMyNSwwLjIsMS45OTEsMC4yYzAuNjE1LDAsMS4yMzItMC4wNTcsMS44MzktMC4xNyBjMC4wNzMtMC4wMTQsMC4xNDUtMC4wMjgsMC4yMTktMC4wNDRjMC4wMi0wLjAwNCwwLjA0Mi0wLjAxMiwwLjA2NC0wLjAyM2MxLjM1OS0wLjI5NywyLjYyMS0wLjg2NCwzLjc1My0xLjY5MWwwLjAyNS0wLjAxOCBjMC4wNC0wLjAyOSwwLjA4LTAuMDU4LDAuMTE5LTAuMDg5YzIuMTkyLTEuNjY0LDMuNjA5LTQuMDQ5LDMuODk4LTYuNzc4bDAuMDY5LTEuMlY0LjgxNEMyMi4wMjYsNC42MDUsMjIsNC4zOTgsMjEuOTI3LDQuMTk0eiBNMTcuNjkyLDEwLjQ4MWwtNC43MDQsNC41MTJjLTAuMjY2LDAuMjU0LTAuNjA4LDAuMzgyLTAuOTQ5LDAuMzgyYy0wLjM0MiwwLTAuNjg0LTAuMTI4LTAuOTQ5LTAuMzgybC00LjcwNS00LjUxMiBDNS44MzgsOS45NTcsNS44Miw5LjA4OSw2LjM0NCw4LjU0MmMwLjUyNC0wLjU0NywxLjM5Mi0wLjU2NSwxLjkzOS0wLjA0bDMuNzU2LDMuNjAxbDMuNzU1LTMuNjAxIGMwLjU0Ny0wLjUyNCwxLjQxNS0wLjUwNiwxLjkzOSwwLjA0QzE4LjI1Niw5LjA4OSwxOC4yMzgsOS45NTYsMTcuNjkyLDEwLjQ4MXoiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tcmF2ZWxyeSIgdmlld2JveD0iMCAwIDIzIDIwIj4KPHBhdGggZD0iTTEyLjA5OCAxOS4zNGEuMjUuMjUgMCAwMS0uMTE4LS4wNDMgMTMuOTg2IDEzLjk4NiAwIDAxLS4zOTQtLjI1OGMtLjE2NC0uMTEtLjQ3Ny0uMzUyLS45MzQtLjcyMy0uNDYtLjM3NS0uODgyLS43NjEtMS4yNy0xLjE2OC0uMzktLjQwNi0uNzk2LS45MjUtMS4yMTgtMS41NjJhOC41MjEgOC41MjEgMCAwMS0uOTc2LTEuOTI2Yy0uMTI1LS4wMjMtLjc1OC0uMTYtMS45MDctLjQxNEE4Ljc4NSA4Ljc4NSAwIDAwNy44NCAxNy4yOWE4LjE1MiA4LjE1MiAwIDAwNC4yNTggMi4wNXptLTYuOTgtNi43NjJsMS44MzEuMzEzQTEzLjQyNCAxMy40MjQgMCAwMTYuNSAxMS4wMmExNi4yMTYgMTYuMjE2IDAgMDEtLjIwNy0xLjYyMmwtLjA0My0uNTkzYy0uNjEuNjEtMS4wNDcgMS40NDUtMS4zMTYgMi41LjAzNS40ODQuMDk3LjkxLjE4MyAxLjI3M3ptMS4xOTgtNi43OTdhOS4wNjQgOS4wNjQgMCAwMC0uODQgMS42NTNjLjMyLS4zNDQuNTktLjU5OC44MS0uNzU4em0xNS42NDkgNC44NDRhOC44IDguOCAwIDAwLS42NzYtMy40MjYgOC44NSA4Ljg1IDAgMDAtMS44MjQtMi44MTIgOC42MTQgOC42MTQgMCAwMC0yLjcyNy0xLjg4MyA4LjExNSA4LjExNSAwIDAwLTMuMzEyLS42OTUgOC4xMzEgOC4xMzEgMCAwMC0zLjA1OS41ODZBOC40MTIgOC40MTIgMCAwMDcuNzU0IDQuMDVjLS4yMTkuNDMzLS4zODMgMS4wMjctLjQ4OCAxLjc4NWE1LjQwNyA1LjQwNyAwIDAxMS41NTQtLjkzIDcuNDgxIDcuNDgxIDAgMDExLjcyNy0uNDggMTYuNjQyIDE2LjY0MiAwIDAxMS41NTgtLjE1M2MuNDg5LS4wMi44ODMtLjAxNSAxLjE4LjAxMmwuNDM4LjAzNWMuMjM4LjAwOC40My4wNjMuNTc0LjE3MmEuNjYuNjYgMCAwMS4yNy4zNjdjLjAzLjE0MS4wNTQuMjc4LjA3LjQxNGEuOC44IDAgMDEtLjAxMi4zMTcgMTIuNzgxIDEyLjc4MSAwIDAwLTIuNDc3LS4wMDQgNy4wOTMgNy4wOTMgMCAwMC0xLjk5Mi40ODQgOS42IDkuNiAwIDAwLTEuNTU0LjgwMUExMi40NiAxMi40NiAwIDAwNy4xNzYgNy45N2MuMDMxLjI3LjA3LjYxMy4xMjUgMS4wMzEuMDU0LjQyMi4xODMgMS4wODIuMzg2IDEuOTg4LjIwNC45MDMuNDMgMS41Ny42NzYgMi4wMDQuODk1LjA0MyAxLjc5My0uMDEyIDIuNjk2LS4xNjQuOTAyLS4xNTIgMS42ODMtLjM1MSAyLjMzNi0uNTk4YTIwLjY4MSAyMC42ODEgMCAwMDEuNzctLjc0NmMuNTI2LS4yNTQuOTI1LS40NzIgMS4xOS0uNjZsLjQwNy0uMjY1Yy4xNTYtLjEyMS4zLS4xOTYuNDMtLjIzYS4zNjcuMzY3IDAgMDEuMzMxLjA1OGMuMDk0LjA3LjE1Ny4xOTkuMTg0LjM4My4xMDIuNzIyLS4wMzkgMS4xNzEtLjQyNiAxLjM1MS0xLjUwOC43MjMtMy4yMDMgMS4yMTktNS4wODYgMS40OTYtLjk3Ni4xNDktMi4xMjkuMjA3LTMuNDQ5LjE3NmE3LjY3MyA3LjY3MyAwIDAwMS4xOTUgMS45NzNjLjUwNC41OTcgMSAxLjA3IDEuNDkzIDEuNDE4LjQ5Ni4zNDMuOTY4LjYzNiAxLjQyMS44NzguNDU0LjI0My44MjUuNDA3IDEuMTA2LjQ4OWwuNDI2LjEzM2MxLjAzOS4xNzEgMS45OTIuMTEzIDIuODYzLS4xNjggMS40MTQtLjczOSAyLjU1NS0xLjgxMyAzLjQxOC0zLjIyNy44NjctMS40MTQgMS4yOTctMi45NjkgMS4yOTctNC42NjR6bS44MDUtLjQxNGMtLjEwMiAxLjAwNC0uMjQ3IDEuNzkzLS40MzQgMi4zNjctLjUwOCAxLjU0Ny0xLjE2OCAyLjgzNi0xLjk3NyAzLjg2Ny0uODA4IDEuMDMyLTEuOTY0IDEuOTczLTMuNDY4IDIuODI4LS4zNDguMjQ3LS42NDUuNDEtLjg5NS40OTMtLjUyLjE5NS0xLjExMy4yNTQtMS43NzMuMTgtLjI2Mi4wMTktLjUyOC4wMy0uNzk3LjAzLTIuMDU1IDAtMy44ODMtLjY0LTUuNDkyLTEuOTMtMS42MS0xLjI4OC0yLjY4LTIuOTI5LTMuMjItNC45MzMtLjAwNyAwLS4wMTkgMC0uMDQyLS4wMDQtLjAyNC0uMDA0LS4wNC0uMDA3LS4wNTUtLjAwNy0uMDQzLjM3NS0uMDM1Ljc5My4wMjggMS4yNTcuMDYyLjQ2NS4xNTYuODkxLjI4NSAxLjI4Mi4xMjUuMzkuMjU0Ljc1Ny4zOSAxLjA5My4xMzMuMzQuMjUuNjA2LjM0NC44MDFsLjE1Mi4yOWMuMDU5LjA5LjI1NC4zOTQuNTg2LjkxM2E0LjMyNyA0LjMyNyAwIDAxLTEuMzU1LTEuMTg3IDUuNjY5IDUuNjY5IDAgMDEtLjg1Ni0xLjU2MyAxNC4wODcgMTQuMDg3IDAgMDEtLjQzLTEuNTMxIDkuMDEyIDkuMDEyIDAgMDEtLjE5LTEuMmwtLjAyLS40NjhjLS4wMzUtLjAxNi0uMTYtLjA1OS0uMzY3LS4xMzctLjIwNy0uMDc4LS4zODMtLjE0OC0uNTI4LS4yMDMtLjE0NC0uMDU0LS4zMzYtLjEzMy0uNTc4LS4yMjZhOS4yMjEgOS4yMjEgMCAwMS0uNjI1LS4yODJjLS4xNzYtLjA5LS4zNi0uMTgzLS41NDMtLjI4NS0uMTg3LS4wOTctLjM0LS4xOTktLjQ2NS0uM2ExLjI3IDEuMjcgMCAwMS0uMjctLjI4NmMuMTM4LjA3NS4zMjEuMTcyLjU0OC4yODUuMjMuMTE4LjY0LjI4NiAxLjIzLjUwOC41OTQuMjIzIDEuMTIxLjM2NCAxLjU4Ni40MjZsLjAyMy0uMzZjLjA3OS0xLjEwOS40MTgtMi4xODcgMS4wMjQtMy4yMzRBOS4yMjYgOS4yMjYgMCAwMTYuNSA0LjYyMWMuMjAzLS44NTUuNS0xLjY1Mi44ODMtMi4zOS4xMS0uMjA4LjIyNi0uMzc2LjM0Ny0uNS4xMjUtLjEzLjMwNS0uMjQ3LjUzNi0uMzYgMS4xNDgtLjU1IDIuMjUtLjkzNyAzLjMwNC0xLjE2QTkuOTM1IDkuOTM1IDAgMDExNC44Ni4wOWMxLjEzNi4xNCAyLjI1LjUgMy4zNCAxLjA4MiAxLjU5My44NTUgMi44MDQgMi4xMDUgMy42MjQgMy43NS44MiAxLjY0NCAxLjEzNyAzLjQwNi45NDYgNS4yODl6bTAgMCI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1yZWRkaXQiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0yMiwxMS44MTZjMC0xLjI1Ni0xLjAyMS0yLjI3Ny0yLjI3Ny0yLjI3N2MtMC41OTMsMC0xLjEyMiwwLjI0LTEuNTI2LDAuNjE0Yy0xLjQ4MS0wLjk2NS0zLjQ1NS0xLjU5NC01LjY0Ny0xLjY5IGwxLjE3MS0zLjcwMmwzLjE4LDAuNzQ4YzAuMDA4LDEuMDI4LDAuODQ2LDEuODYyLDEuODc2LDEuODYyYzEuMDM1LDAsMS44NzctMC44NDIsMS44NzctMS44NzhjMC0xLjAzNS0wLjg0Mi0xLjg3Ny0xLjg3Ny0xLjg3NyBjLTAuNzY5LDAtMS40MzEsMC40NjYtMS43MiwxLjEzbC0zLjUwOC0wLjgyNmMtMC4yMDMtMC4wNDctMC4zOTksMC4wNjctMC40NiwwLjI2MWwtMS4zNSw0LjI2OCBjLTIuMzE2LDAuMDM4LTQuNDExLDAuNjctNS45NywxLjY3MUM1LjM2OCw5Ljc2NSw0Ljg1Myw5LjUzOSw0LjI3Nyw5LjUzOUMzLjAyMSw5LjUzOSwyLDEwLjU2LDIsMTEuODE2IGMwLDAuODE0LDAuNDMzLDEuNTIzLDEuMDc4LDEuOTI1Yy0wLjAzNywwLjIyMS0wLjA2MSwwLjQ0NC0wLjA2MSwwLjY3MmMwLDMuMjkyLDQuMDExLDUuOTcsOC45NDEsNS45N3M4Ljk0MS0yLjY3OCw4Ljk0MS01Ljk3IGMwLTAuMjE0LTAuMDItMC40MjQtMC4wNTMtMC42MzJDMjEuNTMzLDEzLjM5LDIyLDEyLjY2MSwyMiwxMS44MTZ6IE0xOC43NzYsNC4zOTRjMC42MDYsMCwxLjEsMC40OTMsMS4xLDEuMXMtMC40OTMsMS4xLTEuMSwxLjEgcy0xLjEtMC40OTQtMS4xLTEuMVMxOC4xNjksNC4zOTQsMTguNzc2LDQuMzk0eiBNMi43NzcsMTEuODE2YzAtMC44MjcsMC42NzItMS41LDEuNDk5LTEuNWMwLjMxMywwLDAuNTk4LDAuMTAzLDAuODM4LDAuMjY5IGMtMC44NTEsMC42NzYtMS40NzcsMS40NzktMS44MTIsMi4zNkMyLjk4MywxMi42NzIsMi43NzcsMTIuMjcsMi43NzcsMTEuODE2eiBNMTEuOTU5LDE5LjYwNmMtNC41MDEsMC04LjE2NC0yLjMyOS04LjE2NC01LjE5MyBTNy40NTcsOS4yMiwxMS45NTksOS4yMnM4LjE2NCwyLjMyOSw4LjE2NCw1LjE5M1MxNi40NiwxOS42MDYsMTEuOTU5LDE5LjYwNnogTTIwLjYzNiwxMy4wMDFjLTAuMzI2LTAuODktMC45NDgtMS43MDEtMS43OTctMi4zODQgYzAuMjQ4LTAuMTg2LDAuNTUtMC4zMDEsMC44ODMtMC4zMDFjMC44MjcsMCwxLjUsMC42NzMsMS41LDEuNUMyMS4yMjMsMTIuMjk5LDIwLjk5MiwxMi43MjcsMjAuNjM2LDEzLjAwMXogTTguOTk2LDE0LjcwNCBjLTAuNzYsMC0xLjM5Ny0wLjYxNi0xLjM5Ny0xLjM3NmMwLTAuNzYsMC42MzctMS4zOTcsMS4zOTctMS4zOTdjMC43NiwwLDEuMzc2LDAuNjM3LDEuMzc2LDEuMzk3IEMxMC4zNzIsMTQuMDg4LDkuNzU2LDE0LjcwNCw4Ljk5NiwxNC43MDR6IE0xNi40MDEsMTMuMzI4YzAsMC43Ni0wLjYxNiwxLjM3Ni0xLjM3NiwxLjM3NmMtMC43NiwwLTEuMzk5LTAuNjE2LTEuMzk5LTEuMzc2IGMwLTAuNzYsMC42MzktMS4zOTcsMS4zOTktMS4zOTdDMTUuNzg1LDExLjkzMSwxNi40MDEsMTIuNTY4LDE2LjQwMSwxMy4zMjh6IE0xNS4yMjksMTYuNzA4YzAuMTUyLDAuMTUyLDAuMTUyLDAuMzk4LDAsMC41NSBjLTAuNjc0LDAuNjc0LTEuNzI3LDEuMDAyLTMuMjE5LDEuMDAyYy0wLjAwNCwwLTAuMDA3LTAuMDAyLTAuMDExLTAuMDAyYy0wLjAwNCwwLTAuMDA3LDAuMDAyLTAuMDExLDAuMDAyIGMtMS40OTIsMC0yLjU0NC0wLjMyOC0zLjIxOC0xLjAwMmMtMC4xNTItMC4xNTItMC4xNTItMC4zOTgsMC0wLjU1YzAuMTUyLTAuMTUyLDAuMzk5LTAuMTUxLDAuNTUsMCBjMC41MjEsMC41MjEsMS4zOTQsMC43NzUsMi42NjksMC43NzVjMC4wMDQsMCwwLjAwNywwLjAwMiwwLjAxMSwwLjAwMmMwLjAwNCwwLDAuMDA3LTAuMDAyLDAuMDExLTAuMDAyIGMxLjI3NSwwLDIuMTQ4LTAuMjUzLDIuNjY5LTAuNzc1QzE0LjgzMSwxNi41NTYsMTUuMDc4LDE2LjU1NiwxNS4yMjksMTYuNzA4eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1za3lwZSIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTEwLjExMywyLjY5OWMwLjAzMy0wLjAwNiwwLjA2Ny0wLjAxMywwLjEtMC4wMmMwLjAzMywwLjAxNywwLjA2NiwwLjAzMywwLjA5OCwwLjA1MUwxMC4xMTMsMi42OTl6IE0yLjcyLDEwLjIyMyBjLTAuMDA2LDAuMDM0LTAuMDExLDAuMDY5LTAuMDE3LDAuMTAzYzAuMDE4LDAuMDMyLDAuMDMzLDAuMDY0LDAuMDUxLDAuMDk1TDIuNzIsMTAuMjIzeiBNMjEuMjc1LDEzLjc3MSBjMC4wMDctMC4wMzUsMC4wMTEtMC4wNzEsMC4wMTgtMC4xMDZjLTAuMDE4LTAuMDMxLTAuMDMzLTAuMDY0LTAuMDUyLTAuMDk1TDIxLjI3NSwxMy43NzF6IE0xMy41NjMsMjEuMTk5IGMwLjAzMiwwLjAxOSwwLjA2NSwwLjAzNSwwLjA5NiwwLjA1M2MwLjAzNi0wLjAwNiwwLjA3MS0wLjAxMSwwLjEwNS0wLjAxN0wxMy41NjMsMjEuMTk5eiBNMjIsMTYuMzg2IGMwLDEuNDk0LTAuNTgxLDIuODk4LTEuNjM3LDMuOTUzYy0xLjA1NiwxLjA1Ny0yLjQ1OSwxLjYzNy0zLjk1MywxLjYzN2MtMC45NjcsMC0xLjkxNC0wLjI1MS0yLjc1LTAuNzI1IGMwLjAzNi0wLjAwNiwwLjA3MS0wLjAxMSwwLjEwNS0wLjAxN2wtMC4yMDItMC4wMzVjMC4wMzIsMC4wMTksMC4wNjUsMC4wMzUsMC4wOTYsMC4wNTNjLTAuNTQzLDAuMDk2LTEuMDk5LDAuMTQ3LTEuNjU0LDAuMTQ3IGMtMS4yNzUsMC0yLjUxMi0wLjI1LTMuNjc2LTAuNzQzYy0xLjEyNS0wLjQ3NC0yLjEzNS0xLjE1Ni0zLjAwMi0yLjAyM2MtMC44NjctMC44NjctMS41NDgtMS44NzctMi4wMjMtMy4wMDIgYy0wLjQ5My0xLjE2NC0wLjc0My0yLjQwMS0wLjc0My0zLjY3NmMwLTAuNTQ2LDAuMDQ5LTEuMDkzLDAuMTQyLTEuNjI4YzAuMDE4LDAuMDMyLDAuMDMzLDAuMDY0LDAuMDUxLDAuMDk1TDIuNzIsMTAuMjIzIGMtMC4wMDYsMC4wMzQtMC4wMTEsMC4wNjktMC4wMTcsMC4xMDNDMi4yNDQsOS41LDIsOC41NjYsMiw3LjYxNWMwLTEuNDkzLDAuNTgyLTIuODk4LDEuNjM3LTMuOTUzIGMxLjA1Ni0xLjA1NiwyLjQ2LTEuNjM4LDMuOTUzLTEuNjM4YzAuOTE1LDAsMS44MTgsMC4yMjgsMi42MjIsMC42NTVjLTAuMDMzLDAuMDA3LTAuMDY3LDAuMDEzLTAuMSwwLjAybDAuMTk5LDAuMDMxIGMtMC4wMzItMC4wMTgtMC4wNjYtMC4wMzQtMC4wOTgtMC4wNTFjMC4wMDIsMCwwLjAwMy0wLjAwMSwwLjAwNC0wLjAwMWMwLjU4Ni0wLjExMiwxLjE4Ny0wLjE2OSwxLjc4OC0wLjE2OSBjMS4yNzUsMCwyLjUxMiwwLjI0OSwzLjY3NiwwLjc0MmMxLjEyNCwwLjQ3NiwyLjEzNSwxLjE1NiwzLjAwMiwyLjAyNGMwLjg2OCwwLjg2NywxLjU0OCwxLjg3NywyLjAyNCwzLjAwMiBjMC40OTMsMS4xNjQsMC43NDMsMi40MDEsMC43NDMsMy42NzZjMCwwLjU3NS0wLjA1NCwxLjE1LTAuMTU3LDEuNzEyYy0wLjAxOC0wLjAzMS0wLjAzMy0wLjA2NC0wLjA1Mi0wLjA5NWwwLjAzNCwwLjIwMSBjMC4wMDctMC4wMzUsMC4wMTEtMC4wNzEsMC4wMTgtMC4xMDZDMjEuNzU0LDE0LjQ5NCwyMiwxNS40MzIsMjIsMTYuMzg2eiBNMTYuODE3LDE0LjEzOGMwLTEuMzMxLTAuNjEzLTIuNzQzLTMuMDMzLTMuMjgyIGwtMi4yMDktMC40OWMtMC44NC0wLjE5Mi0xLjgwNy0wLjQ0NC0xLjgwNy0xLjIzN2MwLTAuNzk0LDAuNjc5LTEuMzQ4LDEuOTAzLTEuMzQ4YzIuNDY4LDAsMi4yNDMsMS42OTYsMy40NjgsMS42OTYgYzAuNjQ1LDAsMS4yMDktMC4zNzksMS4yMDktMS4wMzFjMC0xLjUyMS0yLjQzNS0yLjY2My00LjUtMi42NjNjLTIuMjQyLDAtNC42MywwLjk1Mi00LjYzLDMuNDg4YzAsMS4yMjEsMC40MzYsMi41MjEsMi44MzksMy4xMjMgbDIuOTg0LDAuNzQ1YzAuOTAzLDAuMjIzLDEuMTI5LDAuNzMxLDEuMTI5LDEuMTg5YzAsMC43NjItMC43NTgsMS41MDctMi4xMjksMS41MDdjLTIuNjc5LDAtMi4zMDctMi4wNjItMy43NDMtMi4wNjIgYy0wLjY0NSwwLTEuMTEzLDAuNDQ0LTEuMTEzLDEuMDc4YzAsMS4yMzYsMS41MDEsMi44ODYsNC44NTYsMi44ODZDMTUuMjM2LDE3LjczNywxNi44MTcsMTYuMTk5LDE2LjgxNywxNC4xMzh6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXNsaWRlc2hhcmUiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xMS43MzgsMTAuMjMyYTIuMTQyLDIuMTQyLDAsMCwxLS43MjEsMS42MTksMi41NTYsMi41NTYsMCwwLDEtMy40NjQsMCwyLjE4MywyLjE4MywwLDAsMSwwLTMuMjQzLDIuNTcyLDIuNTcyLDAsMCwxLDMuNDY0LDBBMi4xMzYsMi4xMzYsMCwwLDEsMTEuNzM4LDEwLjIzMlptNS43LDBhMi4xNSwyLjE1LDAsMCwxLS43MTUsMS42MTksMi41NjMsMi41NjMsMCwwLDEtMy40NjksMCwyLjE4MywyLjE4MywwLDAsMSwwLTMuMjQzLDIuNTgsMi41OCwwLDAsMSwzLjQ2OSwwQTIuMTQ0LDIuMTQ0LDAsMCwxLDE3LjQzOSwxMC4yMzJabTIuNTU1LDIuMDQ1VjQuN2EyLjEyOCwyLjEyOCwwLDAsMC0uMzYzLTEuNCwxLjYxNCwxLjYxNCwwLDAsMC0xLjI2MS0uNDE1SDUuNzQyYTEuNjU2LDEuNjU2LDAsMCwwLTEuMjc4LjM4NkEyLjI0NiwyLjI0NiwwLDAsMCw0LjEyOSw0Ljd2Ny42NDNhOC4yMTIsOC4yMTIsMCwwLDAsMSwuNDU0cS41MTYuMTkzLjkyLjMxOGE2Ljg0Nyw2Ljg0NywwLDAsMCwuOTIuMjFxLjUxNi4wODUuODA2LjEyNWE2LjYxNSw2LjYxNSwwLDAsMCwuNzk1LjA0NWwuNjY1LjAwNnEuMTYsMCwuNjQyLS4wMjN0LjUwNi0uMDIzYTEuNDM4LDEuNDM4LDAsMCwxLDEuMDc5LjMwNywxLjEzNCwxLjEzNCwwLDAsMCwuMTE0LjEsNy4yMTUsNy4yMTUsMCwwLDAsLjY5My41NzlxLjA3OS0xLjAzMywxLjM0LS45ODguMDU3LDAsLjQxNS4wMTdsLjQ4OC4wMjNxLjEzLjAwNi41MTcuMDExdC42LS4wMTFsLjYxOS0uMDUxYTUuNDE5LDUuNDE5LDAsMCwwLC42OTMtLjFsLjctLjE1M2E1LjM1Myw1LjM1MywwLDAsMCwuNzYxLS4yMjFxLjM0NS0uMTMxLjc2Ni0uMzA3YTguNzI3LDguNzI3LDAsMCwwLC44MTgtLjM5MlptMS44NTEtLjA1N2ExMC40LDEwLjQsMCwwLDEtNC4yMjUsMi44NjIsNi41LDYuNSwwLDAsMS0uMjYxLDUuMjgxLDMuNTI0LDMuNTI0LDAsMCwxLTIuMDc4LDEuNjgxLDIuNDUyLDIuNDUyLDAsMCwxLTIuMDY3LS4xNywxLjkxNSwxLjkxNSwwLDAsMS0uOTMxLTEuODYzbC0uMDExLTMuN1YxNi4zbC0uMjc5LS4wNjhxLS4xODgtLjA0NS0uMjY3LS4wNTdsLS4wMTEsMy44MzlhMS45LDEuOSwwLDAsMS0uOTQzLDEuODYzLDIuNDgxLDIuNDgxLDAsMCwxLTIuMDc4LjE3LDMuNTE5LDMuNTE5LDAsMCwxLTIuMDY3LTEuNyw2LjU0Niw2LjU0NiwwLDAsMS0uMjUtNS4yNThBMTAuNCwxMC40LDAsMCwxLDIuMTUyLDEyLjIyYS41Ni41NiwwLDAsMS0uMDQ1LS43MTVxLjIzOC0uMy42ODEuMDExbC4xMjUuMDc5YS43NjcuNzY3LDAsMCwxLC4xMjUuMDkxVjMuOGExLjk4NywxLjk4NywwLDAsMSwuNTM0LTEuNCwxLjcsMS43LDAsMCwxLDEuMjk1LS41NzlIMTkuMTQxYTEuNywxLjcsMCwwLDEsMS4yOTUuNTc5LDEuOTg1LDEuOTg1LDAsMCwxLC41MzQsMS40djcuODgybC4yMzgtLjE3cS40NDMtLjMwNy42ODEtLjAxMWEuNTYuNTYsMCwwLDEtLjA0NS43MTVaIj48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXNuYXBjaGF0IiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTIuMDY1LDJhNS41MjYsNS41MjYsMCwwLDEsMy4xMzIuODkyQTUuODU0LDUuODU0LDAsMCwxLDE3LjMyNiw1LjRhNS44MjEsNS44MjEsMCwwLDEsLjM1MSwyLjMzcTAsLjYxMi0uMTE3LDIuNDg3YS44MDkuODA5LDAsMCwwLC4zNjUuMDkxLDEuOTMsMS45MywwLDAsMCwuNjY0LS4xNzYsMS45MywxLjkzLDAsMCwxLC42NjQtLjE3NiwxLjMsMS4zLDAsMCwxLC43MjkuMjM0LjcuNywwLDAsMSwuMzUxLjYuODM5LjgzOSwwLDAsMS0uNDEuNywyLjczMiwyLjczMiwwLDAsMS0uOS40MSwzLjE5MiwzLjE5MiwwLDAsMC0uOS4zNzguNzI4LjcyOCwwLDAsMC0uNDEuNjE4LDEuNTc1LDEuNTc1LDAsMCwwLC4xNTYuNTYsNi45LDYuOSwwLDAsMCwxLjMzNCwxLjk1Myw1LjYsNS42LDAsMCwwLDEuODgxLDEuMzE1LDUuODc1LDUuODc1LDAsMCwwLDEuMDQyLjMuNDIuNDIsMCwwLDEsLjM2NS40NTZxMCwuOTExLTIuODUyLDEuMzQxYTEuMzc5LDEuMzc5LDAsMCwwLS4xNDMuNTA3LDEuOCwxLjgsMCwwLDEtLjE4Mi42MDUuNDUxLjQ1MSwwLDAsMS0uNDI5LjI0MSw1Ljg3OCw1Ljg3OCwwLDAsMS0uODA3LS4wODUsNS45MTcsNS45MTcsMCwwLDAtLjgzMy0uMDg1LDQuMjE3LDQuMjE3LDAsMCwwLS44MDcuMDY1LDIuNDIsMi40MiwwLDAsMC0uODIuMjkzLDYuNjgyLDYuNjgyLDAsMCwwLS43NTUuNXEtLjM1MS4yNjctLjc1NS41MjdhMy44ODYsMy44ODYsMCwwLDEtLjk4OS40MzZBNC40NzEsNC40NzEsMCwwLDEsMTEuODMxLDIyYTQuMzA3LDQuMzA3LDAsMCwxLTEuMjU2LS4xNzYsMy43ODQsMy43ODQsMCwwLDEtLjk3Ni0uNDM2cS0uNC0uMjYtLjc0OS0uNTI3YTYuNjgyLDYuNjgyLDAsMCwwLS43NTUtLjUsMi40MjIsMi40MjIsMCwwLDAtLjgwNy0uMjkzLDQuNDMyLDQuNDMyLDAsMCwwLS44Mi0uMDY1LDUuMDg5LDUuMDg5LDAsMCwwLS44NTMuMSw1LDUsMCwwLDEtLjc2Mi4xLjQ3NC40NzQsMCwwLDEtLjQ1Ni0uMjQxLDEuODE5LDEuODE5LDAsMCwxLS4xODItLjYxOCwxLjQxMSwxLjQxMSwwLDAsMC0uMTQzLS41MjFxLTIuODUyLS40MjktMi44NTItMS4zNDFhLjQyLjQyLDAsMCwxLC4zNjUtLjQ1Niw1Ljc5Myw1Ljc5MywwLDAsMCwxLjA0Mi0uMyw1LjUyNCw1LjUyNCwwLDAsMCwxLjg4MS0xLjMxNSw2Ljc4OSw2Ljc4OSwwLDAsMCwxLjMzNC0xLjk1M0ExLjU3NSwxLjU3NSwwLDAsMCw2LDEyLjlhLjcyOC43MjgsMCwwLDAtLjQxLS42MTgsMy4zMjMsMy4zMjMsMCwwLDAtLjktLjM4NCwyLjkxMiwyLjkxMiwwLDAsMS0uOS0uNDEuODE0LjgxNCwwLDAsMS0uNDEtLjY4NC43MS43MSwwLDAsMSwuMzM4LS41OTMsMS4yMDgsMS4yMDgsMCwwLDEsLjcxNi0uMjQxLDEuOTc2LDEuOTc2LDAsMCwxLC42MjUuMTY5LDIuMDA4LDIuMDA4LDAsMCwwLC42OS4xNjkuOTE5LjkxOSwwLDAsMCwuNDE2LS4wOTFxLS4xMTctMS44NDktLjExNy0yLjQ3NEE1Ljg2MSw1Ljg2MSwwLDAsMSw2LjM4NSw1LjQsNS41MTYsNS41MTYsMCwwLDEsOC42MjUsMi44MTksNy4wNzUsNy4wNzUsMCwwLDEsMTIuMDYyLDJaIj48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXNvdW5kY2xvdWQiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik04LjksMTYuMUw5LDE0TDguOSw5LjVjMC0wLjEsMC0wLjEtMC4xLTAuMWMwLDAtMC4xLTAuMS0wLjEtMC4xYy0wLjEsMC0wLjEsMC0wLjEsMC4xYzAsMC0wLjEsMC4xLTAuMSwwLjFMOC4zLDE0bDAuMSwyLjEgYzAsMC4xLDAsMC4xLDAuMSwwLjFjMCwwLDAuMSwwLjEsMC4xLDAuMUM4LjgsMTYuMyw4LjksMTYuMyw4LjksMTYuMXogTTExLjQsMTUuOWwwLjEtMS44TDExLjQsOWMwLTAuMSwwLTAuMi0wLjEtMC4yIGMwLDAtMC4xLDAtMC4xLDBzLTAuMSwwLTAuMSwwYy0wLjEsMC0wLjEsMC4xLTAuMSwwLjJsMCwwLjFsLTAuMSw1YzAsMCwwLDAuNywwLjEsMnYwYzAsMC4xLDAsMC4xLDAuMSwwLjFjMC4xLDAuMSwwLjEsMC4xLDAuMiwwLjEgYzAuMSwwLDAuMSwwLDAuMi0wLjFjMC4xLDAsMC4xLTAuMSwwLjEtMC4yTDExLjQsMTUuOXogTTIuNCwxMi45TDIuNSwxNGwtMC4yLDEuMWMwLDAuMSwwLDAuMS0wLjEsMC4xYzAsMC0wLjEsMC0wLjEtMC4xTDIuMSwxNCBsMC4xLTEuMUMyLjIsMTIuOSwyLjMsMTIuOSwyLjQsMTIuOUMyLjMsMTIuOSwyLjQsMTIuOSwyLjQsMTIuOXogTTMuMSwxMi4yTDMuMywxNGwtMC4yLDEuOGMwLDAuMSwwLDAuMS0wLjEsMC4xIGMtMC4xLDAtMC4xLDAtMC4xLTAuMUwyLjgsMTRMMywxMi4yQzMsMTIuMiwzLDEyLjIsMy4xLDEyLjJDMy4xLDEyLjIsMy4xLDEyLjIsMy4xLDEyLjJ6IE0zLjksMTEuOUw0LjEsMTRsLTAuMiwyLjEgYzAsMC4xLDAsMC4xLTAuMSwwLjFjLTAuMSwwLTAuMSwwLTAuMS0wLjFMMy41LDE0bDAuMi0yLjFjMC0wLjEsMC0wLjEsMC4xLTAuMUMzLjksMTEuOCwzLjksMTEuOCwzLjksMTEuOXogTTQuNywxMS45TDQuOSwxNCBsLTAuMiwyLjFjMCwwLjEtMC4xLDAuMS0wLjEsMC4xYy0wLjEsMC0wLjEsMC0wLjEtMC4xTDQuMywxNGwwLjItMi4yYzAtMC4xLDAtMC4xLDAuMS0wLjFDNC43LDExLjcsNC43LDExLjgsNC43LDExLjl6IE01LjYsMTIgbDAuMiwybC0wLjIsMi4xYzAsMC4xLTAuMSwwLjEtMC4xLDAuMWMwLDAtMC4xLDAtMC4xLDBjMCwwLDAtMC4xLDAtMC4xTDUuMSwxNGwwLjItMmMwLDAsMC0wLjEsMC0wLjFzMC4xLDAsMC4xLDAgQzUuNSwxMS45LDUuNSwxMS45LDUuNiwxMkw1LjYsMTJ6IE02LjQsMTAuN0w2LjYsMTRsLTAuMiwyLjFjMCwwLDAsMC4xLDAsMC4xYzAsMC0wLjEsMC0wLjEsMGMtMC4xLDAtMC4xLTAuMS0wLjItMC4yTDUuOSwxNCBsMC4yLTMuM2MwLTAuMSwwLjEtMC4yLDAuMi0wLjJjMCwwLDAuMSwwLDAuMSwwQzYuNCwxMC43LDYuNCwxMC43LDYuNCwxMC43eiBNNy4yLDEwbDAuMiw0LjFsLTAuMiwyLjFjMCwwLDAsMC4xLDAsMC4xIGMwLDAtMC4xLDAtMC4xLDBjLTAuMSwwLTAuMi0wLjEtMC4yLTAuMmwtMC4xLTIuMUw2LjgsMTBjMC0wLjEsMC4xLTAuMiwwLjItMC4yYzAsMCwwLjEsMCwwLjEsMFM3LjIsOS45LDcuMiwxMHogTTgsOS42TDguMiwxNCBMOCwxNi4xYzAsMC4xLTAuMSwwLjItMC4yLDAuMmMtMC4xLDAtMC4yLTAuMS0wLjItMC4yTDcuNSwxNGwwLjEtNC40YzAtMC4xLDAtMC4xLDAuMS0wLjFjMCwwLDAuMS0wLjEsMC4xLTAuMWMwLjEsMCwwLjEsMCwwLjEsMC4xIEM4LDkuNiw4LDkuNiw4LDkuNnogTTExLjQsMTYuMUwxMS40LDE2LjFMMTEuNCwxNi4xeiBNOS43LDkuNkw5LjgsMTRsLTAuMSwyLjFjMCwwLjEsMCwwLjEtMC4xLDAuMnMtMC4xLDAuMS0wLjIsMC4xIGMtMC4xLDAtMC4xLDAtMC4xLTAuMXMtMC4xLTAuMS0wLjEtMC4yTDkuMiwxNGwwLjEtNC40YzAtMC4xLDAtMC4xLDAuMS0wLjJzMC4xLTAuMSwwLjItMC4xYzAuMSwwLDAuMSwwLDAuMiwwLjFTOS43LDkuNSw5LjcsOS42IEw5LjcsOS42eiBNMTAuNiw5LjhsMC4xLDQuM2wtMC4xLDJjMCwwLjEsMCwwLjEtMC4xLDAuMmMwLDAtMC4xLDAuMS0wLjIsMC4xYy0wLjEsMC0wLjEsMC0wLjItMC4xYzAsMC0wLjEtMC4xLTAuMS0wLjJMMTAsMTQgbDAuMS00LjNjMC0wLjEsMC0wLjEsMC4xLTAuMmMwLDAsMC4xLTAuMSwwLjItMC4xYzAuMSwwLDAuMSwwLDAuMiwwLjFTMTAuNiw5LjcsMTAuNiw5Ljh6IE0xMi40LDE0bC0wLjEsMmMwLDAuMSwwLDAuMS0wLjEsMC4yIGMtMC4xLDAuMS0wLjEsMC4xLTAuMiwwLjFjLTAuMSwwLTAuMSwwLTAuMi0wLjFjLTAuMS0wLjEtMC4xLTAuMS0wLjEtMC4ybC0wLjEtMWwtMC4xLTFsMC4xLTUuNXYwYzAtMC4xLDAtMC4yLDAuMS0wLjIgYzAuMSwwLDAuMS0wLjEsMC4yLTAuMWMwLDAsMC4xLDAsMC4xLDBjMC4xLDAsMC4xLDAuMSwwLjEsMC4yTDEyLjQsMTR6IE0yMi4xLDEzLjljMCwwLjctMC4yLDEuMy0wLjcsMS43Yy0wLjUsMC41LTEuMSwwLjctMS43LDAuNyBoLTYuOGMtMC4xLDAtMC4xLDAtMC4yLTAuMWMtMC4xLTAuMS0wLjEtMC4xLTAuMS0wLjJWOC4yYzAtMC4xLDAuMS0wLjIsMC4yLTAuM2MwLjUtMC4yLDEtMC4zLDEuNi0wLjNjMS4xLDAsMi4xLDAuNCwyLjksMS4xIGMwLjgsMC44LDEuMywxLjcsMS40LDIuOGMwLjMtMC4xLDAuNi0wLjIsMS0wLjJjMC43LDAsMS4zLDAuMiwxLjcsMC43QzIxLjgsMTIuNiwyMi4xLDEzLjIsMjIuMSwxMy45TDIyLjEsMTMuOXoiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tc3BvdGlmeSIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTEyLDJDNi40NzcsMiwyLDYuNDc3LDIsMTJjMCw1LjUyMyw0LjQ3NywxMCwxMCwxMGM1LjUyMywwLDEwLTQuNDc3LDEwLTEwQzIyLDYuNDc3LDE3LjUyMywyLDEyLDIgTTE2LjU4NiwxNi40MjQgYy0wLjE4LDAuMjk1LTAuNTYzLDAuMzg3LTAuODU3LDAuMjA3Yy0yLjM0OC0xLjQzNS01LjMwNC0xLjc2LTguNzg1LTAuOTY0Yy0wLjMzNSwwLjA3Ny0wLjY3LTAuMTMzLTAuNzQ2LTAuNDY5IGMtMC4wNzctMC4zMzUsMC4xMzItMC42NywwLjQ2OS0wLjc0NmMzLjgwOS0wLjg3MSw3LjA3Ny0wLjQ5Niw5LjcxMywxLjExNUMxNi42NzMsMTUuNzQ2LDE2Ljc2NiwxNi4xMywxNi41ODYsMTYuNDI0IE0xNy44MSwxMy43IGMtMC4yMjYsMC4zNjctMC43MDYsMC40ODItMS4wNzIsMC4yNTdjLTIuNjg3LTEuNjUyLTYuNzg1LTIuMTMxLTkuOTY1LTEuMTY2QzYuMzYsMTIuOTE3LDUuOTI1LDEyLjY4NCw1LjgsMTIuMjczIEM1LjY3NSwxMS44Niw1LjkwOCwxMS40MjUsNi4zMiwxMS4zYzMuNjMyLTEuMTAyLDguMTQ3LTAuNTY4LDExLjIzNCwxLjMyOEMxNy45MiwxMi44NTQsMTguMDM1LDEzLjMzNSwxNy44MSwxMy43IE0xNy45MTUsMTAuODY1IGMtMy4yMjMtMS45MTQtOC41NC0yLjA5LTExLjYxOC0xLjE1NkM1LjgwNCw5Ljg1OSw1LjI4MSw5LjU4LDUuMTMxLDkuMDg2QzQuOTgyLDguNTkxLDUuMjYsOC4wNjksNS43NTUsNy45MTkgYzMuNTMyLTEuMDcyLDkuNDA0LTAuODY1LDEzLjExNSwxLjMzOGMwLjQ0NSwwLjI2NCwwLjU5LDAuODM4LDAuMzI3LDEuMjgyQzE4LjkzMywxMC45ODMsMTguMzU5LDExLjEyOSwxNy45MTUsMTAuODY1Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXN0YWNrb3ZlcmZsb3ciIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Im0gMTcuODE3MTI4LDIwLjIyODYwNSB2IC01LjMzNzIxNyBoIDEuNzcxNDMxIFYgMjIgSCAzLjYgdiAtNy4xMDg2MTIgaCAxLjc3MTQwMSB2IDUuMzM3MjE3IHoiPjwvcGF0aD4KPHBhdGggZD0ibSA3LjMyNjc4NDUsMTQuMzg1MzU5IDguNjk1OTI5NSwxLjgxNzMxNiAwLjM2ODE2OCwtMS43NDgzODUgLTguNjk1OTMxOCwtMS44MTczMTkgeiBtIDEuMTUwMzE5NywtNC4xNDA5NDQgOC4wNTE3OTY4LDMuNzQ5ODcyIDAuNzM2MTcsLTEuNjEwMzg1IC04LjA1MTgzNDQsLTMuNzcyODUxNyB6IG0gMi4yMzE1MDc4LC0zLjk1NjkxNTQgNi44MzI0MDUsNS42ODIyNjY0IDEuMTI3MzgsLTEuMzU3MzE2IC02LjgzMjU3NiwtNS42ODIyNjM2IHogbSA0LjQxNywtNC4yMDk5MDE5IC0xLjQyNjQ0OCwxLjA1ODE4NjQgNS4yOTExOTEsNy4xMzE2MTE5IDEuNDI2NDEyLC0xLjA1ODI3NDUgeiBNIDcuMTQyNzI5NiwxOC40MzQxODkgaCA4Ljg3OTk4NDQgdiAtMS43NzEzIEggNy4xNDI3Mjk2IFoiPjwvcGF0aD4KPHBhdGggZD0ibSAxNy44MTcxMjgsMjAuMjI4NjA1IHYgLTUuMzM3MjE3IGggMS43NzE0MzEgViAyMiBIIDMuNiB2IC03LjEwODYxMiBoIDEuNzcxNDAxIHYgNS4zMzcyMTcgeiI+PC9wYXRoPgo8cGF0aCBkPSJtIDcuMzI2Nzg0NSwxNC4zODUzNTkgOC42OTU5Mjk1LDEuODE3MzE2IDAuMzY4MTY4LC0xLjc0ODM4NSAtOC42OTU5MzE4LC0xLjgxNzMxOSB6IG0gMS4xNTAzMTk3LC00LjE0MDk0NCA4LjA1MTc5NjgsMy43NDk4NzIgMC43MzYxNywtMS42MTAzODUgLTguMDUxODM0NCwtMy43NzI4NTE3IHogbSAyLjIzMTUwNzgsLTMuOTU2OTE1NCA2LjgzMjQwNSw1LjY4MjI2NjQgMS4xMjczOCwtMS4zNTczMTYgLTYuODMyNTc2LC01LjY4MjI2MzYgeiBtIDQuNDE3LC00LjIwOTkwMTkgLTEuNDI2NDQ4LDEuMDU4MTg2NCA1LjI5MTE5MSw3LjEzMTYxMTkgMS40MjY0MTIsLTEuMDU4Mjc0NSB6IE0gNy4xNDI3Mjk2LDE4LjQzNDE4OSBoIDguODc5OTg0NCB2IC0xLjc3MTMgSCA3LjE0MjcyOTYgWiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi1zdHJhdmEiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xNS4zODcgMTcuOTQ0bC0yLjA4OS00LjExNmgtMy4wNjVMMTUuMzg3IDI0bDUuMTUtMTAuMTcyaC0zLjA2Nm0tNy4wMDgtNS41OTlsMi44MzYgNS41OThoNC4xNzJMMTAuNDYzIDBsLTcgMTMuODI4aDQuMTY5Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXN0dW1ibGV1cG9uIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTIsNC4yOTRjLTIuNDY5LDAtNC40NzEsMi4wMDItNC40NzEsNC40NzF2Ni4zNTNjMCwwLjU4NS0wLjQ3NCwxLjA1OS0xLjA1OSwxLjA1OWMtMC41ODUsMC0xLjA1OS0wLjQ3NC0xLjA1OS0xLjA1OSB2LTIuODI0SDJ2Mi45NDFjMCwyLjQ2OSwyLjAwMiw0LjQ3MSw0LjQ3MSw0LjQ3MWMyLjQ2OSwwLDQuNDcxLTIuMDAyLDQuNDcxLTQuNDcxVjguNzY1YzAtMC41ODUsMC40NzQtMS4wNTksMS4wNTktMS4wNTkgczEuMDU5LDAuNDc0LDEuMDU5LDEuMDU5djEuMjk0bDEuNDEyLDAuNjQ3bDItMC42NDdWOC43NjVDMTYuNDcxLDYuMjk2LDE0LjQ2OSw0LjI5NCwxMiw0LjI5NHogTTEzLjA1OSwxMi4zNTN2Mi44ODIgYzAsMi40NjksMi4wMDIsNC40NzEsNC40NzEsNC40NzFTMjIsMTcuNzA0LDIyLDE1LjIzNXYtMi44MjRoLTMuNDEydjIuODI0YzAsMC41ODUtMC40NzQsMS4wNTktMS4wNTksMS4wNTkgYy0wLjU4NSwwLTEuMDU5LTAuNDc0LTEuMDU5LTEuMDU5di0yLjg4MmwtMiwwLjY0N0wxMy4wNTksMTIuMzUzeiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi10ZWxlZ3JhbSIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTEyIDJDNi40NzcgMiAyIDYuNDc3IDIgMTJzNC40NzcgMTAgMTAgMTAgMTAtNC40NzcgMTAtMTBTMTcuNTIzIDIgMTIgMnptMy4wOCAxNC43NTdzLS4yNS42MjUtLjkzNi4zMjVsLTIuNTQxLTEuOTQ5LTEuNjMgMS40ODZzLS4xMjcuMDk2LS4yNjYuMDM2YzAgMC0uMTItLjAxMS0uMjctLjQ4Ni0uMTUtLjQ3NS0uOTExLTIuOTcyLS45MTEtMi45NzJMNiAxMi4zNDlzLS4zODctLjEzNy0uNDI1LS40MzhjLS4wMzctLjMuNDM3LS40NjIuNDM3LS40NjJsMTAuMDMtMy45MzRzLjgyNC0uMzYyLjgyNC4yMzhsLTEuNzg2IDkuMDA0eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi10aWt0b2siIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xMi4yMiAySDE1LjY0QzE1LjY0IDIgMTUuNDUwMiA2LjM5MzUxIDIwLjM4OTggNi43MDE4NlYxMC4wOTgxQzIwLjM4OTggMTAuMDk4MSAxNy43NTM3IDEwLjI2MzYgMTUuNjQgOC42NDk1N0wxNS42NzY5IDE1LjY2MTVDMTUuNjc2OSAxNi45MTUxIDE1LjMwNTIgMTguMTQwNiAxNC42MDg3IDE5LjE4MjlDMTMuOTEyMyAyMC4yMjUzIDEyLjkyMjQgMjEuMDM3NyAxMS43NjQyIDIxLjUxNzVDMTAuNjA2IDIxLjk5NzIgOS4zMzE2MiAyMi4xMjI4IDguMTAyMDkgMjEuODc4MkM2Ljg3MjU3IDIxLjYzMzcgNS43NDMxNiAyMS4wMzAxIDQuODU2NjkgMjAuMTQzN0MzLjk3MDIyIDE5LjI1NzMgMy4zNjY1IDE4LjEyNzkgMy4xMjE4NiAxNi44OTg0QzIuODc3MjMgMTUuNjY4OSAzLjAwMjY3IDE0LjM5NDUgMy40ODIzMyAxMy4yMzYzQzMuOTYxOTkgMTIuMDc4MSA0Ljc3NDMyIDExLjA4ODEgNS44MTY2IDEwLjM5MTZDNi44NTg4OCA5LjY5NTAyIDguMDg0MyA5LjMyMzE4IDkuMzM3OTEgOS4zMjMwN0gxMC4yMjcxVjEyLjcyMzFWMTIuNzk1NEM5LjY0NzU3IDEyLjYxNDcgOS4wMjU3OCAxMi42MjE3IDguNDUwNDMgMTIuODE1MkM3Ljg3NTA4IDEzLjAwODggNy4zNzU1NiAxMy4zNzkyIDcuMDIzMTQgMTMuODczNEM2LjY3MDcxIDE0LjM2NzcgNi40ODMzOCAxNC45NjA2IDYuNDg3ODYgMTUuNTY3N0M2LjQ5MjM1IDE2LjE3NDcgNi42ODg0MiAxNi43NjQ4IDcuMDQ4MTEgMTcuMjUzOEM3LjQwNzgxIDE3Ljc0MjggNy45MTI3NCAxOC4xMDU3IDguNDkwODkgMTguMjkwOEM5LjA2OTAzIDE4LjQ3NTggOS42OTA4NiAxOC40NzM2IDEwLjI2NzYgMTguMjg0M0MxMC44NDQ0IDE4LjA5NTEgMTEuMzQ2NyAxNy43Mjg1IDExLjcwMjkgMTcuMjM2OUMxMi4wNTkgMTYuNzQ1NCAxMi4yNTA4IDE2LjE1MzggMTIuMjUwOSAxNS41NDY4TDEyLjIyIDJaIj48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXR1bWJsciIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTE2Ljc0OSwxNy4zOTZjLTAuMzU3LDAuMTctMS4wNDEsMC4zMTktMS41NTEsMC4zMzJjLTEuNTM5LDAuMDQxLTEuODM3LTEuMDgxLTEuODUtMS44OTZWOS44NDdoMy44NjFWNi45MzdoLTMuODQ3VjIuMDM5IGMwLDAtMi43NywwLTIuODE3LDBjLTAuMDQ2LDAtMC4xMjcsMC4wNDEtMC4xMzgsMC4xNDRjLTAuMTY1LDEuNDk5LTAuODY3LDQuMTMtMy43ODMsNS4xODF2Mi40ODRoMS45NDV2Ni4yODIgYzAsMi4xNTEsMS41ODcsNS4yMDYsNS43NzUsNS4xMzVjMS40MTMtMC4wMjQsMi45ODItMC42MTYsMy4zMjktMS4xMjZMMTYuNzQ5LDE3LjM5NnoiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24tdHdpdGNoIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTYuNDk5LDguMDg5aC0xLjYzNnY0LjkxaDEuNjM2VjguMDg5eiBNMTIsOC4wODloLTEuNjM3djQuOTFIMTJWOC4wODl6IE00LjIyOCwzLjE3OEwzLDYuNDUxdjEzLjA5Mmg0LjQ5OVYyMmgyLjQ1NiBsMi40NTQtMi40NTZoMy42ODFMMjEsMTQuNjM2VjMuMTc4SDQuMjI4eiBNMTkuMzY0LDEzLjgxNmwtMi44NjQsMi44NjVIMTJsLTIuNDUzLDIuNDUzVjE2LjY4SDUuODYzVjQuODE0aDEzLjUwMVYxMy44MTZ6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXR3aXR0ZXIiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0yMi4yMyw1LjkyNGMtMC43MzYsMC4zMjYtMS41MjcsMC41NDctMi4zNTcsMC42NDZjMC44NDctMC41MDgsMS40OTgtMS4zMTIsMS44MDQtMi4yNyBjLTAuNzkzLDAuNDctMS42NzEsMC44MTItMi42MDYsMC45OTZDMTguMzI0LDQuNDk4LDE3LjI1Nyw0LDE2LjA3Nyw0Yy0yLjI2NiwwLTQuMTAzLDEuODM3LTQuMTAzLDQuMTAzIGMwLDAuMzIyLDAuMDM2LDAuNjM1LDAuMTA2LDAuOTM1QzguNjcsOC44NjcsNS42NDcsNy4yMzQsMy42MjMsNC43NTFDMy4yNyw1LjM1NywzLjA2Nyw2LjA2MiwzLjA2Nyw2LjgxNCBjMCwxLjQyNCwwLjcyNCwyLjY3OSwxLjgyNSwzLjQxNWMtMC42NzMtMC4wMjEtMS4zMDUtMC4yMDYtMS44NTktMC41MTNjMCwwLjAxNywwLDAuMDM0LDAsMC4wNTJjMCwxLjk4OCwxLjQxNCwzLjY0NywzLjI5Miw0LjAyMyBjLTAuMzQ0LDAuMDk0LTAuNzA3LDAuMTQ0LTEuMDgxLDAuMTQ0Yy0wLjI2NCwwLTAuNTIxLTAuMDI2LTAuNzcyLTAuMDc0YzAuNTIyLDEuNjMsMi4wMzgsMi44MTYsMy44MzMsMi44NSBjLTEuNDA0LDEuMS0zLjE3NCwxLjc1Ni01LjA5NiwxLjc1NmMtMC4zMzEsMC0wLjY1OC0wLjAxOS0wLjk3OS0wLjA1N2MxLjgxNiwxLjE2NCwzLjk3MywxLjg0Myw2LjI5LDEuODQzIGM3LjU0NywwLDExLjY3NS02LjI1MiwxMS42NzUtMTEuNjc1YzAtMC4xNzgtMC4wMDQtMC4zNTUtMC4wMTItMC41MzFDMjAuOTg1LDcuNDcsMjEuNjgsNi43NDcsMjIuMjMsNS45MjR6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXZpbWVvIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMjIuMzk2LDcuMTY0Yy0wLjA5MywyLjAyNi0xLjUwNyw0Ljc5OS00LjI0NSw4LjMyQzE1LjMyMiwxOS4xNjEsMTIuOTI4LDIxLDEwLjk3LDIxYy0xLjIxNCwwLTIuMjQtMS4xMTktMy4wNzktMy4zNTkgYy0wLjU2LTIuMDUzLTEuMTE5LTQuMTA2LTEuNjgtNi4xNTlDNS41ODgsOS4yNDMsNC45MjEsOC4xMjIsNC4yMDYsOC4xMjJjLTAuMTU2LDAtMC43MDEsMC4zMjgtMS42MzQsMC45OEwxLjU5NCw3Ljg0MSBjMS4wMjctMC45MDIsMi4wNC0xLjgwNSwzLjAzNy0yLjcwOEM2LjAwMSwzLjk1LDcuMDMsMy4zMjcsNy43MTUsMy4yNjRjMS42MTktMC4xNTYsMi42MTYsMC45NTEsMi45OSwzLjMyMSBjMC40MDQsMi41NTcsMC42ODUsNC4xNDcsMC44NDEsNC43NjljMC40NjcsMi4xMjEsMC45ODEsMy4xODEsMS41NDIsMy4xODFjMC40MzUsMCwxLjA5LTAuNjg4LDEuOTYzLTIuMDY1IGMwLjg3MS0xLjM3NiwxLjMzOC0yLjQyMiwxLjQwMS0zLjE0MmMwLjEyNS0xLjE4Ny0wLjM0My0xLjc4Mi0xLjQwMS0xLjc4MmMtMC40OTgsMC0xLjAxMiwwLjExNS0xLjU0MSwwLjM0MSBjMS4wMjMtMy4zNSwyLjk3Ny00Ljk3Nyw1Ljg2Mi00Ljg4NEMyMS41MTEsMy4wNjYsMjIuNTIsNC40NTMsMjIuMzk2LDcuMTY0eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi12ayIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTIyLDcuMWMwLjIsMC40LTAuNCwxLjUtMS42LDMuMWMtMC4yLDAuMi0wLjQsMC41LTAuNywwLjljLTAuNSwwLjctMC45LDEuMS0wLjksMS40Yy0wLjEsMC4zLTAuMSwwLjYsMC4xLDAuOCBjMC4xLDAuMSwwLjQsMC40LDAuOCwwLjloMGwwLDBjMSwwLjksMS42LDEuNywyLDIuM2MwLDAsMCwwLjEsMC4xLDAuMWMwLDAuMSwwLDAuMSwwLjEsMC4zYzAsMC4xLDAsMC4yLDAsMC40IGMwLDAuMS0wLjEsMC4yLTAuMywwLjNjLTAuMSwwLjEtMC40LDAuMS0wLjYsMC4xbC0yLjcsMGMtMC4yLDAtMC40LDAtMC42LTAuMWMtMC4yLTAuMS0wLjQtMC4xLTAuNS0wLjJsLTAuMi0wLjEgYy0wLjItMC4xLTAuNS0wLjQtMC43LTAuN3MtMC41LTAuNi0wLjctMC44Yy0wLjItMC4yLTAuNC0wLjQtMC42LTAuNkMxNC44LDE1LDE0LjYsMTUsMTQuNCwxNWMwLDAsMCwwLTAuMSwwYzAsMC0wLjEsMC4xLTAuMiwwLjIgYy0wLjEsMC4xLTAuMiwwLjItMC4yLDAuM2MtMC4xLDAuMS0wLjEsMC4zLTAuMiwwLjVjLTAuMSwwLjItMC4xLDAuNS0wLjEsMC44YzAsMC4xLDAsMC4yLDAsMC4zYzAsMC4xLTAuMSwwLjItMC4xLDAuMmwwLDAuMSBjLTAuMSwwLjEtMC4zLDAuMi0wLjYsMC4yaC0xLjJjLTAuNSwwLTEsMC0xLjUtMC4yYy0wLjUtMC4xLTEtMC4zLTEuNC0wLjZzLTAuNy0wLjUtMS4xLTAuN3MtMC42LTAuNC0wLjctMC42bC0wLjMtMC4zIGMtMC4xLTAuMS0wLjItMC4yLTAuMy0wLjNzLTAuNC0wLjUtMC43LTAuOXMtMC43LTEtMS4xLTEuNmMtMC40LTAuNi0wLjgtMS4zLTEuMy0yLjJDMi45LDkuNCwyLjUsOC41LDIuMSw3LjVDMiw3LjQsMiw3LjMsMiw3LjIgYzAtMC4xLDAtMC4xLDAtMC4ybDAtMC4xYzAuMS0wLjEsMC4zLTAuMiwwLjYtMC4ybDIuOSwwYzAuMSwwLDAuMiwwLDAuMiwwLjFTNS45LDYuOSw1LjksN0w2LDdjMC4xLDAuMSwwLjIsMC4yLDAuMywwLjMgQzYuNCw3LjcsNi41LDgsNi43LDguNEM2LjksOC44LDcsOSw3LjEsOS4ybDAuMiwwLjNjMC4yLDAuNCwwLjQsMC44LDAuNiwxLjFjMC4yLDAuMywwLjQsMC41LDAuNSwwLjdzMC4zLDAuMywwLjQsMC40IGMwLjEsMC4xLDAuMywwLjEsMC40LDAuMWMwLjEsMCwwLjIsMCwwLjMtMC4xYzAsMCwwLDAsMC4xLTAuMWMwLDAsMC4xLTAuMSwwLjEtMC4yYzAuMS0wLjEsMC4xLTAuMywwLjEtMC41YzAtMC4yLDAuMS0wLjUsMC4xLTAuOCBjMC0wLjQsMC0wLjgsMC0xLjNjMC0wLjMsMC0wLjUtMC4xLTAuOGMwLTAuMi0wLjEtMC40LTAuMS0wLjVMOS42LDcuNkM5LjQsNy4zLDkuMSw3LjIsOC43LDcuMUM4LjYsNy4xLDguNiw3LDguNyw2LjkgQzguOSw2LjcsOSw2LjYsOS4xLDYuNWMwLjQtMC4yLDEuMi0wLjMsMi41LTAuM2MwLjYsMCwxLDAuMSwxLjQsMC4xYzAuMSwwLDAuMywwLjEsMC4zLDAuMWMwLjEsMC4xLDAuMiwwLjEsMC4yLDAuMyBjMCwwLjEsMC4xLDAuMiwwLjEsMC4zczAsMC4zLDAsMC41YzAsMC4yLDAsMC40LDAsMC42YzAsMC4yLDAsMC40LDAsMC43YzAsMC4zLDAsMC42LDAsMC45YzAsMC4xLDAsMC4yLDAsMC40YzAsMC4yLDAsMC40LDAsMC41IGMwLDAuMSwwLDAuMywwLDAuNHMwLjEsMC4zLDAuMSwwLjRjMC4xLDAuMSwwLjEsMC4yLDAuMiwwLjNjMC4xLDAsMC4xLDAsMC4yLDBjMC4xLDAsMC4yLDAsMC4zLTAuMWMwLjEtMC4xLDAuMi0wLjIsMC40LTAuNCBzMC4zLTAuNCwwLjUtMC43YzAuMi0wLjMsMC41LTAuNywwLjctMS4xYzAuNC0wLjcsMC44LTEuNSwxLjEtMi4zYzAtMC4xLDAuMS0wLjEsMC4xLTAuMmMwLTAuMSwwLjEtMC4xLDAuMS0wLjFsMCwwbDAuMSwwIGMwLDAsMCwwLDAuMSwwczAuMiwwLDAuMiwwbDMsMGMwLjMsMCwwLjUsMCwwLjcsMFMyMS45LDcsMjEuOSw3TDIyLDcuMXoiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24td2hhdHNhcHAiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0yLjA0OCwyMmwxLjQwNi01LjEzNmMtMC44NjctMS41MDMtMS4zMjQtMy4yMDgtMS4zMjMtNC45NTVDMi4xMzMsNi40NDYsNi41NzksMiwxMi4wNDIsMmMyLjY1MSwwLjAwMSw1LjE0LDEuMDMzLDcuMDExLDIuOTA2YzEuODcxLDEuODczLDIuOTAxLDQuMzYzLDIuOSw3LjAxMWMtMC4wMDIsNS40NjQtNC40NDgsOS45MS05LjkxLDkuOTFjMCwwLDAsMCwwLDBoLTAuMDA0Yy0xLjY1OS0wLjAwMS0zLjI4OC0wLjQxNy00LjczNi0xLjIwNkwyLjA0OCwyMnogTTcuNTQ1LDE4LjgyOGwwLjMwMSwwLjE3OWMxLjI2NSwwLjc1MSwyLjcxNCwxLjE0OCw0LjE5MywxLjE0OGgwLjAwM2M0LjU0LDAsOC4yMzUtMy42OTUsOC4yMzctOC4yMzdjMC4wMDEtMi4yMDEtMC44NTUtNC4yNzEtMi40MS01LjgyOGMtMS41NTUtMS41NTctMy42MjMtMi40MTUtNS44MjQtMi40MTZjLTQuNTQ0LDAtOC4yMzksMy42OTUtOC4yNDEsOC4yMzdjLTAuMDAxLDEuNTU2LDAuNDM1LDMuMDcyLDEuMjU5LDQuMzg0bDAuMTk2LDAuMzEybC0wLjgzMiwzLjA0TDcuNTQ1LDE4LjgyOHogTTE3LjAzNSwxNC4yNzRjLTAuMDYyLTAuMTAzLTAuMjI3LTAuMTY1LTAuNDc1LTAuMjg5Yy0wLjI0OC0wLjEyNC0xLjQ2NS0wLjcyMy0xLjY5Mi0wLjgwNmMtMC4yMjctMC4wODMtMC4zOTItMC4xMjQtMC41NTcsMC4xMjRjLTAuMTY1LDAuMjQ4LTAuNjQsMC44MDYtMC43ODQsMC45NzFjLTAuMTQ0LDAuMTY1LTAuMjg5LDAuMTg2LTAuNTM2LDAuMDYyYy0wLjI0OC0wLjEyNC0xLjA0Ni0wLjM4NS0xLjk5MS0xLjIyOWMtMC43MzYtMC42NTctMS4yMzMtMS40NjgtMS4zNzgtMS43MTVjLTAuMTQ0LTAuMjQ4LTAuMDE1LTAuMzgyLDAuMTA5LTAuNTA1YzAuMTExLTAuMTExLDAuMjQ4LTAuMjg5LDAuMzcxLTAuNDM0YzAuMTI0LTAuMTQ1LDAuMTY1LTAuMjQ4LDAuMjQ4LTAuNDEzYzAuMDgzLTAuMTY1LDAuMDQxLTAuMzEtMC4wMjEtMC40MzRjLTAuMDYyLTAuMTI0LTAuNTU3LTEuMzQzLTAuNzYzLTEuODM5QzkuMzY0LDcuMjg0LDkuMTU5LDcuMzUsOS4wMDcsNy4zNDJjLTAuMTQ0LTAuMDA3LTAuMzEtMC4wMDktMC40NzUtMC4wMDljLTAuMTY1LDAtMC40MzMsMC4wNjItMC42NiwwLjMxQzcuNjQ2LDcuODkxLDcuMDA2LDguNDksNy4wMDYsOS43MDljMCwxLjIxOSwwLjg4NywyLjM5NiwxLjAxMSwyLjU2MmMwLjEyNCwwLjE2NSwxLjc0NiwyLjY2Niw0LjIzLDMuNzM5YzAuNTkxLDAuMjU1LDEuMDUyLDAuNDA4LDEuNDEyLDAuNTIyYzAuNTkzLDAuMTg5LDEuMTMzLDAuMTYyLDEuNTYsMC4wOThjMC40NzYtMC4wNzEsMS40NjUtMC41OTksMS42NzEtMS4xNzdDMTcuMDk2LDE0Ljg3MywxNy4wOTYsMTQuMzc4LDE3LjAzNSwxNC4yNzR6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8c3ltYm9sIGlkPSJpY29uLXdvb2NvbW1lcmNlIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMTksMkg1QzMuMywyLDIsMy4zLDIsNXYxMWMwLDEuNywxLjMsMywzLDNoNGw2LDNsLTEtM2g1YzEuNywwLDMtMS4zLDMtM1Y1QzIyLDMuMywyMC43LDIsMTksMnogTTE3LjQsNi41Yy0wLjQsMC44LTAuOCwyLjEtMSwzLjljLTAuMywxLjgtMC40LDMuMS0wLjMsNC4xYzAsMC4zLDAsMC41LTAuMSwwLjdzLTAuMywwLjQtMC42LDAuNHMtMC42LTAuMS0wLjktMC40Yy0xLTEtMS44LTIuNi0yLjQtNC42Yy0wLjcsMS40LTEuMiwyLjQtMS42LDMuMWMtMC42LDEuMi0xLjIsMS44LTEuNiwxLjljLTAuMywwLTAuNS0wLjItMC44LTAuN0M3LjYsMTMuNSw3LDEwLjcsNi40LDYuN2MwLTAuMywwLTAuNSwwLjItMC43QzYuNyw1LjgsNyw1LjcsNy4zLDUuNmMwLjUsMCwwLjksMC4yLDAuOSwwLjhjMC4zLDIuMywwLjcsNC4yLDEuMSw1LjdsMi40LTQuNUMxMS45LDcuMiwxMi4xLDcsMTIuNSw3YzAuNSwwLDAuOCwwLjMsMC45LDAuOWMwLjMsMS40LDAuNiwyLjYsMSwzLjdjMC4zLTIuNywwLjgtNC43LDEuNC01LjljMC4yLTAuMywwLjQtMC41LDAuNy0wLjVjMC4yLDAsMC41LDAuMSwwLjcsMC4yYzAuMiwwLjIsMC4zLDAuNCwwLjMsMC42UzE3LjUsNi40LDE3LjQsNi41eiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi13b3JkcHJlc3MiIHZpZXdib3g9IjAgMCAyNCAyNCI+CjxwYXRoIGQ9Ik0xMi4xNTgsMTIuNzg2TDkuNDYsMjAuNjI1YzAuODA2LDAuMjM3LDEuNjU3LDAuMzY2LDIuNTQsMC4zNjZjMS4wNDcsMCwyLjA1MS0wLjE4MSwyLjk4Ni0wLjUxIGMtMC4wMjQtMC4wMzgtMC4wNDYtMC4wNzktMC4wNjUtMC4xMjRMMTIuMTU4LDEyLjc4NnogTTMuMDA5LDEyYzAsMy41NTksMi4wNjgsNi42MzQsNS4wNjcsOC4wOTJMMy43ODgsOC4zNDEgQzMuMjg5LDkuNDU5LDMuMDA5LDEwLjY5NiwzLjAwOSwxMnogTTE4LjA2OSwxMS41NDZjMC0xLjExMi0wLjM5OS0xLjg4MS0wLjc0MS0yLjQ4Yy0wLjQ1Ni0wLjc0MS0wLjg4My0xLjM2OC0wLjg4My0yLjEwOSBjMC0wLjgyNiwwLjYyNy0xLjU5NiwxLjUxLTEuNTk2YzAuMDQsMCwwLjA3OCwwLjAwNSwwLjExNiwwLjAwN0MxNi40NzIsMy45MDQsMTQuMzQsMy4wMDksMTIsMy4wMDkgYy0zLjE0MSwwLTUuOTA0LDEuNjEyLTcuNTEyLDQuMDUyYzAuMjExLDAuMDA3LDAuNDEsMC4wMTEsMC41NzksMC4wMTFjMC45NCwwLDIuMzk2LTAuMTE0LDIuMzk2LTAuMTE0IEM3Ljk0Nyw2LjkzLDguMDA0LDcuNjQyLDcuNTIsNy42OTljMCwwLTAuNDg3LDAuMDU3LTEuMDI5LDAuMDg1bDMuMjc0LDkuNzM5bDEuOTY4LTUuOTAxbC0xLjQwMS0zLjgzOCBDOS44NDgsNy43NTYsOS4zODksNy42OTksOS4zODksNy42OTlDOC45MDQsNy42Nyw4Ljk2MSw2LjkzLDkuNDQ2LDYuOTU4YzAsMCwxLjQ4NCwwLjExNCwyLjM2OCwwLjExNCBjMC45NCwwLDIuMzk3LTAuMTE0LDIuMzk3LTAuMTE0YzAuNDg1LTAuMDI4LDAuNTQyLDAuNjg0LDAuMDU3LDAuNzQxYzAsMC0wLjQ4OCwwLjA1Ny0xLjAyOSwwLjA4NWwzLjI0OSw5LjY2NWwwLjg5Ny0yLjk5NiBDMTcuODQxLDEzLjI4NCwxOC4wNjksMTIuMzE2LDE4LjA2OSwxMS41NDZ6IE0xOS44ODksNy42ODZjMC4wMzksMC4yODYsMC4wNiwwLjU5MywwLjA2LDAuOTI0YzAsMC45MTItMC4xNzEsMS45MzgtMC42ODQsMy4yMiBsLTIuNzQ2LDcuOTRjMi42NzMtMS41NTgsNC40Ny00LjQ1NCw0LjQ3LTcuNzcxQzIwLjk5MSwxMC40MzYsMjAuNTkxLDguOTY3LDE5Ljg4OSw3LjY4NnogTTEyLDIyQzYuNDg2LDIyLDIsMTcuNTE0LDIsMTIgQzIsNi40ODYsNi40ODYsMiwxMiwyYzUuNTE0LDAsMTAsNC40ODYsMTAsMTBDMjIsMTcuNTE0LDE3LjUxNCwyMiwxMiwyMnoiPjwvcGF0aD4KPC9zeW1ib2w+CjxzeW1ib2wgaWQ9Imljb24teWVscCIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTEyLjI3MSwxNi43MTh2MS40MTdxLS4wMTEsMy4yNTctLjA2NywzLjRhLjcwNy43MDcsMCwwLDEtLjU2OS40NDYsNC42MzcsNC42MzcsMCwwLDEtMi4wMjQtLjQyNEE0LjYwOSw0LjYwOSwwLDAsMSw3LjgsMjAuNTY1YS44NDQuODQ0LDAsMCwxLS4xOS0uNC42OTIuNjkyLDAsMCwxLC4wNDQtLjI5LDMuMTgxLDMuMTgxLDAsMCwxLC4zNzktLjUyNHEuMzM1LS40MTIsMi4wMTktMi40MDkuMDExLDAsLjY2OS0uNzgxYS43NTcuNzU3LDAsMCwxLC40NC0uMjc0Ljk2NS45NjUsMCwwLDEsLjU1Mi4wMzkuOTQ1Ljk0NSwwLDAsMSwuNDE4LjMyNC43MzIuNzMyLDAsMCwxLC4xMzkuNDY4Wm0tMS42NjItMi44YS43ODMuNzgzLDAsMCwxLS41OC43ODFsLTEuMzM5LjQzNXEtMy4wNjcuOTgxLTMuMjU3Ljk4MWEuNzExLjcxMSwwLDAsMS0uNi0uNCwyLjYzNiwyLjYzNiwwLDAsMS0uMTktLjgzNiw5LjEzNCw5LjEzNCwwLDAsMSwuMDExLTEuODU3LDMuNTU5LDMuNTU5LDAsMCwxLC4zMzUtMS4zODkuNjU5LjY1OSwwLDAsMSwuNjI1LS4zNTcsMjIuNjI5LDIyLjYyOSwwLDAsMSwyLjI1My44NTlxLjc4MS4zMjQsMS4yODMuNTI0bC45MzcuMzc5YS43NzEuNzcxLDAsMCwxLC40LjM0QS45ODIuOTgyLDAsMCwxLDEwLjYwOSwxMy45MTdabTkuMjEzLDMuMzEzYTQuNDY3LDQuNDY3LDAsMCwxLTEuMDIxLDEuOCw0LjU1OSw0LjU1OSwwLDAsMS0xLjUxMiwxLjQxNy42NzEuNjcxLDAsMCwxLS43LS4wNzhxLS4xNTYtLjExMi0yLjA1Mi0zLjJsLS41MjQtLjg1OWEuNzYxLjc2MSwwLDAsMS0uMTI4LS41MTMuOTU3Ljk1NywwLDAsMSwuMjE3LS41MTMuNzc0Ljc3NCwwLDAsMSwuOTI2LS4yOXEuMDExLjAxMSwxLjMyNy40NDYsMi4yNjQuNzM2LDIuNy44ODdhMi4wODIsMi4wODIsMCwwLDEsLjUyNC4yMjkuNjczLjY3MywwLDAsMSwuMjQ1LjY4Wm0tNy41LTcuMDQ5cS4wNTYsMS4xMzctLjYsMS4zNjEtLjY0Ny4xOS0xLjI3Mi0uNzkyTDYuMjM3LDQuMDhhLjcuNywwLDAsMSwuMjEyLS42OTEsNS43ODgsNS43ODgsMCwwLDEsMi4zMTQtMSw1LjkyOCw1LjkyOCwwLDAsMSwyLjUtLjM1Mi42ODEuNjgxLDAsMCwxLC41NDcuNXEuMDM0LjIuMjQ1LDMuNDA3VDEyLjMyNywxMC4xODFabTcuMzg0LDEuMmEuNjc5LjY3OSwwLDAsMS0uMjkuNjU4cS0uMTY3LjExMi0zLjY3Ljk1OS0uNzQ3LjE2Ny0xLjAxNS4yNTdsLjAxMS0uMDIyYS43NjkuNzY5LDAsMCwxLS41MTMtLjA0NC45MTQuOTE0LDAsMCwxLS40MTMtLjM1Ny43ODYuNzg2LDAsMCwxLDAtLjk3MXEuMDExLS4wMTEuODM2LTEuMTM3LDEuMzk0LTEuOTA4LDEuNjczLTIuMjc1YTIuNDIzLDIuNDIzLDAsMCwxLC4zNzktLjQzNUEuNy43LDAsMCwxLDE3LjQzNSw4YTQuNDgyLDQuNDgyLDAsMCwxLDEuMzcyLDEuNDg5LDQuODEsNC44MSwwLDAsMSwuOSwxLjg2OHYuMDM0WiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi14YW5nYSIgdmlld2JveD0iMCAwIDI0IDI0Ij4KPHBhdGggZD0iTTksOWg2djZIOVY5eiBNMyw5aDZWM0gzVjl6IE0xNSw5aDZWM2gtNlY5eiBNMTUsMjFoNnYtNmgtNlYyMXogTTMsMjFoNnYtNkgzVjIxeiI+PC9wYXRoPgo8L3N5bWJvbD4KPHN5bWJvbCBpZD0iaWNvbi15b3V0dWJlIiB2aWV3Ym94PSIwIDAgMjQgMjQiPgo8cGF0aCBkPSJNMjEuOCw4LjAwMWMwLDAtMC4xOTUtMS4zNzgtMC43OTUtMS45ODVjLTAuNzYtMC43OTctMS42MTMtMC44MDEtMi4wMDQtMC44NDdjLTIuNzk5LTAuMjAyLTYuOTk3LTAuMjAyLTYuOTk3LTAuMjAyIGgtMC4wMDljMCwwLTQuMTk4LDAtNi45OTcsMC4yMDJDNC42MDgsNS4yMTYsMy43NTYsNS4yMiwyLjk5NSw2LjAxNkMyLjM5NSw2LjYyMywyLjIsOC4wMDEsMi4yLDguMDAxUzIsOS42MiwyLDExLjIzOHYxLjUxNyBjMCwxLjYxOCwwLjIsMy4yMzcsMC4yLDMuMjM3czAuMTk1LDEuMzc4LDAuNzk1LDEuOTg1YzAuNzYxLDAuNzk3LDEuNzYsMC43NzEsMi4yMDUsMC44NTVjMS42LDAuMTUzLDYuOCwwLjIwMSw2LjgsMC4yMDEgczQuMjAzLTAuMDA2LDcuMDAxLTAuMjA5YzAuMzkxLTAuMDQ3LDEuMjQzLTAuMDUxLDIuMDA0LTAuODQ3YzAuNi0wLjYwNywwLjc5NS0xLjk4NSwwLjc5NS0xLjk4NXMwLjItMS42MTgsMC4yLTMuMjM3di0xLjUxNyBDMjIsOS42MiwyMS44LDguMDAxLDIxLjgsOC4wMDF6IE05LjkzNSwxNC41OTRsLTAuMDAxLTUuNjJsNS40MDQsMi44Mkw5LjkzNSwxNC41OTR6Ij48L3BhdGg+Cjwvc3ltYm9sPgo8L2RlZnM+Cjwvc3ZnPg==)
